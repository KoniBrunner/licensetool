﻿CREATE TABLE [dbo].[SentMessages]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [PluginName] NVARCHAR(64) NOT NULL, 
    [MessageMD5] NVARCHAR(32) NOT NULL, 
    [Timestamp] DATETIME NOT NULL
)
