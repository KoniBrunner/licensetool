﻿CREATE TABLE [dbo].[License]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [FeatureName] NVARCHAR(128) NOT NULL, 
    [OrderName] NVARCHAR(128) NULL, 
    [OrderStartDate] SMALLDATETIME NULL, 
    [OrderEndDate] SMALLDATETIME NULL, 
    [LicServer] INT NOT NULL, 
    [Type] INT NULL DEFAULT 0, 
    [Version] NVARCHAR(50) NULL, 
    [NumLicenses] INT NULL , 
    [Status] INT NULL DEFAULT 0, 
    CONSTRAINT [FK_License_LicServer] FOREIGN KEY ([LicServer]) REFERENCES [LicServer]([Id]) 
)
