﻿CREATE TABLE [dbo].[LicenseUsage]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [EndDate] SMALLDATETIME NULL, 
    [StartDate] SMALLDATETIME NULL, 
    [LicUser] INT NOT NULL, 
    [LicHost] INT NOT NULL, 
    [License] INT NOT NULL, 
    [State] INT NULL DEFAULT 0, 
    [Version] NVARCHAR(50) NULL, 
    [Quantity] INT NULL DEFAULT 1, 
    CONSTRAINT [FK_LicenseUsage_LicUser] FOREIGN KEY ([LicUser]) REFERENCES [LicUser]([Id]), 
    CONSTRAINT [FK_LicenseUsage_LicHost] FOREIGN KEY ([LicHost]) REFERENCES [LicHost]([Id]), 
    CONSTRAINT [FK_LicenseUsage_License] FOREIGN KEY ([License]) REFERENCES [License]([Id])
)
