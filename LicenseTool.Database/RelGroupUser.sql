﻿CREATE TABLE [dbo].[RelGroupUser]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [GroupId] INT NOT NULL, 
    [UserId] INT NOT NULL, 
    CONSTRAINT [FK_RelGroupUser_LicUser] FOREIGN KEY (UserId) REFERENCES LicUser(Id), 
    CONSTRAINT [FK_RelGroupUser_Group] FOREIGN KEY (GroupId) REFERENCES [Group](Id)
)
