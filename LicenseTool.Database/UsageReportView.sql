﻿CREATE VIEW [dbo].[UsageReportView]
	AS 
WITH SAMPLES AS
(
	SELECT DISTINCT DATEPART(YEAR, LU.StartDate) as Year,
	DATEPART(MONTH, LU.StartDate) as Month,
	DATEPART(DAY, LU.StartDate) as Day,
	DATEPART(HOUR, LU.StartDate) as Hour,
	(DATEPART(MINUTE, LU.StartDate) / 5)*5 as Minute,
	DATEADD(mm, (DATEPART(YEAR, LU.StartDate)-1900) * 12 + DATEPART(MONTH, LU.StartDate) - 1, DATEPART(DAY, LU.StartDate) - 1 ) + dateadd(mi, DATEPART(HOUR, LU.StartDate) * 60 + (DATEPART(MINUTE, LU.StartDate) / 5)*5 - 5, 0 ) as FromDate,
	DATEADD(mm, (DATEPART(YEAR, LU.StartDate)-1900) * 12 + DATEPART(MONTH, LU.StartDate) - 1, DATEPART(DAY, LU.StartDate) - 1 ) + dateadd(mi, DATEPART(HOUR, LU.StartDate) * 60 + (DATEPART(MINUTE, LU.StartDate) / 5)*5 + 4, 0 ) + dateadd(ss, 59, 0) + dateadd(ms, 998, 0) as ToDate,
	LI.Id as FeatureId
	FROM LicenseUsage LU
	inner join License LI on LU.License=LI.Id

	UNION

	SELECT DISTINCT DATEPART(YEAR, LU.EndDate) as Year,
	DATEPART(MONTH, LU.EndDate) as Month,
	DATEPART(DAY, LU.EndDate) as Day,
	DATEPART(HOUR, LU.EndDate) as Hour,
	(DATEPART(MINUTE, LU.EndDate) / 5)*5 as Minute,
	DATEADD(mm, (DATEPART(YEAR, LU.EndDate)-1900) * 12 + DATEPART(MONTH, LU.EndDate) - 1, DATEPART(DAY, LU.EndDate) - 1 ) + dateadd(mi, DATEPART(HOUR, LU.EndDate) * 60 + (DATEPART(MINUTE, LU.EndDate) / 5)*5 - 5, 0 ) as FromDate,
	DATEADD(mm, (DATEPART(YEAR, LU.EndDate)-1900) * 12 + DATEPART(MONTH, LU.EndDate) - 1, DATEPART(DAY, LU.EndDate) - 1 ) + dateadd(mi, DATEPART(HOUR, LU.EndDate) * 60 + (DATEPART(MINUTE, LU.EndDate) / 5)*5 + 4, 0 ) + dateadd(ss, 59, 0) + dateadd(ms, 998, 0) as ToDate,
	LI.Id as FeatureId
	FROM LicenseUsage LU
	inner join License LI on LU.License=LI.Id

), QUANTITIES AS (
SELECT DISTINCT SMPL.Year, SMPL.Month, SMPL.Day, SMPL.Hour, SMPL.Minute, SMPL.FeatureId, SUM(ILU.Quantity) as Quantity FROM SAMPLES SMPL
inner join LicenseUsage ILU on ILU.License=SMPL.FeatureId and ((ILU.StartDate <= SMPL.ToDate and ILU.EndDate >= SMPL.FromDate) or (ILU.StartDate < SMPL.FromDate and ILU.EndDate > SMPL.ToDate))
GROUP BY SMPL.Year, SMPL.Month, SMPL.Day, SMPL.Hour, SMPL.Minute, SMPL.FeatureId
)
SELECT Year, Month, -1 as Day, -1 as Hour, -1 as Minute, FeatureId, MAX(Quantity) as Quantity FROM QUANTITIES
GROUP BY Year, Month, FeatureId
UNION ALL
SELECT Year, Month, Day, -1 as Hour, -1 as Minute, FeatureId, MAX(Quantity) as Quantity FROM QUANTITIES
GROUP BY Year, Month, Day, FeatureId
UNION ALL
SELECT Year, Month, Day, Hour, -1 as Minute, FeatureId, MAX(Quantity) as Quantity FROM QUANTITIES
GROUP BY Year, Month, Day, Hour, FeatureId
UNION ALL
SELECT Year, Month, Day, Hour, Minute, FeatureId, Quantity FROM QUANTITIES
