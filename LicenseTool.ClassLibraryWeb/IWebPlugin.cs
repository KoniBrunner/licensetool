﻿using LicenseTool.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace LicenseTool.ClassLibraryWeb
{

    /// <summary>
    /// An interface describing a plugin used in the web application
    /// </summary>
    public interface IWebPlugin : IPlugin
    {

        /// <summary>
        /// Renders the plugin on the index page
        /// </summary>
        /// <param name="context">The http context</param>
        /// <param name="pluginId">The id of that plugin</param>
        /// <returns>The html string of the plugin to be shown on the index page</returns>
        string RenderIndex(HttpContextBase context, int pluginId);

        /// <summary>
        /// Renders the plugin on the html result page
        /// </summary>
        /// <param name="context">The http context</param>
        /// <returns>The html string of the plugin to be shown on the html result page</returns>
        string RenderResultHtml(HttpContextBase context);

        /// <summary>
        /// Gets the url path of the plugin icon
        /// </summary>
        /// <param name="context">The http context</param>
        /// <returns>The url path to the plugin icon to be shown</returns>
        string GetIconPath(HttpContextBase context);

        /// <summary>
        /// Renders the plugin on the table result page
        /// </summary>
        /// <param name="context">The http context</param>
        /// <returns>The html string array of the plugin to be shown on the table result page</returns>
        string[][] BuildResultTable(HttpContextBase context);

        /// <summary>
        /// Renders the plugin on the index page
        /// </summary>
        /// <param name="context">The http context</param>
        /// <param name="pluginId">The id of that plugin</param>
        /// <returns>The html string of the plugin to be shown on the index page</returns>
        string RenderIndex(HttpContext context, int pluginId);

        /// <summary>
        /// Renders the plugin on the html result page
        /// </summary>
        /// <param name="context">The http context</param>
        /// <returns>The html string of the plugin to be shown on the html result page</returns>
        string RenderResultHtml(HttpContext context);

        /// <summary>
        /// Gets the url path of the plugin icon
        /// </summary>
        /// <param name="context">The http context</param>
        /// <returns>The url path to the plugin icon to be shown</returns>
        string GetIconPath(HttpContext context);

        /// <summary>
        /// Renders the plugin on the table result page
        /// </summary>
        /// <param name="context">The http context</param>
        /// <returns>The html string array of the plugin to be shown on the table result page</returns>
        string[][] BuildResultTable(HttpContext context);
    }

}
