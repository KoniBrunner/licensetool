@echo off
setlocal

set TargetName=%~1
set DestName=%~2
set RND=%RANDOM%
set TEMPDIR=%TEMP%\ZipBuild_%RND%

echo Building zip file "%DestName%"
echo Target "%TargetName%.*"
echo Temp location "%TEMPDIR%"

mkdir "%TEMPDIR%"
copy "%TargetName%.exe" "%TEMPDIR%"
copy "%TargetName%.pdb" "%TEMPDIR%"
copy "%TargetName%.exe.config" "%TEMPDIR%"

ping localhost -n 2 > NUL

echo Set fso = CreateObject("Scripting.FileSystemObject") > "%TEMP%\CmdZipBuild_%RND%.vbs"
echo Set objArgs = WScript.Arguments >> "%TEMP%\CmdZipBuild_%RND%.vbs"
echo InputFolder = fso.GetAbsolutePathName(objArgs(0)) >> "%TEMP%\CmdZipBuild_%RND%.vbs"
echo ZipFile = fso.GetAbsolutePathName(objArgs(1)) >> "%TEMP%\CmdZipBuild_%RND%.vbs"
echo fso.CreateTextFile(ZipFile, True).Write "PK" ^& Chr(5) ^& Chr(6) ^& String(18, vbNullChar) >> "%TEMP%\CmdZipBuild_%RND%.vbs"
echo Set objShell = CreateObject("Shell.Application") >> "%TEMP%\CmdZipBuild_%RND%.vbs"
echo Set source = objShell.NameSpace(InputFolder).Items >> "%TEMP%\CmdZipBuild_%RND%.vbs"
echo objShell.NameSpace(ZipFile).CopyHere(source) >> "%TEMP%\CmdZipBuild_%RND%.vbs"
echo wScript.Sleep 2000 >> "%TEMP%\CmdZipBuild_%RND%.vbs"

CScript  "%TEMP%\CmdZipBuild_%RND%.vbs" "%TEMPDIR%" "%DestName%"

rmdir /s /q "%TEMPDIR%"
del /f /q "%TEMP%\CmdZipBuild_%RND%.vbs"

endlocal