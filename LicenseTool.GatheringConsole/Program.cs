﻿using LicenseTool.ClassLibrary;
using log4net;
using log4net.Appender;
using log4net.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LicenseTool.GatheringConsole
{
    /// <summary>
    /// The main program of the gathering command line tool
    /// </summary>
    public class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        /// <summary>
        /// Main method for the tool
        /// </summary>
        /// <param name="args">The arguments given on command line</param>
        /// <seealso cref="Gather"/>
        /// <seealso cref="AdIntegration"/>
        public static void Main(string[] args)
        {
            if (log.IsDebugEnabled)
                log.Debug("Main");
            foreach (ILoggerRepository repo in LogManager.GetAllRepositories())
            {
                foreach (IAppender appender in repo.GetAppenders())
                {
                    if (appender is RollingFileAppender)
                    {
                        FileInfo logFile = new FileInfo(((RollingFileAppender)appender).File);
                        log.Info("LogFile " + appender.Name + ": " + logFile.FullName);
                    }
                }
            }
            Gather prg = new Gather();

            if (args.Length == 0)
            {
                args = new string[1];
                args[0] = "";
            }
            switch (args[0].ToLower())
            {
                case "dir":
                    prg.GatherDirectory(new DirectoryInfo(Properties.Settings.Default.WatchLocation),
                        new DirectoryInfo(Properties.Settings.Default.MoveToLocation));
                    break;
                case "lic":
                    prg.GatherLicenses(new DirectoryInfo(Properties.Settings.Default.TempLocation),
                        new FileInfo(Properties.Settings.Default.LmutilPath));
                    break;
                case "ad":
                    AdIntegration.FillAllUsers();
                    break;
                default:
                    log.Error("Usage: GatheringConsole [ dir | lic | ad ]");
                    break;
            }

            if (log.IsDebugEnabled)
                log.Debug("Main Done");
        }

    }
}
