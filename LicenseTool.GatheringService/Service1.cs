﻿using LicenseTool.ClassLibrary;
using LicenseTool.Plugins;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LicenseTool.GatheringService
{

    /// <summary>
    /// The windows service class
    /// </summary>
    public partial class Service1 : ServiceBase
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Thread mainThread = null;
        private Thread gatherThread = null;
        private Thread checkerThread = null;
        private bool stopping = false;
        private bool pausing = false;
        private DateTime startTime;
        private DateTime lastCheckTime;

        /// <summary>
        /// Constructor initializing the class
        /// </summary>
        public Service1()
        {
            if (log.IsDebugEnabled)
                log.Debug("Service1");
            InitializeComponent();
            ThreadStart mainThreadStarter = new ThreadStart(MainStarter);
            mainThread = new Thread(mainThreadStarter);
            lastCheckTime = DateTime.Now;
            if (log.IsDebugEnabled)
                log.Debug("Service1 Done");
        }

        /// <summary>
        /// Called when the service starts
        /// </summary>
        /// <param name="args">Start arguments</param>
        protected override void OnStart(string[] args)
        {
            if (log.IsDebugEnabled)
                log.Debug("OnStart");
            if (stopping) return;
            stopping = false;
            startTime = DateTime.Now;
            mainThread.Start();
            if (log.IsDebugEnabled)
                log.Debug("OnStart Done");
        }

        /// <summary>
        /// Called when the service stops
        /// </summary>
        protected override void OnStop()
        {
            if (log.IsDebugEnabled)
                log.Debug("OnStop");
            stopping = true;
            mainThread.Join(11000);
            if (mainThread.IsAlive) mainThread.Abort();
            if ((checkerThread != null && checkerThread.IsAlive) ||
                (gatherThread != null && gatherThread.IsAlive))
            {
                while (!checkerThread.Join(10000) || !gatherThread.Join(10000))
                {
                    RequestAdditionalTime(10000);
                }
            }
            stopping = false;
            if (log.IsDebugEnabled)
                log.Debug("OnStop Done");
        }

        /// <summary>
        /// Called when the service pauses
        /// </summary>
        protected override void OnPause()
        {
            if (log.IsDebugEnabled)
                log.Debug("OnPause");
            if (stopping) return;
            pausing = true;
            base.OnPause();
            if (log.IsDebugEnabled)
                log.Debug("OnPause Done");
        }

        /// <summary>
        /// Called when the service continues
        /// </summary>
        protected override void OnContinue()
        {
            if (log.IsDebugEnabled)
                log.Debug("OnContinue");
            pausing = false;
            base.OnContinue();
            if (log.IsDebugEnabled)
                log.Debug("OnContinue Done");
        }

        /// <summary>
        /// Called when the service shuts down
        /// </summary>
        protected override void OnShutdown()
        {
            if (log.IsDebugEnabled)
                log.Debug("OnShutdown");

            if (gatherThread != null && gatherThread.IsAlive)
                gatherThread.Abort();
            if (checkerThread != null && checkerThread.IsAlive)
                checkerThread.Abort();

            base.OnShutdown();
            if (log.IsDebugEnabled)
                log.Debug("OnShutdown Done");
        }

        /// <summary>
        /// The main start method running in a thread
        /// </summary>
        protected virtual void MainStarter()
        {
            if (log.IsDebugEnabled)
                log.Debug("MainStarter");
            try
            {
                while (true)
                {
                    while (pausing || DateTime.Now < startTime)
                    {
                        if (stopping) return;
                        Thread.Sleep(10000);
                    }

                    log.Info("Starting gathering");
                    ThreadStart gatherThreadStarter = new ThreadStart(GatherStarter);
                    gatherThread = new Thread(gatherThreadStarter);
                    gatherThread.Start();

                    startTime = DateTime.Now.AddMinutes(Properties.Settings.Default.MainThreadSleepInterval);
                    log.Info("Next start at " + startTime.ToString("s"));
                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (ThreadInterruptedException)
            {
            }
            if (log.IsDebugEnabled)
                log.Debug("MainStarter Done");
        }

        /// <summary>
        /// The Gather start method running in a thread
        /// </summary>
        protected virtual void GatherStarter()
        {
            if (log.IsDebugEnabled)
                log.Debug("GatherStarter");
            if (stopping || pausing) return;

            try
            {
                Gather prg = new Gather();
                prg.GatherLicenses(new DirectoryInfo(Properties.Settings.Default.TempLocation),
                    new FileInfo(Properties.Settings.Default.LmutilPath));
                if (lastCheckTime < DateTime.Now.AddDays(-1))
                {
                    lastCheckTime = DateTime.Now;
                    DirectoryInfo moveTo = new DirectoryInfo(
                        new DirectoryInfo(Properties.Settings.Default.TempLocation).FullName +
                        Path.DirectorySeparatorChar + "licenseToolGatherDone");
                    if (!moveTo.Exists) return;
                    moveTo.GetFiles().Where(f => f.CreationTime < DateTime.Now.AddDays(-Properties.Settings.Default.DeleteTempFilesOlderThanDays))
                             .ToList().ForEach(f => f.Delete());
                }

                log.Info("Starting checker");
                ThreadStart checkerThreadStarter = new ThreadStart(CheckerStarter);
                checkerThread = new Thread(checkerThreadStarter);
                checkerThread.Start();

            }
            catch (ThreadAbortException)
            {
            }
            catch (ThreadInterruptedException)
            {
            }

            if (log.IsDebugEnabled)
                log.Debug("GatherStarter Done");
        }

        /// <summary>
        /// The Checker start method running in a thread
        /// </summary>
        protected virtual void CheckerStarter()
        {
            if (log.IsDebugEnabled)
                log.Debug("CheckerStarter");
            if (stopping || pausing) return;

            try
            {

                using (LicenseToolDatabaseEntities db = new LicenseToolDatabaseEntities())
                {
                    foreach (string plug in Properties.Settings.Default.CheckerPlugins)
                    {
                        IPlugin item = Activator.CreateInstance(Type.GetType(plug), new object[] { db }) as IPlugin;
                        string[][] retVal = item.BuildResultTable();
                        if (retVal.Length > 0)
                        {
                            //check if already sent
                            bool toBeSend = false;
                            DateTime? lastSendDate = new DateTime?();
                            StringBuilder checkStr = new StringBuilder();
                            foreach (string[] sub in retVal)
                            {
                                foreach (string value in sub)
                                {
                                    checkStr.Append(value);
                                    checkStr.Append('.');
                                }
                                checkStr.Append('.');
                            }
                            string hash;
                            using (MD5 md5 = MD5.Create())
                            {
                                hash = BitConverter.ToString(
                                  md5.ComputeHash(Encoding.UTF8.GetBytes(checkStr.ToString()))
                                );
                            }
                            string checkPlugName = item.GetType().FullName;
                            SentMessages message = db.SentMessages.FirstOrDefault(
                                m => m.PluginName == checkPlugName && m.MessageMD5 == hash);
                            if (message == null)
                            {
                                toBeSend = true;
                                message = new SentMessages();
                                message.PluginName = checkPlugName;
                                message.MessageMD5 = hash;
                                message.Timestamp = DateTime.Now;
                                db.SaveChanges();
                            }
                            else
                            {
                                if (message.Timestamp < DateTime.Now.AddDays(-Properties.Settings.Default.CheckerPluginsResendIntervall))
                                {
                                    toBeSend = true;
                                    lastSendDate = message.Timestamp;
                                    message.Timestamp = DateTime.Now;
                                    db.SaveChanges();
                                }
                            }

                            //send email
                            if (toBeSend)
                            {
                                foreach (string to in Properties.Settings.Default.CheckerPluginsSendTo)
                                {
                                    MailMessage mail = new MailMessage(Properties.Settings.Default.CheckerPluginsSendFrom, to);
                                    SmtpClient client = new SmtpClient();
                                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                                    if (string.IsNullOrEmpty(Properties.Settings.Default.SmtpPassword))
                                    {
                                        client.UseDefaultCredentials = true;
                                        client.Credentials = CredentialCache.DefaultNetworkCredentials;
                                    }
                                    else
                                    {
                                        client.UseDefaultCredentials = false;
                                        client.Credentials = new System.Net.NetworkCredential(
                                            Properties.Settings.Default.SmtpUser, Properties.Settings.Default.SmtpPassword);
                                    }

                                    client.Host = Properties.Settings.Default.SmtpServer;
                                    client.Port = Properties.Settings.Default.SmtpPort;
                                    mail.Subject = "Message from License Tool Plugin '" + item.Name + "'";

                                    StringBuilder msgBody = new StringBuilder();
                                    msgBody.AppendLine("The License Tool Plugin '" + item.Name + "' generated a message you should take care of:");
                                    msgBody.AppendLine("<br />");
                                    msgBody.AppendLine(item.Description);
                                    msgBody.AppendLine("<br />");
                                    msgBody.AppendLine("table>");
                                    foreach (string[] sub in retVal)
                                        msgBody.AppendLine("<tr><td>" + string.Join("</td><td>", sub) + "</td></tr>");
                                    msgBody.AppendLine("/table>");
                                    msgBody.AppendLine("<br />");
                                    msgBody.AppendLine("<br />");
                                    msgBody.AppendLine("<br />");
                                    msgBody.AppendLine("Additional information:");
                                    msgBody.AppendLine("<ul>");
                                    msgBody.AppendLine("<li>Plugin: " + item.GetType().FullName + "</li>");
                                    msgBody.AppendLine("<li>Date: " + DateTime.Now.ToString() + "</li>");
                                    if (lastSendDate.HasValue)
                                        msgBody.AppendLine("<li>Last sent: " + lastSendDate.Value.ToString() + "</li>");
                                    msgBody.AppendLine("</ul>");
                                    msgBody.AppendLine("&copy; " + DateTime.Now.Year + " - License Tool by Konrad Brunner");
                                    msgBody.AppendLine("<br />");
                                    mail.BodyEncoding = Encoding.Unicode;
                                    mail.IsBodyHtml = true;
                                    mail.Body = msgBody.ToString();
                                    client.Send(mail);
                                }
                            }
                        }
                    }
                }

            }
            catch (ThreadAbortException)
            {
            }
            catch (ThreadInterruptedException)
            {
            }

            if (log.IsDebugEnabled)
                log.Debug("CheckerStarter Done");
        }


    }

}
