﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace LicenseTool.GatheringService
{

    /// <summary>
    /// The main program of the gathering windows service
    /// </summary>
    public static class Program
    {

        /// <summary>
        /// The main entry point for the application. Instantiates the service class and launches the Run method.
        /// </summary>
        public static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new Service1() 
            };
            ServiceBase.Run(ServicesToRun);
        }
    }

}
