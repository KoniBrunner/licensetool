﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LicenseTool.ClassLibrary
{

    /// <summary>
    /// A thread pool item
    /// </summary>
    class PoolItem
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private ManualResetEvent doneEvent;
        private LicServer server = null;
        private LicenseToolDatabaseEntities db = null;
        private FileInfo lmutil = null;
        private DirectoryInfo watchDir = null;
        private DirectoryInfo moveTo = null;
        private DirectoryInfo errorTo = null;

        /// <summary>
        /// Constructor initalizing a new instance
        /// </summary>
        /// <param name="db">The db context</param>
        /// <param name="server">The server we do run a gather for</param>
        /// <param name="doneEvent">The thread pool done event</param>
        /// <param name="tempDir">The temporary directory to work in</param>
        /// <param name="lmutil">The path to the lmutil utility</param>
        /// <exception cref="ArgumentNullException">If <paramref name="db"/> is <c>null</c></exception>
        /// <exception cref="ArgumentNullException">If <paramref name="server"/> is <c>null</c></exception>
        /// <exception cref="ArgumentNullException">If <paramref name="doneEvent"/> is <c>null</c></exception>
        /// <exception cref="ArgumentNullException">If <paramref name="tempDir"/> is <c>null</c></exception>
        /// <exception cref="ArgumentNullException">If <paramref name="lmutil"/> is <c>null</c></exception>
        public PoolItem(LicenseToolDatabaseEntities db, LicServer server, ManualResetEvent doneEvent,
            DirectoryInfo tempDir, FileInfo lmutil)
        {
            if (log.IsDebugEnabled)
                log.Debug("PoolItem");

            if (db == null) throw new ArgumentNullException("db");
            if (server == null) throw new ArgumentNullException("server");
            if (doneEvent == null) throw new ArgumentNullException("doneEvent");
            if (tempDir == null) throw new ArgumentNullException("tempDir");
            if (lmutil == null) throw new ArgumentNullException("lmutil");

            this.server = server;
            this.doneEvent = doneEvent;
            this.db = db;
            this.lmutil = lmutil;
            watchDir = new DirectoryInfo(tempDir.FullName + Path.DirectorySeparatorChar + "licenseToolGatherWatch");
            if (!watchDir.Exists) watchDir.Create();
            moveTo = new DirectoryInfo(tempDir.FullName + Path.DirectorySeparatorChar + "licenseToolGatherDone");
            if (!moveTo.Exists) moveTo.Create();
            errorTo = new DirectoryInfo(tempDir.FullName + Path.DirectorySeparatorChar + "licenseToolGatherError");
            if (!errorTo.Exists) errorTo.Create();

            if (log.IsDebugEnabled)
                log.Debug("PoolItem Done");
        }

        /// <summary>
        /// The callback of the thread pool item
        /// </summary>
        /// <param name="threadContext">The thread context</param>
        /// <exception cref="ArgumentNullException">If <paramref name="threadContext"/> is <c>null</c></exception>
        public void ThreadPoolCallback(Object threadContext)
        {
            if (log.IsDebugEnabled)
                log.Debug("ThreadPoolCallback");

            log.Info("Gathering licenses of server " + server.Name);
            if (threadContext == null) throw new ArgumentNullException("threadContext");

            for (int i = 0; i < 3; i++)
            {
                try
                {
                    FileInfo fileLmstat = new FileInfo(watchDir.FullName + Path.DirectorySeparatorChar + "gather_" + server.Name + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + (i + 1) + ".log");

                    string serverPort = "";
                    string serverHost = "";
                    switch (i)
                    {
                        case 0:
                            if (server.NavLicHost1 == null) continue;
                            serverPort = server.LicPort1.ToString();
                            serverHost = server.NavLicHost1.Name;
                            break;
                        case 1:
                            if (server.NavLicHost2 == null) continue;
                            serverPort = server.LicPort2.ToString();
                            serverHost = server.NavLicHost2.Name;
                            break;
                        case 2:
                            if (server.NavLicHost3 == null) continue;
                            serverPort = server.LicPort3.ToString();
                            serverHost = server.NavLicHost3.Name;
                            break;
                    }

                    System.Diagnostics.ProcessStartInfo procStIfo =
                         new System.Diagnostics.ProcessStartInfo(lmutil.FullName, "lmstat -a -c " + serverPort + "@" + serverHost);
                    procStIfo.RedirectStandardOutput = true;
                    procStIfo.UseShellExecute = false;
                    procStIfo.CreateNoWindow = true;
                    System.Diagnostics.Process proc = new System.Diagnostics.Process();
                    proc.StartInfo = procStIfo;
                    proc.Start();

                    using (StreamWriter stdWrite = fileLmstat.CreateText())
                    {
                        using (StreamReader stdOut = proc.StandardOutput)
                        {
                            while (!stdOut.EndOfStream)
                            {
                                stdWrite.WriteLine(stdOut.ReadLine());
                            }
                        }
                    }

                    log.Info("Parsing file " + fileLmstat.FullName);
                    bool hadError = true;

                    try
                    {
                        using (StreamReader reader = fileLmstat.OpenText())
                        {
                            lock (db)
                            {
                                Parser parser = new Parser(reader, db, server);
                                parser.Parse();
                                hadError = false;
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        log.Error("Exception: Type: " + ex.GetType().ToString() + " Message: " + ex.Message, ex);
                    } 

                    if (!hadError)
                        fileLmstat.MoveTo(moveTo.FullName + Path.DirectorySeparatorChar + fileLmstat.Name);
                    else
                        fileLmstat.MoveTo(errorTo.FullName + Path.DirectorySeparatorChar + fileLmstat.Name);

                }
                catch (Exception ex)
                {
                    log.Error("Exception: Type: " + ex.GetType().ToString() + " Message: " + ex.Message, ex);
                }

            }

            log.Info("Server " + server.Name + " done.");

            doneEvent.Set();
            if (log.IsDebugEnabled)
                log.Debug("ThreadPoolCallback Done");
        }

    }

    /// <summary>
    /// The class holding the gather methods
    /// </summary>
    public class Gather
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Gathers the license servers
        /// </summary>
        /// <param name="tempDir">The temporary directory to work in</param>
        /// <param name="lmutil">The path to the lmutil utility</param>
        /// <exception cref="ArgumentNullException">If <paramref name="tempDir"/> is <c>null</c></exception>
        /// <exception cref="ArgumentNullException">If <paramref name="lmutil"/> is <c>null</c></exception>
        public virtual void GatherLicenses(DirectoryInfo tempDir, FileInfo lmutil)
        {
            if (log.IsDebugEnabled)
                log.Debug("GatherLicenses");

            if (tempDir == null) throw new ArgumentNullException("tempDir");
            if (lmutil == null) throw new ArgumentNullException("lmutil");
            
            if (tempDir.Exists)
            {
                log.Info("Reading existing data from database");
                try
                {
                    short tries = 0;
                    while (true)
                    {
                        if (tries > 3)
                        {
                            break;
                            throw new TooMuchTriesException("Had 3 times a sql exception. Giving up.");
                        }
                        tries++;
                        try
                        {
                            using (LicenseToolDatabaseEntities db = new LicenseToolDatabaseEntities())
                            {
                                db.LicUser.ToList();
                                db.LicHost.ToList();
                                db.License.ToList();
                                List<LicServer> servers = db.LicServer.ToList();
                                db.LicenseUsage.Where(u => u.State == (int)LicenseState.OUT).ToList();

                                ManualResetEvent[] doneEvents = new ManualResetEvent[servers.Count];
                                PoolItem[] poolItems = new PoolItem[servers.Count];
                                for (int i = 0; i < servers.Count; i++)
                                {
                                    LicServer server = servers[i];
                                    doneEvents[i] = new ManualResetEvent(false);
                                    poolItems[i] = new PoolItem(db, server, doneEvents[i], tempDir, lmutil);
                                    ThreadPool.QueueUserWorkItem(poolItems[i].ThreadPoolCallback, i);
                                }
                                WaitHandle.WaitAll(doneEvents);

                                try
                                {
                                    log.Info("Saving content to database");
                                    db.SaveChanges();
                                }
                                catch (DbEntityValidationException dbEx)
                                {
                                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                                    {
                                        foreach (var validationError in validationErrors.ValidationErrors)
                                        {
                                            log.Error("DbEntityValidationException: Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
                                        }
                                    }
                                }
                                catch (DbUpdateException upEx)
                                {
                                    var sqlException = upEx.GetBaseException() as SqlException;
                                    if (sqlException != null)
                                    {
                                        log.Error("DbEntityValidationException: SqlException: Number: " + sqlException.Number + " Message: " + sqlException.Message);
                                    }
                                }
                            }
                            break;

                        }
                        catch (EntityException eex)
                        {
                            log.Error("Exception: Type: " + eex.GetType().ToString() + " Message: " + eex.Message, eex);
                            Thread.Sleep(10000);
                        }
                        catch (SqlException sex)
                        {
                            log.Error("Exception: Type: " + sex.GetType().ToString() + " Message: " + sex.Message, sex);
                            Thread.Sleep(10000);
                        }
                    }
                    AdIntegration.FillEmptyUsers();
                }
                catch (Exception ex)
                {
                    log.Error("Exception: Type: " + ex.GetType().ToString() + " Message: " + ex.Message, ex);
                }
            }
            else
            {
                log.Error("Can't find temporary directory " + tempDir.FullName);
            }
            if (log.IsDebugEnabled)
                log.Debug("GatherLicenses Done");
        }

        /// <summary>
        /// Gathers all files in a directory
        /// </summary>
        /// <param name="watchDir">The directory we look for files</param>
        /// <param name="moveTo">The directory we move parsed files to</param>
        /// <exception cref="ArgumentNullException">If <paramref name="watchDir"/> is <c>null</c></exception>
        /// <exception cref="ArgumentNullException">If <paramref name="moveTo"/> is <c>null</c></exception>
        public virtual void GatherDirectory(DirectoryInfo watchDir, DirectoryInfo moveTo)
        {
            if (log.IsDebugEnabled)
                log.Debug("GatherDirectory");

            if (watchDir == null) throw new ArgumentNullException("watchDir");
            if (moveTo == null) throw new ArgumentNullException("moveTo");

            log.Info("Gathering directory: " + watchDir.FullName);
            try
            {
                if (watchDir.Exists && moveTo.Exists)
                {
                    watchDir.Refresh();
                    if (watchDir.GetFiles().Count() > 0)
                    {
                        log.Info("Reading existing data from database");
                        using (LicenseToolDatabaseEntities db = new LicenseToolDatabaseEntities())
                        {
                            db.LicUser.ToList();
                            db.LicHost.ToList();
                            db.License.ToList();
                            db.LicServer.ToList();
                            db.LicenseUsage.Where(u => u.State == (int)LicenseState.OUT).ToList();

                            foreach (FileInfo file in watchDir.EnumerateFiles()
                                .OrderBy(f => f.CreationTime)
                                .Select(f => f)
                                .ToList())
                            {
                                log.Info("Parsing file " + file.FullName);
                                try
                                {
                                    bool hadError = true;
                                    using (StreamReader reader = file.OpenText())
                                    {
                                        Parser parser = new Parser(reader, db, null);
                                        parser.Parse();
                                        try
                                        {
                                            log.Info("Saving content to database");
                                            db.SaveChanges();
                                            hadError = false;
                                        }
                                        catch (DbEntityValidationException dbEx)
                                        {
                                            foreach (var validationErrors in dbEx.EntityValidationErrors)
                                            {
                                                foreach (var validationError in validationErrors.ValidationErrors)
                                                {
                                                    log.Error("DbEntityValidationException: Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
                                                }
                                            }
                                        }
                                        catch (DbUpdateException upEx)
                                        {
                                            var sqlException = upEx.GetBaseException() as SqlException;
                                            if (sqlException != null)
                                            {
                                                log.Error("DbEntityValidationException: SqlException: Number: " + sqlException.Number + " Message: " + sqlException.Message);
                                            }
                                        }
                                    }
                                    if (!hadError) file.MoveTo(moveTo.FullName + Path.DirectorySeparatorChar + file.Name);

                                }
                                catch (Exception ex)
                                {
                                    log.Error("Exception: Type: " + ex.GetType().ToString() + " Message: " + ex.Message, ex);
                                }
                            }
                        }
                        AdIntegration.FillEmptyUsers();
                    }
                }
                else
                {
                    log.Error("Can't find watch or moveto directory!");
                }

            }
            catch (Exception ex)
            {
                log.Error("Exception: Type: " + ex.GetType().ToString() + " Message: " + ex.Message, ex);
            } log.Info("Done.");

            if (log.IsDebugEnabled)
                log.Debug("GatherDirectory Done");
        }
    }
}
