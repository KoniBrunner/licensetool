﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LicenseTool.ClassLibrary
{

    /// <summary>
    /// Exception throws on errors in parsing
    /// </summary>
    class ParseException : ApplicationException
    {
        public ParseException(string message)
            : base(message)
        {
        }
    }

    /// <summary>
    /// Exception thrown after 3 unsuccesfull connection tries
    /// </summary>
    class TooMuchTriesException : ApplicationException
    {
        public TooMuchTriesException(string message)
            : base(message)
        {
        }
    }

}
