﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LicenseTool.ClassLibrary
{
    /// <summary>
    /// This class handles the integration to the active directory
    /// </summary>
    public class AdIntegration
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Fills empty users with active directory information
        /// </summary>
        public static void FillEmptyUsers()
        {
            if (log.IsDebugEnabled)
                log.Debug("FillEmptyUsers");

            try
            {
                DirectoryEntry entry = null;
                if (!string.IsNullOrEmpty(Properties.Settings.Default.ActiveDirectorySearchPwd))
                    entry = new DirectoryEntry(Properties.Settings.Default.ActiveDirectorySearchRoot,
                        Properties.Settings.Default.ActiveDirectorySearchUser, Properties.Settings.Default.ActiveDirectorySearchPwd);
                else
                    entry = new DirectoryEntry(Properties.Settings.Default.ActiveDirectorySearchRoot);
                DirectorySearcher dSearch = new DirectorySearcher(entry);
                dSearch.PropertiesToLoad.AddRange(new string[] { "givenName", "initials", "sn", "mail", "telephoneNumber" });
                using (LicenseToolDatabaseEntities db = new LicenseToolDatabaseEntities())
                {
                    List<LicUser> users = db.LicUser.Where(u => string.IsNullOrEmpty(u.Email) || string.IsNullOrEmpty(u.FullName)).ToList();
                    foreach (LicUser user in users)
                        FillUser(user.Name, dSearch, user);
                    db.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                log.Error("Exception: Type: " + ex.GetType().ToString() + " Message: " + ex.Message, ex);
            }

            if (log.IsDebugEnabled)
                log.Debug("FillEmptyUsers Done");
        }

        /// <summary>
        /// Overwrites all users with active directory information
        /// </summary>
        public static void FillAllUsers()
        {
            if (log.IsDebugEnabled)
                log.Debug("FillAllUsers");

            try
            {
                DirectoryEntry entry = null;
                if (!string.IsNullOrEmpty(Properties.Settings.Default.ActiveDirectorySearchPwd))
                    entry = new DirectoryEntry(Properties.Settings.Default.ActiveDirectorySearchRoot,
                        Properties.Settings.Default.ActiveDirectorySearchUser, Properties.Settings.Default.ActiveDirectorySearchPwd);
                else
                    entry = new DirectoryEntry(Properties.Settings.Default.ActiveDirectorySearchRoot);
                DirectorySearcher dSearch = new DirectorySearcher(entry);
                dSearch.PropertiesToLoad.AddRange(new string[] { "givenName", "initials", "sn", "mail", "telephoneNumber" });
                using (LicenseToolDatabaseEntities db = new LicenseToolDatabaseEntities())
                {
                    List<LicUser> users = db.LicUser.ToList();
                    foreach (LicUser user in users)
                        FillUser(user.Name, dSearch, user);
                    db.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                log.Error("Exception: Type: " + ex.GetType().ToString() + " Message: " + ex.Message, ex);
            }

            if (log.IsDebugEnabled)
                log.Debug("FillAllUsers Done");
        }

        /// <summary>
        /// Fills a specific user with active directory information
        /// </summary>
        /// <param name="loginName">The login name of the ad user</param>
        /// <param name="dSearch">A directory searcher instance</param>
        /// <param name="user">The entity to be filled</param>
        /// <exception cref="ArgumentNullException">If <paramref name="loginName"/> is <c>null</c> or <c>empty</c></exception>
        /// <exception cref="ArgumentNullException">If <paramref name="dSearch"/> is <c>null</c></exception>
        /// <exception cref="ArgumentNullException">If <paramref name="user"/> is <c>null</c></exception>
        private static void FillUser(string loginName, DirectorySearcher dSearch, LicUser user)
        {
            if (log.IsDebugEnabled)
                log.Debug("FillUser loginName=" + loginName);

            if (string.IsNullOrEmpty(loginName)) throw new ArgumentNullException("loginName");
            if (dSearch == null) throw new ArgumentNullException("dSearch");
            if (user == null) throw new ArgumentNullException("user");

            dSearch.Filter = "(&(objectClass=user)(sAMAccountName=" + loginName + "))";
            SearchResult result = dSearch.FindOne();
            if (result == null)
            {
                if (log.IsDebugEnabled)
                    log.Debug("result is null!");
                return;
            }
            if (log.IsDebugEnabled)
                log.Debug("result found: " + result.Path);

            user.FullName = (GetProperty(result, "givenName") + " " + GetProperty(result, "initials") + " " + GetProperty(result, "sn")).Replace("  ", " ");
            if (log.IsDebugEnabled)
                log.Debug("user.FullName: " + user.FullName);
            user.Email = GetProperty(result, "mail");
            if (log.IsDebugEnabled)
                log.Debug("user.Email: " + user.Email);
            user.PhoneNumber = GetProperty(result, "telephoneNumber");
            if (log.IsDebugEnabled)
                log.Debug("user.PhoneNumber: " + user.PhoneNumber);

            if (log.IsDebugEnabled)
                log.Debug("FillUser Done");
        }

        /// <summary>
        /// Gets the value of a property
        /// </summary>
        /// <param name="searchResult">The result the containing the property</param>
        /// <param name="propertyName">The name of the property the value has to be taken from</param>
        /// <returns>The value of the property</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="propertyName"/> is <c>null</c> or <c>empty</c></exception>
        /// <exception cref="ArgumentNullException">If <paramref name="searchResult"/> is <c>null</c></exception>
        private static string GetProperty(SearchResult searchResult, string propertyName)
        {
            if (log.IsDebugEnabled)
                log.Debug("GetProperty");

            if (string.IsNullOrEmpty(propertyName)) throw new ArgumentNullException("PropertyName");
            if (searchResult == null) throw new ArgumentNullException("searchResult");

            if (searchResult.Properties.Contains(propertyName))
            {
                if (log.IsDebugEnabled)
                    log.Debug("GetProperty Done");
                return searchResult.Properties[propertyName][0].ToString();
            }
            else
            {
                if (log.IsDebugEnabled)
                    log.Debug("GetProperty Done");
                return string.Empty;
            }
        }

    }
}
