﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using res = LicenseTool.ResourceLibrary.Enum;

namespace LicenseTool.ClassLibrary
{

    /// <summary>
    /// Enum to specify the server and vendor status of a license server
    /// </summary>
    public enum ServerAndVendorStatus
    {
        /// <summary>
        /// Specifies that the server or vendor daemon status is unknown
        /// </summary>
        [Display(Name = "UNKNOWN", ResourceType = typeof(res))]
        UNKNOWN = -1,
        /// <summary>
        /// Specifies that the server or vendor daemon is down
        /// </summary>
        [Display(Name = "DOWN", ResourceType = typeof(res))]
        DOWN = 0,
        /// <summary>
        /// Specifies that the server or vendor daemon is up and running
        /// </summary>
        [Display(Name = "UP", ResourceType = typeof(res))]
        UP = 1,
        /// <summary>
        /// Specifies that the server or vendor daemon on different hosts have different status
        /// </summary>
        [Display(Name = "MIXED", ResourceType = typeof(res))]
        MIXED = 2
    }

    /// <summary>
    /// Enum to specify the type of a license
    /// </summary>
    public enum LicenseType
    {
        /// <summary>
        /// Specifies a floating license type
        /// </summary>
        [Display(Name = "FLOATING", ResourceType = typeof(res))]
        FLOATING = 0,
        /// <summary>
        /// Specifies a named user license type
        /// </summary>
        [Display(Name = "NAMEDUSER", ResourceType = typeof(res))]
        NAMEDUSER = 1
    }

    /// <summary>
    /// Enum to specify the state of a license
    /// </summary>
    public enum LicenseState
    {
        /// <summary>
        /// Specifies that the license is actually checked out
        /// </summary>
        [Display(Name = "OUT", ResourceType = typeof(res))]
        OUT = 0,
        /// <summary>
        /// Specifies that the license has been given back to the server
        /// </summary>
        [Display(Name = "IN", ResourceType = typeof(res))]
        IN = 1
    }

    /// <summary>
    /// Enum to specify the status of a license
    /// </summary>
    public enum LicenseStatus
    {
        /// <summary>
        /// Specifies an active license
        /// </summary>
        [Display(Name = "ACTIVE", ResourceType = typeof(res))]
        ACTIVE = 0,
        /// <summary>
        /// Specifies a license in error state
        /// </summary>
        [Display(Name = "ERROR", ResourceType = typeof(res))]
        ERROR = 1
    }

    /// <summary>
    /// Helper class for enums
    /// </summary>
    public class EnumHelper
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Gets the name of a enum value
        /// </summary>
        /// <param name="enumType">The type of the enum</param>
        /// <param name="enumValue">The value of the enum</param>
        /// <returns>The name associated with the enum value</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="enumType"/> is <c>null</c></exception>
        /// <exception cref="ArgumentNullException">If <paramref name="enumValue"/> is <c>null</c></exception>
        public static string GetEnumValueName(Type enumType, Enum enumValue)
        {
            if (log.IsDebugEnabled)
                log.Debug("GetEnumValueName");

            if (enumType == null) throw new ArgumentNullException("enumType");
            if (enumValue == null) throw new ArgumentNullException("enumValue");

            FieldInfo fi = enumType.GetField(enumValue.ToString());

            DisplayAttribute[] attributes =
                (DisplayAttribute[])fi.GetCustomAttributes(
                typeof(DisplayAttribute),
                false);

            if (log.IsDebugEnabled)
                log.Debug("GetEnumValueName Done");
            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].GetName();
            else
                return enumValue.ToString();
        }

    }

}
