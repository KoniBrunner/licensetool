﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;

namespace LicenseTool.ClassLibrary
{

    /// <summary>
    /// The context interface used for dependency injection of the concret context <see cref="LicenseToolDatabaseEntities"/>
    /// <seealso cref="LicenseToolDatabaseEntities"/>
    /// </summary>
    public interface ILicenseToolContext
    {

        /// <summary>
        /// Holding the entities for License
        /// </summary>
        /// <seealso cref="License"/>
        IDbSet<License> License { get; set; }

        /// <summary>
        /// Holding the entities for LicenseUsage
        /// </summary>
        /// <seealso cref="LicenseUsage"/>
        IDbSet<LicenseUsage> LicenseUsage { get; set; }

        /// <summary>
        /// Holding the entities for LicHost
        /// </summary>
        /// <seealso cref="LicHost"/>
        IDbSet<LicHost> LicHost { get; set; }

        /// <summary>
        /// Holding the entities for LicServer
        /// </summary>
        /// <seealso cref="LicServer"/>
        IDbSet<LicServer> LicServer { get; set; }

        /// <summary>
        /// Holding the entities for LicUser
        /// </summary>
        /// <seealso cref="LicUser"/>
        IDbSet<LicUser> LicUser { get; set; }

        /// <summary>
        /// Holding the entities for UsageReportView
        /// </summary>
        /// <seealso cref="UsageReportView"/>
        IDbSet<UsageReportView> UsageReportView { get; set; }

        /// <summary>
        /// Holding the entities for Group
        /// </summary>
        /// <seealso cref="Group"/>
        IDbSet<Group> Group { get; set; }

        /// <summary>
        /// Holding the entities for RelGroupUser
        /// </summary>
        /// <seealso cref="RelGroupUser"/>
        IDbSet<RelGroupUser> RelGroupUser { get; set; }

        /// <summary>
        /// Holding the entities for SentMessages
        /// </summary>
        /// <seealso cref="SentMessages"/>
        IDbSet<SentMessages> SentMessages { get; set; }

        /// <summary>
        /// Saves changes to the database
        /// </summary>
        /// <returns>Number of saved entities</returns>
        int SaveChanges();

        void Dispose();

        /// <summary>
        /// Sets an entity into modified state
        /// </summary>
        /// <param name="entity">The entity to be modified</param>
        void SetModified(object entity);
    }

}
