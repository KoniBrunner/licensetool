﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LicenseTool.ClassLibrary
{

    /// <summary>
    /// Attribute to specify a UIHint for a Enum
    /// </summary>
    public class UIHintEnumTypeAttribute : UIHintAttribute
    {

        private Type enumType = null;

        /// <summary>
        /// Constructor to initialize an instance
        /// </summary>
        /// <param name="uiHint">The template name to be used</param>
        /// <param name="enumType">The type of the enum</param>
        /// <exception cref="ArgumentNullException">If <paramref name="enumType"/> is <c>null</c></exception>
        public UIHintEnumTypeAttribute(string uiHint, Type enumType)
            : base(uiHint)
        {
            if (enumType == null) throw new ArgumentNullException("enumType");
            this.enumType = enumType;
        }

        /// <summary>
        /// Constructor to initialize an instance
        /// </summary>
        /// <param name="uiHint">The template name to be used</param>
        /// <param name="presentationLayer">The presentation layer</param>
        /// <param name="enumType">The type of the enum</param>
        /// <exception cref="ArgumentNullException">If <paramref name="enumType"/> is <c>null</c></exception>
        public UIHintEnumTypeAttribute(string uiHint, string presentationLayer, Type enumType)
            : base(uiHint, presentationLayer)
        {
            if (enumType == null) throw new ArgumentNullException("enumType");
            this.enumType = enumType;
        }

        /// <summary>
        /// Constructor to initialize an instance
        /// </summary>
        /// <param name="uiHint">The template name to be used</param>
        /// <param name="presentationLayer">The presentation layer</param>
        /// <param name="enumType">The type of the enum</param>
        /// <param name="controlParameters">Some control parameters</param>
        /// <exception cref="ArgumentNullException">If <paramref name="enumType"/> is <c>null</c></exception>
        public UIHintEnumTypeAttribute(string uiHint, string presentationLayer, Type enumType, params object[] controlParameters)
            : base(uiHint, presentationLayer, controlParameters)
        {
            if (enumType == null) throw new ArgumentNullException("enumType");
            this.enumType = enumType;
        }

        /// <summary>
        /// The enum type to be rendered
        /// </summary>
        public Type EnumType { get { return this.enumType; } }

    }
}