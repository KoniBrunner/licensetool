﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LicenseTool.ClassLibrary
{
    /// <summary>
    /// The class used to parse license and lmutil output files
    /// </summary>
    public class Parser
    {
        private System.IO.StreamReader reader = null;
        private LicServer server = null;
        private LicenseToolDatabaseEntities db = null;
        private CultureInfo lmutilParserLocale = null;
        private CultureInfo licParserLocale = null;
        private const string LMUTIL = "lmutil - Copyright";
        private const string SERVER = "SERVER ";
        private const string VENDOR = "VENDOR ";
        private const string FEATURE = "FEATURE ";
        private const string INCREMENT = "INCREMENT ";
        private const string PERMANENT = "permanent";
        private const string FLEX_LM_STATUS = "Flexible License Manager status on ";
        private const string LICENSE_SERVER_STATUS = "License server status: ";
        private const string VENDOR_DAEMON_STATUS = "Vendor daemon status ";
        private const string FEATURE_USAGE_INFO = "Feature usage info:";
        private const string USERS_OF = "Users of ";
        private const string LICENSE_SERVER = "license server";
        private const string FLOATING_LICENSE = "floating license";
        private const string ERROR = "Error";
        private const string MASTER = "MASTER";
        private const string ERROR_DOWN = "License server machine is down or not responding";
        private const string LICFILE_ON = "License file(s) on";
        private const string TOTAL = "total";
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Constructor initializing the instance
        /// </summary>
        /// <param name="reader">The stream of the file to be parsed</param>
        /// <param name="db">the database context</param>
        /// <param name="server">The actual license server we parse a file for</param>
        /// <exception cref="ArgumentNullException">If <paramref name="db"/> is <c>null</c></exception>
        /// <exception cref="ArgumentNullException">If <paramref name="server"/> is <c>null</c></exception>
        public Parser(System.IO.StreamReader reader, LicenseToolDatabaseEntities db, LicServer server)
        {
            if (log.IsDebugEnabled)
                log.Debug("Parser");

            if (db == null) throw new ArgumentNullException("db");
            if (server == null) throw new ArgumentNullException("server");
            
            this.reader = reader;
            this.server = server;
            this.db = db;
            lmutilParserLocale = CultureInfo.GetCultureInfo(Properties.Settings.Default.LmutilParserLocale);
            
            if (log.IsDebugEnabled)
                log.Debug("Parser Done");
        }

        /// <summary>
        /// Parses the file given to the constructor
        /// </summary>
        public void Parse()
        {
            if (log.IsDebugEnabled)
                log.Debug("Parse");

            LicServer srv = null;
            string srch = null;
            int inUse = 0;
            int found = 0;
            int serverNum = 0;
            int fileType = 0; //0 unknown, 1 lmutil, 2 license
            DateTime runningTime = DateTime.MinValue;
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                if (log.IsDebugEnabled)
                    log.Debug("line: " + line);

                if (fileType==0 && line.StartsWith(LMUTIL))
                {
                    fileType = 1;
                }
                else if (fileType == 0 && line.StartsWith(SERVER))
                {
                    fileType = 2;
                }

                if (!string.IsNullOrEmpty(line))
                {

                    if (fileType == 1)
                    {

                        if (line.StartsWith(FLEX_LM_STATUS))
                        {
                            string dateTimeStr = line.Substring(FLEX_LM_STATUS.Length);
                            runningTime = Convert.ToDateTime(dateTimeStr, lmutilParserLocale);
                        }
                        if (line.Contains(ERROR_DOWN))
                        {
                            if (server == null)
                            {
                                throw new ParseException("License server was down!");
                            }
                            else
                            {
                                server.ServerStatus = (short)ServerAndVendorStatus.DOWN;
                                return;
                            }
                        }
                        if (line.StartsWith(LICENSE_SERVER_STATUS))
                        {
                            if (log.IsDebugEnabled)
                                log.Debug("LICENSE_SERVER_STATUS");
                            string serverStr = line.Substring(LICENSE_SERVER_STATUS.Length).ToLower();
                            string[] servers = serverStr.Trim().Split(",".ToCharArray(),StringSplitOptions.RemoveEmptyEntries);
                            string[] parts = servers[0].Split("@".ToCharArray());
                            srch = parts[1];
                            if (string.IsNullOrEmpty(srch))
                                throw new ArgumentException("Server host name can't be empty!");
                            LicHost srvHost = db.LicHost.FirstOrDefault(h => h.Name == srch);
                            if (srvHost == null)
                            {
                                srvHost = db.LicHost.Local.FirstOrDefault(h => h.Name == srch);
                                if (srvHost == null)
                                {
                                    srvHost = new LicHost();
                                    srvHost.Name = srch;
                                    db.LicHost.Add(srvHost);
                                    db.Entry(srvHost).State = System.Data.EntityState.Added;
                                }
                            }
                            int port = Convert.ToInt32(parts[0]);
                            srv = db.LicServer.FirstOrDefault(s => (s.LicHost1 == srvHost.Id && s.LicPort1 == port) ||
                               (s.LicHost2 == srvHost.Id && s.LicPort2 == port) || (s.LicHost3 == srvHost.Id && s.LicPort3 == port));
                            if (srv == null)
                            {
                                srv = db.LicServer.Local.FirstOrDefault(s => (s.NavLicHost1.Name == srvHost.Name && s.LicPort1 == port) ||
                                   (s.NavLicHost2 != null && s.NavLicHost2.Name == srvHost.Name && s.LicPort2 == port) ||
                                   (s.NavLicHost3 != null && s.NavLicHost3.Name == srvHost.Name && s.LicPort3 == port));
                                if (srv == null)
                                {
                                    srv = new LicServer();
                                    srv.Name = "Please specify";
                                    srv.NavLicHost1 = srvHost;
                                    srv.LicPort1 = port;
                                    db.LicServer.Add(srv);
                                    db.Entry(srv).State = System.Data.EntityState.Added;
                                }
                            }
                            srv.ServerStatus = (int)ServerAndVendorStatus.UNKNOWN;
                            if (servers.Length > 1 && srv.NavLicHost2 == null)
                            {
                                parts = servers[1].Split("@".ToCharArray(),StringSplitOptions.RemoveEmptyEntries);
                                srch = parts[1];
                                srvHost = db.LicHost.FirstOrDefault(h => h.Name == srch);
                                if (srvHost == null)
                                {
                                    srvHost = db.LicHost.Local.FirstOrDefault(h => h.Name == srch);
                                    if (srvHost == null)
                                    {
                                        srvHost = new LicHost();
                                        srvHost.Name = srch;
                                        db.LicHost.Add(srvHost);
                                        db.Entry(srvHost).State = System.Data.EntityState.Added;
                                    }
                                }
                                port = Convert.ToInt32(parts[0]);
                                srv.NavLicHost2 = srvHost;
                                srv.LicPort2 = port;
                            }
                            if (servers.Length > 2 && srv.NavLicHost3 == null)
                            {
                                parts = servers[2].Split("@".ToCharArray(),StringSplitOptions.RemoveEmptyEntries);
                                srch = parts[1];
                                srvHost = db.LicHost.FirstOrDefault(h => h.Name == srch);
                                if (srvHost == null)
                                {
                                    srvHost = db.LicHost.Local.FirstOrDefault(h => h.Name == srch);
                                    if (srvHost == null)
                                    {
                                        srvHost = new LicHost();
                                        srvHost.Name = srch;
                                        db.LicHost.Add(srvHost);
                                        db.Entry(srvHost).State = System.Data.EntityState.Added;
                                    }
                                }
                                port = Convert.ToInt32(parts[0]);
                                srv.NavLicHost3 = srvHost;
                                srv.LicPort3 = port;
                            }
                            while (!reader.EndOfStream && !line.StartsWith(VENDOR_DAEMON_STATUS))
                            {
                                if (line.Contains(LICFILE_ON))
                                {
                                    if (srv.NavLicHost1 != null && line.ToLower().Contains(srv.NavLicHost1.Name))
                                    {
                                        srv.LicFile1 = "";
                                        parts = line.Split(":".ToCharArray(),StringSplitOptions.RemoveEmptyEntries);
                                        for (int i = 1; i < parts.Length; i++)
                                        {
                                            if (i > 1 && parts[i].Trim().Length > 0) srv.LicFile1 += "|";
                                            srv.LicFile1 += parts[i].Trim();
                                        }
                                    }
                                    if (srv.NavLicHost2 != null && line.ToLower().Contains(srv.NavLicHost2.Name))
                                    {
                                        srv.LicFile2 = "";
                                        parts = line.Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                                        for (int i = 1; i < parts.Length; i++)
                                        {
                                            if (i > 1 && parts[i].Trim().Length > 0) srv.LicFile1 += "|";
                                            srv.LicFile2 += parts[i].Trim();
                                        }
                                    }
                                    if (srv.NavLicHost3 != null && line.ToLower().Contains(srv.NavLicHost3.Name))
                                    {
                                        srv.LicFile3 = "";
                                        parts = line.Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                                        for (int i = 1; i < parts.Length; i++)
                                        {
                                            if (i > 1 && parts[i].Trim().Length > 0) srv.LicFile1 += "|";
                                            srv.LicFile3 += parts[i].Trim();
                                        }
                                    }
                                }
                                if (line.Contains(LICENSE_SERVER))
                                {
                                    if (log.IsDebugEnabled)
                                        log.Debug("LICENSE_SERVER");
                                    parts = line.Trim().Replace(":", "").Replace("(", "").Replace(")", "").ToLower().Split(" ".ToCharArray(),StringSplitOptions.RemoveEmptyEntries);
                                    if (parts.Length > 6)
                                    {
                                        srv.ServerVersion = parts[6];
                                    }
                                    else if (parts.Length > 5)
                                    {
                                        srv.ServerVersion = parts[5];
                                    }
                                    short stati;
                                    if (parts.Length > 4 && parts[3] == "up")
                                        stati = (short)ServerAndVendorStatus.UP;
                                    else
                                        stati = (short)ServerAndVendorStatus.DOWN;

                                    if (srv.ServerStatus == (short)ServerAndVendorStatus.UNKNOWN)
                                    {
                                        srv.ServerStatus = stati;
                                    }
                                    else
                                    {
                                        if (srv.ServerStatus != stati)
                                            srv.ServerStatus = (short)ServerAndVendorStatus.MIXED;
                                    }
                                }
                                line = reader.ReadLine();
                                if (log.IsDebugEnabled)
                                    log.Debug("line: " + line);
                            }
                        }
                        if (line.StartsWith(VENDOR_DAEMON_STATUS))
                        {
                            if (log.IsDebugEnabled)
                                log.Debug("VENDOR_DAEMON_STATUS");
                            line = reader.ReadLine();
                            if (log.IsDebugEnabled)
                                log.Debug("line: " + line);
                            while (!reader.EndOfStream && !line.StartsWith(FEATURE_USAGE_INFO))
                            {
                                if (!string.IsNullOrWhiteSpace(line))
                                {
                                    string[] parts = line.Trim().Replace(":", "").Replace("(", "").Replace(")", "").ToLower().Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                                    if (string.IsNullOrEmpty(srv.VendorName))
                                    {
                                        srv.VendorName = parts[0];
                                    }
                                    if (parts.Length > 2)
                                    {
                                        srv.VendorVersion = parts[2];
                                    }
                                    if (parts.Length > 1 && parts[1] == "up")
                                    {
                                        srv.VendorStatus = (int)ServerAndVendorStatus.UP;
                                    }
                                }
                                line = reader.ReadLine();
                                if (log.IsDebugEnabled)
                                    log.Debug("line: " + line);
                            }
                        }
                        if (line.StartsWith(FEATURE_USAGE_INFO))
                        {
                            if (log.IsDebugEnabled)
                                log.Debug("FEATURE_USAGE_INFO");
                            License lic = null;
                            while (!reader.EndOfStream)
                            {
                                line = reader.ReadLine();
                                if (log.IsDebugEnabled)
                                    log.Debug("line: " + line);
                                if (line.Trim().StartsWith(USERS_OF))
                                {
                                    if (log.IsDebugEnabled)
                                        log.Debug("USERS_OF");

                                    if (inUse != found)
                                        log.Warn("Was not able to find all licenses for " + lic.FeatureName);

                                    string[] parts = line.Trim().Replace(":", "").Replace("(", "").Replace(")", "").ToLower().Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                                    string licName = parts[2];
                                    if (log.IsDebugEnabled)
                                        log.Debug("License: " + licName);
                                    lic = db.License.FirstOrDefault(s => s.FeatureName == licName && s.LicServer == srv.Id);
                                    if (lic == null)
                                    {
                                        lic = db.License.Local.FirstOrDefault(s => s.FeatureName == licName && s.NavLicServer.NavLicHost1.Name == srv.NavLicHost1.Name && s.NavLicServer.LicPort1 == srv.LicPort1);
                                        if (lic == null)
                                        {
                                            lic = new License();
                                            lic.NavLicServer = srv;
                                            lic.FeatureName = licName;
                                            db.License.Add(lic);
                                            db.Entry(lic).State = System.Data.EntityState.Added;
                                        }
                                    }

                                    if (line.Contains(ERROR))
                                    {
                                        int tst = 0;
                                        if (int.TryParse(parts[4], out tst))
                                            lic.NumLicenses = tst;
                                        else
                                            throw new ArgumentException("Something wrong with license parsing!");
                                        inUse = 0;
                                        lic.Status = (int)LicenseStatus.ERROR;
                                    }
                                    else
                                    {
                                        int tst = 0;
                                        if (parts[8]!=TOTAL)
                                            throw new ArgumentException("Something wrong with license parsing!");
                                        if (int.TryParse(parts[5], out tst))
                                            lic.NumLicenses = tst;
                                        else 
                                            throw new ArgumentException("Something wrong with license parsing!");
                                        if (int.TryParse(parts[10], out tst))
                                            inUse = tst;
                                        else
                                            throw new ArgumentException("Something wrong with license parsing!");
                                        lic.Status = (int)LicenseStatus.ACTIVE;
                                    }
                                    found = 0;
                                    continue;
                                }
                                if (lic != null)
                                {
                                    string[] parts = null;
                                    if (line.Contains(lic.FeatureName) && line.Contains(srv.VendorName))
                                    {
                                        parts = line.Trim().Replace(":", "").Replace(",", "").Replace("\"", "").Replace("(", "").Replace(")", "").ToLower().Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                                        if (parts.Length > 1)
                                        {
                                            lic.Version = parts[1];
                                        }
                                    }
                                    if (line.Contains(FLOATING_LICENSE))
                                        lic.Type = (int)LicenseType.FLOATING;
                                    if (!string.IsNullOrWhiteSpace(line))
                                    {
                                        parts = line.Trim().Replace("(", "").Replace(")", "").ToLower().Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                                        if (parts.Length > 9)
                                        {
                                            srch = parts[1].ToLower();
                                            if (string.IsNullOrEmpty(srch))
                                                throw new ArgumentException("Host name can't be empty!");
                                            LicHost srvHost = db.LicHost.FirstOrDefault(h => h.Name == srch);
                                            if (srvHost == null)
                                            {
                                                srvHost = db.LicHost.Local.FirstOrDefault(h => h.Name == srch);
                                                if (srvHost == null)
                                                {
                                                    srvHost = new LicHost();
                                                    srvHost.Name = srch;
                                                    db.LicHost.Add(srvHost);
                                                    db.Entry(srvHost).State = System.Data.EntityState.Added;
                                                }
                                            }
                                            srch = parts[0].ToLower();
                                            if (string.IsNullOrEmpty(srch))
                                                throw new ArgumentException("User name can't be empty!");
                                            LicUser licUser = db.LicUser.FirstOrDefault(u => u.Name == srch);
                                            if (licUser == null)
                                            {
                                                licUser = db.LicUser.Local.FirstOrDefault(u => u.Name == srch);
                                                if (licUser == null)
                                                {
                                                    licUser = new LicUser();
                                                    licUser.Name = srch;
                                                    db.LicUser.Add(licUser);
                                                    db.Entry(licUser).State = System.Data.EntityState.Added;
                                                }
                                            }
                                            string version = parts[3];
                                            string dateStr = parts[7] + " " + parts[8] + "/" + runningTime.Year.ToString() + " " + parts[9];
                                            DateTime startTime = Convert.ToDateTime(dateStr, lmutilParserLocale);

                                            LicenseUsage usage = db.LicenseUsage.FirstOrDefault(u => u.License == lic.Id &&
                                                u.LicUser == licUser.Id && u.LicHost == srvHost.Id && u.Version == version && u.StartDate == startTime);
                                            if (usage == null)
                                            {
                                                usage = db.LicenseUsage.Local.FirstOrDefault(u => u.NavLicense.FeatureName == lic.FeatureName && u.NavLicense.NavLicServer.NavLicHost1.Name == lic.NavLicServer.NavLicHost1.Name && u.NavLicense.NavLicServer.LicPort1 == lic.NavLicServer.LicPort1 &&
                                                    u.NavLicUser.Name == licUser.Name && u.NavLicHost.Name == srvHost.Name && u.Version == version && u.StartDate == startTime);
                                                if (usage == null)
                                                {
                                                    usage = new LicenseUsage();
                                                    usage.NavLicense = lic;
                                                    usage.NavLicHost = srvHost;
                                                    usage.NavLicUser = licUser;
                                                    db.LicenseUsage.Add(usage);
                                                    db.Entry(usage).State = System.Data.EntityState.Added;
                                                }
                                            }
                                            usage.Version = version;
                                            usage.State = (int)LicenseState.OUT;
                                            usage.StartDate = startTime;
                                            usage.EndDate = runningTime;
                                            usage.Quantity = 1;
                                            if (parts.Length > 11)
                                            {
                                                usage.Quantity = Convert.ToInt32(parts[10]);
                                            }
                                            if (usage.Quantity.HasValue)
                                                found += usage.Quantity.Value;
                                        }
                                    }
                                }
                            }
                        }
                        //at the end of file. mark not found licenses from that server as IN

                        if (log.IsDebugEnabled)
                            log.Debug("Cleaning unused licenses");
                        if (srv != null)
                        {

                            foreach (LicenseUsage usage in
                                db.LicenseUsage.Where(u => u.NavLicense.NavLicServer.Id == srv.Id && u.EndDate != runningTime && u.State == (int)LicenseState.OUT).ToList())
                            {
                                usage.State = (int)LicenseState.IN;
                                usage.EndDate = runningTime;
                            }

                            foreach (LicenseUsage usage in
                                db.LicenseUsage.Local.Where(u => u.NavLicense.NavLicServer.NavLicHost1.Name == srv.NavLicHost1.Name && u.NavLicense.NavLicServer.LicPort1 == srv.LicPort1 && u.EndDate != runningTime && u.State == (int)LicenseState.OUT).ToList())
                            {
                                usage.State = (int)LicenseState.IN;
                                usage.EndDate = runningTime;
                            }

                        }

                    }
                    else if (fileType == 2)
                    {
                        //We parse a license file

                        if (line.StartsWith(SERVER))
                        {
                            serverNum++;
                            string[] parts = line.ToLower().Trim().Split(" ".ToCharArray(),StringSplitOptions.RemoveEmptyEntries);
                            srch = parts[1];
                            string hostId = parts[2];
                            if (string.IsNullOrEmpty(srch))
                                throw new ArgumentException("Server host name can't be empty!");
                            if (log.IsDebugEnabled)
                                log.Debug("Server: " + srch);
                            LicHost srvHost = db.LicHost.FirstOrDefault(h => h.Name == srch);
                            if (srvHost == null)
                            {
                                srvHost = db.LicHost.Local.FirstOrDefault(h => h.Name == srch);
                                if (srvHost == null)
                                {
                                    srvHost = new LicHost();
                                    srvHost.Name = srch;
                                    db.LicHost.Add(srvHost);
                                    db.Entry(srvHost).State = System.Data.EntityState.Added;
                                }
                            }
                            int port = Convert.ToInt32(parts[3]);

                            switch (serverNum)
                            { 
                                case 1:
                                srv = db.LicServer.FirstOrDefault(s => (s.LicHost1 == srvHost.Id && s.LicPort1 == port) ||
                                   (s.LicHost2 == srvHost.Id && s.LicPort2 == port) || (s.LicHost3 == srvHost.Id && s.LicPort3 == port));
                                if (srv == null)
                                {
                                    srv = db.LicServer.Local.FirstOrDefault(s => (s.NavLicHost1.Name == srvHost.Name && s.LicPort1 == port) ||
                                       (s.NavLicHost2 != null && s.NavLicHost2.Name == srvHost.Name && s.LicPort2 == port) ||
                                       (s.NavLicHost3 != null && s.NavLicHost3.Name == srvHost.Name && s.LicPort3 == port));
                                    if (srv == null)
                                    {
                                        srv = new LicServer();
                                        srv.Name = "Please specify";
                                        srv.NavLicHost1 = srvHost;
                                        srv.LicPort1 = port;
                                        db.LicServer.Add(srv);
                                        db.Entry(srv).State = System.Data.EntityState.Added;
                                    }
                                }
                                srv.LicHostId1 = hostId;
                                    break;
                                case 2:
                                    srv.NavLicHost2 = srvHost;
                                    srv.LicPort2 = port;
                                    srv.LicHostId2 = hostId;
                                    break;
                                case 3:
                                    srv.NavLicHost3 = srvHost;
                                    srv.LicPort3 = port;
                                    srv.LicHostId3 = hostId;
                                    break;
                                default:
                                    log.Error("Too muchservers found in license file!");

                                    break;
                            }
                        }
                        else if (line.StartsWith(VENDOR))
                        {

                            string[] parts = line.ToLower().Trim().Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                            if (parts.Length > 1) srv.VendorName = parts[1];
                            if (parts.Length > 2) srv.VendorPath = parts[2];
                            if (parts.Length > 3) srv.OptionsFile = parts[3];

                        }
                        else if (line.StartsWith(FEATURE) || line.StartsWith(INCREMENT))
                        {

                            string[] parts = line.ToLower().Trim().Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                            string licName = parts[1];
                            if (log.IsDebugEnabled)
                                log.Debug("License: " + licName);
                            License lic = db.License.FirstOrDefault(s => s.FeatureName == licName && s.LicServer == srv.Id);
                            if (lic == null)
                            {
                                lic = db.License.Local.FirstOrDefault(s => s.FeatureName == licName && s.NavLicServer.NavLicHost1.Name == srv.NavLicHost1.Name && s.NavLicServer.LicPort1 == srv.LicPort1);
                                if (lic == null)
                                {
                                    lic = new License();
                                    lic.NavLicServer = srv;
                                    lic.FeatureName = licName;
                                    db.License.Add(lic);
                                    db.Entry(lic).State = System.Data.EntityState.Added;
                                }
                            }
                            if (parts[4] == PERMANENT)
                            {
                                lic.OrderEndDate = DateTimeValues.SmallMaxValue;
                            }
                            else
                            {
                                if (parts[4].Contains("0000"))
                                {
                                    lic.OrderEndDate = DateTimeValues.SmallMaxValue;
                                }
                                else
                                {
                                    DateTime test;
                                    bool doneIt = false;
                                    foreach (string loc in Properties.Settings.Default.LicParserLocale)
                                    {
                                        licParserLocale = CultureInfo.GetCultureInfo(loc);
                                        if (DateTime.TryParse(parts[4], licParserLocale, DateTimeStyles.None, out test))
                                        {
                                            lic.OrderEndDate = test;
                                            doneIt = true;
                                            break;
                                        }
                                    }
                                    if (!doneIt && DateTime.TryParse(parts[4], CultureInfo.CurrentCulture, DateTimeStyles.None, out test))
                                    {
                                        lic.OrderEndDate = test;
                                        doneIt = true;
                                    }
                                    if (!doneIt && DateTime.TryParse(parts[4], CultureInfo.InvariantCulture, DateTimeStyles.None, out test))
                                    {
                                        lic.OrderEndDate = test;
                                        doneIt = true;
                                    }
                                    if (!doneIt)
                                    {
                                        lic.OrderEndDate = DateTimeValues.SmallMaxValue;
                                    }
                                }
                            }
                            lic.NumLicenses = Convert.ToInt32(parts[5]);
                            lic.Version = parts[3];
                        }

                    }

                }
                
            }
            if (fileType == 0)
            {
                log.Error("Don't know the type of that file!");
            }

            if (log.IsDebugEnabled)
                log.Debug("Parse Done");
        }
    }
}
