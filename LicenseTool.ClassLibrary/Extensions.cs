﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LicenseTool.ClassLibrary
{

    /// <summary>
    /// Holds min and max smalldatetime values
    /// </summary>
    public static class DateTimeValues
    {

        /// <summary>
        /// The minimum value for a smalldatetime
        /// </summary>
        public static DateTime SmallMinValue
        {
            get { return new DateTime(1900, 01, 01, 00, 00, 00); }
        }

        /// <summary>
        /// The maximum value for a smalldatetime
        /// </summary>
        public static DateTime SmallMaxValue
        {
            get { return new DateTime(2079, 06, 06, 23, 59, 00); }
        }

    }

}
