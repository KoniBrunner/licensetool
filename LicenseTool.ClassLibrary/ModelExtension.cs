﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data;
using System.ComponentModel.DataAnnotations;

namespace LicenseTool.ClassLibrary
{

    /// <summary>
    /// The concrete database context
    /// </summary>
    /// <seealso cref="ILicenseToolContext"/>
    public partial class LicenseToolDatabaseEntities : ILicenseToolContext
    {

        /// <summary>
        /// Set the modified state of an entity
        /// </summary>
        /// <param name="entity">The entity to be modified</param>
        /// <exception cref="ArgumentNullException">If <paramref name="entity"/> is <c>null</c></exception>
        public void SetModified(object entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            Entry(entity).State = EntityState.Modified;
        }

        /// <summary>
        /// Holding the entities for License
        /// </summary>
        /// <seealso cref="License"/>
        System.Data.Entity.IDbSet<License> ILicenseToolContext.License
        {
            get
            {
                return License;
            }
            set
            {
                License = (DbSet<License>)value;
            }
        }

        /// <summary>
        /// Holding the entities for LicenseUsage
        /// </summary>
        /// <seealso cref="LicenseUsage"/>
        System.Data.Entity.IDbSet<LicenseUsage> ILicenseToolContext.LicenseUsage
        {
            get
            {
                return LicenseUsage;
            }
            set
            {
                LicenseUsage = (DbSet<LicenseUsage>)value;
            }
        }

        /// <summary>
        /// Holding the entities for LicHost
        /// </summary>
        /// <seealso cref="LicHost"/>
        System.Data.Entity.IDbSet<LicHost> ILicenseToolContext.LicHost
        {
            get
            {
                return LicHost;
            }
            set
            {
                LicHost = (DbSet<LicHost>)value;
            }
        }

        /// <summary>
        /// Holding the entities for LicServer
        /// </summary>
        /// <seealso cref="LicServer"/>
        System.Data.Entity.IDbSet<LicServer> ILicenseToolContext.LicServer
        {
            get
            {
                return LicServer;
            }
            set
            {
                LicServer = (DbSet<LicServer>)value;
            }
        }

        /// <summary>
        /// Holding the entities for LicUser
        /// </summary>
        /// <seealso cref="LicUser"/>
        System.Data.Entity.IDbSet<LicUser> ILicenseToolContext.LicUser
        {
            get
            {
                return LicUser;
            }
            set
            {
                LicUser = (DbSet<LicUser>)value;
            }
        }

        /// <summary>
        /// Holding the entities for UsageReportView
        /// </summary>
        /// <seealso cref="UsageReportView"/>
        System.Data.Entity.IDbSet<UsageReportView> ILicenseToolContext.UsageReportView
        {
            get
            {
                return UsageReportView;
            }
            set
            {
                UsageReportView = (DbSet<UsageReportView>)value;
            }
        }

        /// <summary>
        /// Holding the entities for Group
        /// </summary>
        /// <seealso cref="Group"/>
        System.Data.Entity.IDbSet<Group> ILicenseToolContext.Group
        {
            get
            {
                return Group;
            }
            set
            {
                Group = (DbSet<Group>)value;
            }
        }

        /// <summary>
        /// Holding the entities for RelGroupUser
        /// </summary>
        /// <seealso cref="RelGroupUser"/>
        System.Data.Entity.IDbSet<RelGroupUser> ILicenseToolContext.RelGroupUser
        {
            get
            {
                return RelGroupUser;
            }
            set
            {
                RelGroupUser = (DbSet<RelGroupUser>)value;
            }
        }

        /// <summary>
        /// Holding the entities for SentMessages
        /// </summary>
        /// <seealso cref="SentMessages"/>
        System.Data.Entity.IDbSet<SentMessages> ILicenseToolContext.SentMessages
        {
            get
            {
                return SentMessages;
            }
            set
            {
                SentMessages = (DbSet<SentMessages>)value;
            }
        }
    }

    /// <summary>
    /// The entity describing a host on which a license server is running or from which a license has been used
    /// </summary>
    /// <remarks>For a description of the properties see <see cref="ILicHostMetaData"/></remarks>
    /// <seealso cref="ILicHostMetaData"/>
    [MetadataType(typeof(ILicHostMetaData))]
    public partial class LicHost
    {

        /// <summary>
        /// The fully qualified name of the host containing the domain name
        /// </summary>
        public string FullName
        {
            get
            {
                if (Name == null) return null;
                if (!Name.Contains(".")) return Name + "." + Properties.Settings.Default.Domain;
                return Name;
            }
        }

    }

    /// <summary>
    /// The entity describing a user who uses a license
    /// </summary>
    /// <remarks>For a description of the properties see <see cref="ILicUserMetaData"/></remarks>
    /// <seealso cref="ILicUserMetaData"/>
    [MetadataType(typeof(ILicUserMetaData))]
    public partial class LicUser
    {

        /// <summary>
        /// The display name of the user
        /// </summary>
        public string DisplayName
        {
            get
            {
                string ret = "";
                if (Name != null) ret += Name;
                if (FullName != null) ret += " (" + FullName + ")";
                return ret;
            }
        }

    }

    /// <summary>
    /// The entity describing a license server
    /// </summary>
    /// <remarks>For a description of the properties see <see cref="ILicServerMetaData"/></remarks>
    /// <seealso cref="ILicServerMetaData"/>
    [MetadataType(typeof(ILicServerMetaData))]
    public partial class LicServer
    {

    }

    /// <summary>
    /// The entity describing a license
    /// </summary>
    /// <remarks>For a description of the properties see <see cref="ILicenseMetaData"/></remarks>
    /// <seealso cref="ILicenseMetaData"/>
    [MetadataType(typeof(ILicenseMetaData))]
    public partial class License
    {

    }

    /// <summary>
    /// The entity describing used licenses
    /// </summary>
    /// <remarks>For a description of the properties see <see cref="ILicenseUsageMetaData"/></remarks>
    /// <seealso cref="ILicenseUsageMetaData"/>
    [MetadataType(typeof(ILicenseUsageMetaData))]
    public partial class LicenseUsage
    {

    }

    /// <summary>
    /// The entity to access the report view 
    /// </summary>
    /// <remarks>For a description of the properties see <see cref="IUsageReportViewMetaData"/></remarks>
    /// <seealso cref="IUsageReportViewMetaData"/>
    [MetadataType(typeof(IUsageReportViewMetaData))]
    public partial class UsageReportView
    {
    }

    /// <summary>
    /// The entity describing  already sent messages to prevent sending same message multiple times
    /// </summary>
    /// <remarks>For a description of the properties see <see cref="ISentMessagesMetaData"/></remarks>
    /// <seealso cref="ISentMessagesMetaData"/>
    [MetadataType(typeof(ISentMessagesMetaData))]
    public partial class SentMessages
    {
    }

    /// <summary>
    /// The entity describing the relation between a user and a group
    /// </summary>
    /// <remarks>For a description of the properties see <see cref="IRelGroupUserMetaData"/></remarks>
    /// <seealso cref="IRelGroupUserMetaData"/>
    [MetadataType(typeof(IRelGroupUserMetaData))]
    public partial class RelGroupUser
    {
    }

    /// <summary>
    /// The entity describing a group of users to build reports for
    /// </summary>
    /// <remarks>For a description of the properties see <see cref="IGroupMetaData"/></remarks>
    /// <seealso cref="IGroupMetaData"/>
    [MetadataType(typeof(IGroupMetaData))]
    public partial class Group
    {
    }

}
