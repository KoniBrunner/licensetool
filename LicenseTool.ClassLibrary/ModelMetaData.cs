﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.ComponentModel.DataAnnotations;
using res = LicenseTool.ResourceLibrary.Model;

namespace LicenseTool.ClassLibrary
{

    /// <summary>
    /// Interface for the model annotation of the <see cref="Group"/> entity
    /// </summary>
    /// <seealso cref="Group"/>
    public interface IGroupMetaData
    {
        /// <summary>
        /// The unique identifyer of that entity
        /// </summary>
        [Display(Name = "Id", ResourceType = typeof(res))]
        int Id { get; set; }

        /// <summary>
        /// The description of the group
        /// </summary>
        [Display(Name = "Description", ResourceType = typeof(res))]
        string Description { get; set; }

        /// <summary>
        /// The name of the group
        /// </summary>
        [Display(Name = "Name", ResourceType = typeof(res))]
        [Required(ErrorMessageResourceName = "NameRequiredError", ErrorMessageResourceType = typeof(res))]
        string Name { get; set; }

        /// <summary>
        /// Navigation property
        /// </summary>
        [Display(Name = "Users", ResourceType = typeof(res))]
        System.Collections.Generic.ICollection<RelGroupUser> NavRelGroupUser { get; set; }

    }

    /// <summary>
    /// Interface for the model annotation of the <see cref="License"/> entity
    /// </summary>
    /// <seealso cref="License"/>
    public interface ILicenseMetaData
    {
        /// <summary>
        /// The unique identifyer of that entity
        /// </summary>
        [Display(Name = "Id", ResourceType = typeof(res))]
        int Id { get; set; }

        /// <summary>
        /// The feature name of the license
        /// </summary>
        [Display(Name = "FeatureName", ResourceType = typeof(res))]
        [Required(ErrorMessageResourceName = "FeatureNameRequiredError", ErrorMessageResourceType = typeof(res))]
        string FeatureName { get; set; }

        /// <summary>
        /// The license server delivering that license
        /// </summary>
        [Display(Name = "LicServer", ResourceType = typeof(res))]
        [Required(ErrorMessageResourceName = "LicServerRequiredError", ErrorMessageResourceType = typeof(res))]
        int LicServer { get; set; }

        /// <summary>
        /// Navigation property
        /// </summary>
        [Display(Name = "LicenseUsage", ResourceType = typeof(res))]
        System.Collections.Generic.ICollection<LicenseUsage> NavLicenseUsage { get; set; }

        /// <summary>
        /// Navigation property
        /// </summary>
        [Display(Name = "LicServer", ResourceType = typeof(res))]
        LicServer NavLicServer { get; set; }

        /// <summary>
        /// The number of available licenses
        /// </summary>
        [Display(Name = "NumLicenses", ResourceType = typeof(res))]
        [Required(ErrorMessageResourceName = "NumLicensesRequiredError", ErrorMessageResourceType = typeof(res))]
        int? NumLicenses { get; set; }

        /// <summary>
        /// The expiration date of that license
        /// </summary>
        [Display(Name = "OrderEndDate", ResourceType = typeof(res))]
        DateTime? OrderEndDate { get; set; }

        /// <summary>
        /// The name for order perposes. Could be as an example an order number from the vendor
        /// </summary>
        [Display(Name = "OrderName", ResourceType = typeof(res))]
        string OrderName { get; set; }

        /// <summary>
        /// The date, the license gets valid
        /// </summary>
        [Display(Name = "OrderStartDate", ResourceType = typeof(res))]
        DateTime? OrderStartDate { get; set; }

        /// <summary>
        /// The status of the license
        /// </summary>
        /// <seealso cref="LicenseStatus"/>
        [Display(Name = "Status", ResourceType = typeof(res))]
        [UIHintEnumType("Enum", typeof(LicenseStatus))]
        int? Status { get; set; }

        /// <summary>
        /// The type of the license
        /// </summary>
        /// <seealso cref="LicenseType"/>
        [Display(Name = "Type", ResourceType = typeof(res))]
        [UIHintEnumType("Enum", typeof(LicenseType))]
        int? Type { get; set; }

        /// <summary>
        /// The version of the license
        /// </summary>
        [Display(Name = "Version", ResourceType = typeof(res))]
        string Version { get; set; }
    }

    /// <summary>
    /// Interface for the model annotation of the <see cref="LicenseUsage"/> entity
    /// </summary>
    /// <seealso cref="LicenseUsage"/>
    public interface ILicenseUsageMetaData
    {
        /// <summary>
        /// The unique identifyer of that entity
        /// </summary>
        [Display(Name = "Id", ResourceType = typeof(res))]
        int Id { get; set; }

        /// <summary>
        /// The date the license has been checked in back or latest tested
        /// </summary>
        [Display(Name = "EndDate", ResourceType = typeof(res))]
        DateTime? EndDate { get; set; }

        /// <summary>
        /// The used license
        /// </summary>
        [Display(Name = "License", ResourceType = typeof(res))]
        [Required(ErrorMessageResourceName = "LicenseRequiredError", ErrorMessageResourceType = typeof(res))]
        int License { get; set; }

        /// <summary>
        /// The host used that license
        /// </summary>
        [Display(Name = "LicHost", ResourceType = typeof(res))]
        [Required(ErrorMessageResourceName = "LicHostRequiredError", ErrorMessageResourceType = typeof(res))]
        int LicHost { get; set; }

        /// <summary>
        /// The user used that license
        /// </summary>
        [Display(Name = "LicUser", ResourceType = typeof(res))]
        [Required(ErrorMessageResourceName = "LicUserRequiredError", ErrorMessageResourceType = typeof(res))]
        int LicUser { get; set; }

        /// <summary>
        /// Navigation property
        /// </summary>
        [Display(Name = "License", ResourceType = typeof(res))]
        License NavLicense { get; set; }

        /// <summary>
        /// Navigation property
        /// </summary>
        [Display(Name = "LicHost", ResourceType = typeof(res))]
        LicHost NavLicHost { get; set; }

        /// <summary>
        /// Navigation property
        /// </summary>
        [Display(Name = "LicUser", ResourceType = typeof(res))]
        LicUser NavLicUser { get; set; }

        /// <summary>
        /// The number of used licenses
        /// </summary>
        [Display(Name = "Quantity", ResourceType = typeof(res))]
        [Required(ErrorMessageResourceName = "QuantityRequiredError", ErrorMessageResourceType = typeof(res))]
        int? Quantity { get; set; }

        /// <summary>
        /// The date the license was first checked out
        /// </summary>
        [Display(Name = "StartDate", ResourceType = typeof(res))]
        DateTime? StartDate { get; set; }

        /// <summary>
        /// The state of the license
        /// </summary>
        /// <seealso cref="LicenseState"/>
        [Display(Name = "State", ResourceType = typeof(res))]
        [UIHintEnumType("Enum", typeof(LicenseState))]
        int? State { get; set; }

        /// <summary>
        /// The version of the license
        /// </summary>
        [Display(Name = "Version", ResourceType = typeof(res))]
        string Version { get; set; }
    }

    /// <summary>
    /// Interface for the model annotation of the <see cref="LicHost"/> entity
    /// </summary>
    /// <seealso cref="LicHost"/>
    public interface ILicHostMetaData
    {
        /// <summary>
        /// The unique identifyer of that entity
        /// </summary>
        [Display(Name = "Id", ResourceType = typeof(res))]
        int Id { get; set; }

        /// <summary>
        /// The fully qualified name of the host
        /// </summary>
        [Display(Name = "FullName", ResourceType = typeof(res))]
        string FullName { get; set; }

        /// <summary>
        /// The host name
        /// </summary>
        [Display(Name = "Name", ResourceType = typeof(res))]
        [Required(ErrorMessageResourceName = "NameRequiredError", ErrorMessageResourceType = typeof(res))]
        string Name { get; set; }

        /// <summary>
        /// Navigation property
        /// </summary>
        [Display(Name = "LicenseUsage", ResourceType = typeof(res))]
        System.Collections.Generic.ICollection<LicenseUsage> NavLicenseUsage { get; set; }

        /// <summary>
        /// Navigation property
        /// </summary>
        [Display(Name = "LicServer1", ResourceType = typeof(res))]
        System.Collections.Generic.ICollection<LicServer> NavLicServer1 { get; set; }

        /// <summary>
        /// Navigation property
        /// </summary>
        [Display(Name = "LicServer2", ResourceType = typeof(res))]
        System.Collections.Generic.ICollection<LicServer> NavLicServer2 { get; set; }

        /// <summary>
        /// Navigation property
        /// </summary>
        [Display(Name = "LicServer3", ResourceType = typeof(res))]
        System.Collections.Generic.ICollection<LicServer> NavLicServer3 { get; set; }
    }

    /// <summary>
    /// Interface for the model annotation of the <see cref="LicServer"/> entity
    /// </summary>
    /// <seealso cref="LicServer"/>
    public interface ILicServerMetaData
    {
        /// <summary>
        /// The unique identifyer of that entity
        /// </summary>
        [Display(Name = "Id", ResourceType = typeof(res))]
        int Id { get; set; }

        /// <summary>
        /// The license file on the 1. host
        /// </summary>
        [Display(Name = "LicFile1", ResourceType = typeof(res))]
        string LicFile1 { get; set; }

        /// <summary>
        /// The license file on the 2. host if any
        /// </summary>
        [Display(Name = "LicFile2", ResourceType = typeof(res))]
        string LicFile2 { get; set; }

        /// <summary>
        /// The license file on the 3. host if any
        /// </summary>
        [Display(Name = "LicFile3", ResourceType = typeof(res))]
        string LicFile3 { get; set; }

        /// <summary>
        /// The 1. host used for that license server
        /// </summary>
        [Display(Name = "LicHost1", ResourceType = typeof(res))]
        [Required(ErrorMessageResourceName = "LicHost1RequiredError", ErrorMessageResourceType = typeof(res))]
        int LicHost1 { get; set; }

        /// <summary>
        /// The 2. host used for that license server if any
        /// </summary>
        [Display(Name = "LicHost2", ResourceType = typeof(res))]
        int? LicHost2 { get; set; }

        /// <summary>
        /// The 3. host used for that license server if any
        /// </summary>
        [Display(Name = "LicHost3", ResourceType = typeof(res))]
        int? LicHost3 { get; set; }

        /// <summary>
        /// The HostID of the 1. host
        /// </summary>
        [Display(Name = "LicHostId1", ResourceType = typeof(res))]
        string LicHostId1 { get; set; }

        /// <summary>
        /// The HostID of the 2. host
        /// </summary>
        [Display(Name = "LicHostId2", ResourceType = typeof(res))]
        string LicHostId2 { get; set; }

        /// <summary>
        /// The HostID of the 3. host
        /// </summary>
        [Display(Name = "LicHostId3", ResourceType = typeof(res))]
        string LicHostId3 { get; set; }

        /// <summary>
        /// The port on the 1. host
        /// </summary>
        [Display(Name = "LicPort1", ResourceType = typeof(res))]
        int LicPort1 { get; set; }

        /// <summary>
        /// The port on the 2. host
        /// </summary>
        [Display(Name = "LicPort2", ResourceType = typeof(res))]
        int? LicPort2 { get; set; }

        /// <summary>
        /// The port on the 3. host
        /// </summary>
        [Display(Name = "LicPort3", ResourceType = typeof(res))]
        int? LicPort3 { get; set; }

        /// <summary>
        /// The name of the license server
        /// </summary>
        [Display(Name = "Name", ResourceType = typeof(res))]
        [Required(ErrorMessageResourceName = "NameRequiredError", ErrorMessageResourceType = typeof(res))]
        string Name { get; set; }

        /// <summary>
        /// Navigation property
        /// </summary>
        [Display(Name = "License", ResourceType = typeof(res))]
        System.Collections.Generic.ICollection<License> NavLicense { get; set; }

        /// <summary>
        /// Navigation property
        /// </summary>
        [Display(Name = "LicHost1", ResourceType = typeof(res))]
        LicHost NavLicHost1 { get; set; }

        /// <summary>
        /// Navigation property
        /// </summary>
        [Display(Name = "LicHost2", ResourceType = typeof(res))]
        LicHost NavLicHost2 { get; set; }

        /// <summary>
        /// Navigation property
        /// </summary>
        [Display(Name = "LicHost3", ResourceType = typeof(res))]
        LicHost NavLicHost3 { get; set; }

        /// <summary>
        /// Path to the option file if any
        /// </summary>
        [Display(Name = "OptionsFile", ResourceType = typeof(res))]
        string OptionsFile { get; set; }

        /// <summary>
        /// The status of the license server
        /// </summary>
        /// <seealso cref="ServerAndVendorStatus"/>
        [UIHintEnumType("Enum", typeof(ServerAndVendorStatus))]
        [Display(Name = "ServerStatus", ResourceType = typeof(res))]
        short? ServerStatus { get; set; }

        /// <summary>
        /// The license server version
        /// </summary>
        [Display(Name = "Version", ResourceType = typeof(res))]
        string ServerVersion { get; set; }

        /// <summary>
        /// The vendor daemon name
        /// </summary>
        [Display(Name = "VendorName", ResourceType = typeof(res))]
        [Required(ErrorMessageResourceName = "VendorNameRequiredError", ErrorMessageResourceType = typeof(res))]
        string VendorName { get; set; }

        /// <summary>
        /// The vendor daemon path
        /// </summary>
        [Display(Name = "VendorPath", ResourceType = typeof(res))]
        string VendorPath { get; set; }

        /// <summary>
        /// The status of the vendor daemon
        /// </summary>
        /// <seealso cref="ServerAndVendorStatus"/>
        [UIHintEnumType("Enum", typeof(ServerAndVendorStatus))]
        [Display(Name = "VendorStatus", ResourceType = typeof(res))]
        short? VendorStatus { get; set; }

        /// <summary>
        /// The vendor daemon version
        /// </summary>
        [Display(Name = "VendorVersion", ResourceType = typeof(res))]
        string VendorVersion { get; set; }
    }

    /// <summary>
    /// Interface for the model annotation of the <see cref="LicUser"/> entity
    /// </summary>
    /// <seealso cref="LicUser"/>
    public interface ILicUserMetaData
    {
        /// <summary>
        /// The unique identifyer of that entity
        /// </summary>
        [Display(Name = "Id", ResourceType = typeof(res))]
        int Id { get; set; }

        /// <summary>
        /// The display name of the user
        /// </summary>
        [Display(Name = "DisplayName", ResourceType = typeof(res))]
        string DisplayName { get; set; }

        /// <summary>
        /// The email address of the user
        /// </summary>
        [Display(Name = "Email", ResourceType = typeof(res))]
        string Email { get; set; }

        /// <summary>
        /// The full name of the user
        /// </summary>
        [Display(Name = "FullName", ResourceType = typeof(res))]
        string FullName { get; set; }

        /// <summary>
        /// The name of the user
        /// </summary>
        [Display(Name = "Name", ResourceType = typeof(res))]
        [Required(ErrorMessageResourceName = "NameRequiredError", ErrorMessageResourceType = typeof(res))]
        string Name { get; set; }

        /// <summary>
        /// Navigation property
        /// </summary>
        [Display(Name = "LicenseUsage", ResourceType = typeof(res))]
        System.Collections.Generic.ICollection<LicenseUsage> NavLicenseUsage { get; set; }

        /// <summary>
        /// Navigation property
        /// </summary>
        [Display(Name = "RelGroupUser", ResourceType = typeof(res))]
        System.Collections.Generic.ICollection<RelGroupUser> NavRelGroupUser { get; set; }

        /// <summary>
        /// The phone number of the user
        /// </summary>
        [Display(Name = "PhoneNumber", ResourceType = typeof(res))]
        string PhoneNumber { get; set; }
    }

    /// <summary>
    /// Interface for the model annotation of the <see cref="RelGroupUser"/> entity
    /// </summary>
    /// <seealso cref="RelGroupUser"/>
    public interface IRelGroupUserMetaData
    {
        /// <summary>
        /// The unique identifyer of that entity
        /// </summary>
        [Display(Name = "Id", ResourceType = typeof(res))]
        int Id { get; set; }

        /// <summary>
        /// The group
        /// </summary>
        [Display(Name = "Group", ResourceType = typeof(res))]
        [Required(ErrorMessageResourceName = "GroupIdRequiredError", ErrorMessageResourceType = typeof(res))]
        int GroupId { get; set; }

        /// <summary>
        /// Navigation property
        /// </summary>
        [Display(Name = "Group", ResourceType = typeof(res))]
        Group NavGroup { get; set; }

        /// <summary>
        /// Navigation property
        /// </summary>
        [Display(Name = "User", ResourceType = typeof(res))]
        LicUser NavLicUser { get; set; }

        /// <summary>
        /// The user
        /// </summary>
        [Display(Name = "User", ResourceType = typeof(res))]
        [Required(ErrorMessageResourceName = "UserIdRequiredError", ErrorMessageResourceType = typeof(res))]
        int UserId { get; set; }
    }

    /// <summary>
    /// Interface for the model annotation of the <see cref="SentMessages"/> entity
    /// </summary>
    /// <seealso cref="SentMessages"/>
    public interface ISentMessagesMetaData
    {
        /// <summary>
        /// The unique identifyer of that entity
        /// </summary>
        [Display(Name = "Id", ResourceType = typeof(res))]
        int Id { get; set; }

        /// <summary>
        /// An MD5 hash built with the message body to prevent sending same message multiple times
        /// </summary>
        [Display(Name = "MessageMD5", ResourceType = typeof(res))]
        [Required(ErrorMessageResourceName = "MessageMD5RequiredError", ErrorMessageResourceType = typeof(res))]
        string MessageMD5 { get; set; }

        /// <summary>
        /// The plugin generated that message
        /// </summary>
        [Display(Name = "PluginName", ResourceType = typeof(res))]
        [Required(ErrorMessageResourceName = "PluginNameRequiredError", ErrorMessageResourceType = typeof(res))]
        string PluginName { get; set; }

        /// <summary>
        /// The timestamp the message was last sent
        /// </summary>
        [Display(Name = "Timestamp", ResourceType = typeof(res))]
        [Required(ErrorMessageResourceName = "TimestampRequiredError", ErrorMessageResourceType = typeof(res))]
        DateTime Timestamp { get; set; }
    }

    /// <summary>
    /// Interface for the model annotation of the <see cref="UsageReportView"/> entity
    /// </summary>
    /// <seealso cref="UsageReportView"/>
    public interface IUsageReportViewMetaData
    {
        /// <summary>
        /// The day the license is used
        /// </summary>
        /// <remarks>The day is DBNull for report for year</remarks>
        [Display(Name = "Day", ResourceType = typeof(res))]
        int Day { get; set; }

        /// <summary>
        /// The used license
        /// </summary>
        [Display(Name = "License", ResourceType = typeof(res))]
        int FeatureId { get; set; }

        /// <summary>
        /// The hour the license is used
        /// </summary>
        /// <remarks>The hour is DBNull for report for month or year</remarks>
        [Display(Name = "Hour", ResourceType = typeof(res))]
        int Hour { get; set; }

        /// <summary>
        /// The minute the license is used
        /// </summary>
        /// <remarks>The minute is DBNull for report for month or year</remarks>
        [Display(Name = "Minute", ResourceType = typeof(res))]
        int Minute { get; set; }

        /// <summary>
        /// The month the license is used
        /// </summary>
        [Display(Name = "Month", ResourceType = typeof(res))]
        int Month { get; set; }

        /// <summary>
        /// The quantity of used licenses
        /// </summary>
        [Display(Name = "Quantity", ResourceType = typeof(res))]
        int? Quantity { get; set; }

        /// <summary>
        /// The year the license is used
        /// </summary>
        [Display(Name = "Year", ResourceType = typeof(res))]
        int Year { get; set; }
    }

}