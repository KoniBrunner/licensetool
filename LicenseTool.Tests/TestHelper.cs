﻿using LicenseTool.ClassLibrary;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;

namespace LicenseTool.Tests
{

    /// <summary>
    /// Helper class for tests
    /// </summary>
    public class TestHelper
    {

        /// <summary>
        /// Fakes a DB context
        /// </summary>
        /// <returns>A faked database context with some example data stored on it</returns>
        public static FakeLicenseToolDatabaseEntities FakeDbContext()
        {
            FakeLicenseToolDatabaseEntities context = new FakeLicenseToolDatabaseEntities()
            {
                LicServer =
                {
                    new LicServer { Id = 1, Name = "Server1", LicHost1 = 1},
                    new LicServer { Id = 2, Name = "Server2", LicHost1 = 2},
                    new LicServer { Id = 3, Name = "Server3", LicHost1 = 3}
                },
                License =
                {
                    new License { Id = 1, FeatureName = "Feature1Server1", LicServer = 1, NumLicenses = 1, OrderEndDate = DateTime.Now.AddMonths(1), Type = 0, Status = 0, Version = "1.0"},
                    new License { Id = 2, FeatureName = "Feature1Server2", LicServer = 2, NumLicenses = 1, OrderEndDate = DateTime.Now.AddMonths(1), Type = 0, Status = 0, Version = "1.0"},
                    new License { Id = 3, FeatureName = "Feature1Server3", LicServer = 3, NumLicenses = 1, OrderEndDate = DateTime.Now.AddMonths(1), Type = 0, Status = 0, Version = "1.0"}
                },
                Group = 
                {
                    new Group { Id = 1, Name = "Group1", Description = "Desc1"},
                    new Group { Id = 2, Name = "Group2", Description = "Desc2"},
                    new Group { Id = 3, Name = "Group3", Description = "Desc3"}
                },
                LicUser =
                {
                    new LicUser { Id = 1, Name = "LicUser1", FullName = "FullName1"},
                    new LicUser { Id = 2, Name = "LicUser2", FullName = "FullName2"},
                    new LicUser { Id = 3, Name = "LicUser3", FullName = "FullName3"}
                },
                LicHost =
                {
                    new LicHost { Id = 1, Name = "LicHost1"},
                    new LicHost { Id = 2, Name = "LicHost2"},
                    new LicHost { Id = 3, Name = "LicHost3"}
                },
                RelGroupUser =
                {
                    new RelGroupUser { Id = 1, GroupId = 1, UserId = 1},
                    new RelGroupUser { Id = 2, GroupId = 2, UserId = 2},
                    new RelGroupUser { Id = 3, GroupId = 3, UserId = 3}
                },
                LicenseUsage =
                {
                    new LicenseUsage { Id = 1, License = 1, LicHost = 1, LicUser = 1, Quantity = 1, StartDate = DateTime.Now.AddHours(-2), EndDate = DateTime.Now.AddMinutes(-10), Version = "1.0", State = 1},
                    new LicenseUsage { Id = 2, License = 2, LicHost = 2, LicUser = 2, Quantity = 1, StartDate = DateTime.Now.AddHours(-2), EndDate = DateTime.Now.AddMinutes(-10), Version = "2.0", State = 1},
                    new LicenseUsage { Id = 3, License = 3, LicHost = 3, LicUser = 3, Quantity = 1, StartDate = DateTime.Now.AddHours(-2), EndDate = DateTime.Now.AddMinutes(-10), Version = "3.0", State = 1}
                }
            };
            context.License.Local[0].NavLicServer = context.LicServer.Local[0];
            context.License.Local[1].NavLicServer = context.LicServer.Local[1];
            context.License.Local[2].NavLicServer = context.LicServer.Local[2];
            context.LicServer.Local[0].NavLicHost1 = context.LicHost.Local[0];
            context.LicServer.Local[1].NavLicHost1 = context.LicHost.Local[1];
            context.LicServer.Local[2].NavLicHost1 = context.LicHost.Local[2];
            context.LicenseUsage.Local[0].NavLicense = context.License.Local[0];
            context.LicenseUsage.Local[0].NavLicHost = context.LicHost.Local[0];
            context.LicenseUsage.Local[0].NavLicUser = context.LicUser.Local[0];
            context.LicenseUsage.Local[1].NavLicense = context.License.Local[1];
            context.LicenseUsage.Local[1].NavLicHost = context.LicHost.Local[1];
            context.LicenseUsage.Local[1].NavLicUser = context.LicUser.Local[1];
            context.LicenseUsage.Local[2].NavLicense = context.License.Local[2];
            context.LicenseUsage.Local[2].NavLicHost = context.LicHost.Local[2];
            context.LicenseUsage.Local[2].NavLicUser = context.LicUser.Local[2];
            return context;
        }

        /// <summary>
        /// Builds a faked controller context
        /// </summary>
        /// <param name="controller">The controller the returned context should be designed for</param>
        /// <returns>A faked controller context</returns>
        public static ControllerContext FakeControllerContext(Controller controller)
        {
            var routeData = new RouteData();
            var wrapper = new HttpContextWrapper(FakeHttpContext());
            var requestContext = new RequestContext(wrapper, routeData);
            var controllerContext = new ControllerContext(requestContext, controller);
            return controllerContext;
        }

        /// <summary>
        /// Builds a faked http context
        /// </summary>
        /// <remarks>
        /// The request url is always http://localhost/
        /// </remarks>
        /// <returns>A faked http context</returns>
        public static HttpContext FakeHttpContext()
        {
            var httpRequest = new HttpRequest("", "http://localhost/", "");
            var stringWriter = new StringWriter();
            var httpResponce = new HttpResponse(stringWriter);
            var httpContext = new HttpContext(httpRequest, httpResponce);

            var identity = new GenericIdentity("TestUser");
            var user = new GenericPrincipal(identity, null);
            httpContext.User = user;
            var sessionContainer = new HttpSessionStateContainer("id",
                new SessionStateItemCollection(), new HttpStaticObjectsCollection(), 10,
                true, HttpCookieMode.AutoDetect, SessionStateMode.InProc, false);
            SessionStateUtility.AddHttpSessionStateToContext(httpContext, sessionContainer);
            return httpContext;
        }

        /// <summary>
        /// Builds a moq controller context
        /// </summary>
        /// <param name="controller">The controller the returned context should be designed for</param>
        /// <param name="fileInUpload">A filestream to a file to be uploaded</param>
        /// <returns>A moq controller context</returns>
        public static ControllerContext MoqControllerContext(Controller controller, FileStream fileInUpload = null)
        {
            var routeData = new RouteData();
            var requestContext = new RequestContext(MoqHttpContext(fileInUpload), routeData);
            var controllerContext = new ControllerContext(requestContext, controller);
            return controllerContext;
        }

        /// <summary>
        /// Builds a moq http context
        /// </summary>
        /// <param name="uploadFile">The controller the returned context should be designed for</param>
        /// <param name="httpMethod">The http method of the request</param>
        /// <returns>A moq http context</returns>
        public static HttpContextBase MoqHttpContext(FileStream uploadFile = null, string httpMethod = "GET")
        {
            var request = new Mock<HttpRequestBase>();
            request.Setup(r => r.HttpMethod).Returns(httpMethod);
            request.Setup(r => r.AppRelativeCurrentExecutionFilePath).Returns("/");
            request.Setup(r => r.ApplicationPath).Returns("/");

            var response = new Mock<HttpResponseBase>();
            response.Setup(s => s.ApplyAppPathModifier(It.IsAny<string>())).Returns<string>(s => s);
            response.SetupProperty(res => res.StatusCode, (int)System.Net.HttpStatusCode.OK);

            var server = new Mock<HttpServerUtilityBase>(MockBehavior.Loose);

            FileInfo assy = new FileInfo(typeof(TestHelper).Assembly.Location);
            DirectoryInfo solutionDir = assy.Directory.Parent.Parent.Parent;
            string rootDir = Path.Combine(new string[] { solutionDir.FullName, "WebApplication" });
            server.Setup(i => i.MapPath(It.IsAny<string>()))
               .Returns((string a) => a.Replace("~/", rootDir+"\\").Replace("/", @"\"));

            var session = new Mock<HttpSessionStateBase>();

            var httpContext = new Mock<HttpContextBase>();
            httpContext.Setup(c => c.Request).Returns(request.Object);
            httpContext.Setup(c => c.Response).Returns(response.Object);
            httpContext.Setup(c => c.Session).Returns(session.Object);
            httpContext.Setup(c => c.Server).Returns(server.Object);

            var identity = new GenericIdentity("TestUser");
            var user = new GenericPrincipal(identity, null);
            httpContext.Setup(c => c.User).Returns(user);

            if (uploadFile != null)
            {
                var file = new Mock<HttpPostedFileBase>();
                var files = new Mock<HttpFileCollectionBase>();
                files.Setup(x => x.Count).Returns(1);
                file.Setup(x => x.InputStream).Returns(uploadFile);
                file.Setup(x => x.ContentLength).Returns((int)uploadFile.Length);
                file.Setup(x => x.FileName).Returns(uploadFile.Name);
                file.Setup(x => x.SaveAs(It.IsAny<string>())).Callback((string filename) => OurSaveAs(uploadFile, filename));
                files.Setup(x => x.Get(0).InputStream).Returns(file.Object.InputStream);
                request.Setup(x => x.Files).Returns(files.Object);
                request.Setup(x => x.Files.Count).Returns(1);
                request.Setup(x => x.Files[0]).Returns(file.Object);
            }
            else
            {
                var files = new Mock<HttpFileCollectionBase>();
                files.Setup(x => x.Count).Returns(0);
                request.Setup(x => x.Files).Returns(files.Object);
                request.Setup(x => x.Files.Count).Returns(0);
            }

            return httpContext.Object;
        }

        /// <summary>
        /// Saves a file stream to a file
        /// </summary>
        /// <param name="uploadFile">The upload file stream</param>
        /// <param name="filename">The file to be created</param>
        private static void OurSaveAs(FileStream uploadFile, string filename)
        {
            FileInfo outFile = new FileInfo(filename);
            FileStream outStr = outFile.OpenWrite();
            uploadFile.CopyTo(outStr);
            outStr.Close();
        }

        /// <summary>
        /// Execute OnActionExecuting on a controller
        /// </summary>
        /// <param name="controller">The controller on which the action has to be executed</param>
        public static void ExecuteOnActionExecuting(Controller controller)
        {
            // Arrange
            var actionDescriptor = new MockRepository(MockBehavior.Default).OneOf<ActionDescriptor>();
            var ctx1 = new ActionExecutingContext(controller.ControllerContext,
                actionDescriptor, new Dictionary<string, object>());
            MethodInfo onActionExecuting = controller.GetType().GetMethod(
                "OnActionExecuting", BindingFlags.Instance | BindingFlags.NonPublic);

            // Act
            onActionExecuting.Invoke(controller, new object[] { ctx1 });
        }

    }
}
