﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LicenseTool.Tests
{
    /// <summary>
    /// The unit tests
    /// </summary>
    class NamespaceDoc
    {
    }
}

namespace LicenseTool.Tests.Controllers
{
    /// <summary>
    /// The unit tests for the web controllers
    /// </summary>
    class NamespaceDoc
    {
    }
}

namespace LicenseTool.Tests.Misc
{
    /// <summary>
    /// Misc helper classes for unit tests
    /// </summary>
    class NamespaceDoc
    {
    }
}
