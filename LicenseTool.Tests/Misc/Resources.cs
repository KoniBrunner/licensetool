﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using System.Threading;

namespace LicenseTool.Tests.Misc
{

    /// <summary>
    /// Test class with tests for resources
    /// </summary>
    [TestClass]
    public class Resources
    {

        /// <summary>
        /// Tests the culture en-GB
        /// </summary>
        [TestMethod]
        public void Resources_Culture_enGB()
        {
            TestCulture("en", "GB");
        }

        /// <summary>
        /// Tests the culture de-CH
        /// </summary>
        [TestMethod]
        public void Resources_Culture_deCH()
        {
            TestCulture("de", "CH");
        }

        /// <summary>
        /// Tests a given culture
        /// </summary>
        /// <param name="language">The language of the culture</param>
        /// <param name="country">The two letter ISO code of the country</param>
        private void TestCulture(string language, string country)
        {
            // Arrange
            CultureInfo thc = Thread.CurrentThread.CurrentCulture;
            CultureInfo uic = Thread.CurrentThread.CurrentUICulture;

            CultureInfo en_thc = CultureInfo.GetCultureInfo(language + "-" + country);
            CultureInfo en_uic = CultureInfo.GetCultureInfo(language + "-" + country);

            // Act
            Thread.CurrentThread.CurrentCulture = en_thc;
            Thread.CurrentThread.CurrentUICulture = en_uic;
            string tstCommonEn = LicenseTool.ResourceLibrary.Common.CurrentLanguage;
            string tstModelEn = LicenseTool.ResourceLibrary.Model.CurrentLanguage;
            string tstEnumEn = LicenseTool.ResourceLibrary.Enum.CurrentLanguage;
            Thread.CurrentThread.CurrentCulture = thc;
            Thread.CurrentThread.CurrentUICulture = uic;

            // Assert
            Assert.AreEqual(tstCommonEn, language);
            Assert.AreEqual(tstModelEn, language);
            Assert.AreEqual(tstEnumEn, language);
        }

    }
}
