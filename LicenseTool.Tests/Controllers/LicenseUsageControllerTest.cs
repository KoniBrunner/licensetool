﻿using LicenseTool.ClassLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using LicenseTool.WebApplication.Controllers;
using LicenseTool.WebApplication.Models;

namespace LicenseTool.Tests.Controllers
{

    /// <summary>
    /// Test class for the LicenseUsage Controller
    /// </summary>
    [TestClass]
    public class LicenseUsageControllerTest
    {
        private LicenseUsageController controller = null;
        private FakeLicenseToolDatabaseEntities db = null;

        /// <summary>
        /// Initializes this test with the fake contexts
        /// </summary>
        [TestInitialize]
        public void LicenseUsageController_Initialize()
        {
            db = TestHelper.FakeDbContext();
            controller = new LicenseUsageController(db);
            controller.ControllerContext = TestHelper.MoqControllerContext(controller);
        }

        /// <summary>
        /// Cleans up all resources of this test
        /// </summary>
        [TestCleanup]
        public void LicenseUsageController_Cleanup()
        {
            controller.Dispose();
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicenseUsageController_Index_Returns_ViewResult()
        {
            // Arrange

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);

            Assert.IsTrue(result.ViewData.ContainsKey("AllLicServers"));
            Assert.IsTrue(result.ViewData.ContainsKey("AllStates"));
            Assert.IsTrue(result.ViewData.ContainsKey("AllLicenses"));
            Assert.IsTrue(result.ViewData.ContainsKey("ShownItems"));
            Assert.IsInstanceOfType(result.ViewData["ShownItems"], typeof(List<LicenseUsage>));
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicenseUsageController_Index_WithSearchModel_Returns_ViewResult_WithModel()
        {
            // Arrange
            LicenseUsageSearchModel search = new LicenseUsageSearchModel() { Version = "2" };

            // Act
            ViewResult result = controller.Index(search) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.ViewData.ContainsKey("AllLicServers"));
            Assert.IsTrue(result.ViewData.ContainsKey("AllStates"));
            Assert.IsTrue(result.ViewData.ContainsKey("AllLicenses"));
            Assert.IsTrue(result.ViewData.ContainsKey("ShownItems"));
            Assert.IsInstanceOfType(result.ViewData["ShownItems"], typeof(List<LicenseUsage>));
            Assert.AreEqual(((List<LicenseUsage>)result.ViewData["ShownItems"])[0].Version, "2.0");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicenseUsageController_Details_Returns_ViewResult_WithModel()
        {
            // Arrange
            LicenseUsage LicenseUsage = null;

            // Act
            ViewResult result = controller.Details(1) as ViewResult;
            if (result != null) LicenseUsage = (LicenseUsage)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(LicenseUsage);
            Assert.AreEqual(LicenseUsage.Version, "1.0");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicenseUsageController_Details_Returns_HttpNotFoundResult_IfIdUnknown()
        {
            // Arrange

            // Act
            HttpNotFoundResult result = controller.Details(999) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicenseUsageController_Create_Returns_ViewResult()
        {
            // Arrange

            // Act
            ViewResult result = controller.Create() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.ViewData.ContainsKey("License"));
            Assert.IsTrue(result.ViewData.ContainsKey("LicHost"));
            Assert.IsTrue(result.ViewData.ContainsKey("LicUser"));
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicenseUsageController_Create_CreatesEntity_Returns_RedirectToAction()
        {
            // Arrange
            LicenseUsage LicenseUsage = new LicenseUsage()
            {
                Id = 4,
                License = 1,
                LicHost = 1,
                LicUser = 1,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now,
                Quantity = 1,
                State = 0,
                Version = "Test"
            };
            LicenseUsage chkLicenseUsage = null;
            
            // Act
            RedirectToRouteResult result = controller.Create(LicenseUsage) as RedirectToRouteResult;
            chkLicenseUsage = db.LicenseUsage.FirstOrDefault(g => g.Id == 4);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.RouteValues["action"], "Index");
            Assert.IsNotNull(chkLicenseUsage);
            Assert.AreEqual(chkLicenseUsage.Version, "Test");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicenseUsageController_Edit_Returns_ViewResult_WithModel()
        {
            // Arrange
            LicenseUsage LicenseUsage = null;

            // Act
            ViewResult result = controller.Edit(1) as ViewResult;
            if (result != null) LicenseUsage = (LicenseUsage)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(LicenseUsage);
            Assert.AreEqual(LicenseUsage.Version, "1.0");
            Assert.IsTrue(result.ViewData.ContainsKey("License"));
            Assert.IsTrue(result.ViewData.ContainsKey("LicHost"));
            Assert.IsTrue(result.ViewData.ContainsKey("LicUser"));
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicenseUsageController_Edit_Returns_HttpNotFoundResult_IfIdUnknown()
        {
            // Arrange

            // Act
            HttpNotFoundResult result = controller.Details(999) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicenseUsageController_Edit_ModifiesEntity_Returns_RedirectToAction()
        {
            // Arrange
            LicenseUsage LicenseUsage = new LicenseUsage()
            {
                Id = 3,
                License = 3,
                LicHost = 3,
                LicUser = 3,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now,
                Quantity = 1,
                State = 0,
                Version = "Test"
            };
            LicenseUsage chkLicenseUsage = null;

            // Act
            RedirectToRouteResult result = controller.Edit(LicenseUsage) as RedirectToRouteResult;
            chkLicenseUsage = db.LicenseUsage.FirstOrDefault(g => g.Id == 3);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.RouteValues["action"], "Index");
            Assert.IsNotNull(chkLicenseUsage);
            Assert.AreEqual(chkLicenseUsage.Version, "Test");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicenseUsageController_Delete_Returns_ViewResult_WithModel()
        {
            // Arrange
            LicenseUsage LicenseUsage = null;

            // Act
            ViewResult result = controller.Delete(1) as ViewResult;
            if (result != null) LicenseUsage = (LicenseUsage)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(LicenseUsage);
            Assert.AreEqual(LicenseUsage.Version, "1.0");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicenseUsageController_Delete_Returns_HttpNotFoundResult_IfIdUnknown()
        {
            // Arrange

            // Act
            HttpNotFoundResult result = controller.Delete(999) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicenseUsageController_DeleteConfirmed_DeletesEntity_Returns_RedirectToAction()
        {
            // Arrange
            LicenseUsage LicenseUsage = new LicenseUsage()
            {
                Id = 5,
                License = 3,
                LicHost = 3,
                LicUser = 3,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now,
                Quantity = 1,
                State = 0,
                Version = "Test"
            };
            LicenseUsage chkLicenseUsage = null;

            // Act
            db.LicenseUsage.Add(LicenseUsage);
            db.SaveChanges();
            RedirectToRouteResult result = controller.DeleteConfirmed(5) as RedirectToRouteResult;
            chkLicenseUsage = db.LicenseUsage.FirstOrDefault(g => g.Id == 5);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.RouteValues["action"], "Index");
            Assert.IsNull(chkLicenseUsage);
        }


    }

}
