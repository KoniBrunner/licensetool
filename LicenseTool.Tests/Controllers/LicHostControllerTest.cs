﻿using LicenseTool.ClassLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using LicenseTool.WebApplication.Controllers;
using LicenseTool.WebApplication.Models;

namespace LicenseTool.Tests.Controllers
{

    /// <summary>
    /// Test class for the LicHost Controller
    /// </summary>
    [TestClass]
    public class LicHostControllerTest
    {
        private LicHostController controller = null;
        private FakeLicenseToolDatabaseEntities db = null;

        /// <summary>
        /// Initializes this test with the fake contexts
        /// </summary>
        [TestInitialize]
        public void LicHostController_Initialize()
        {
            db = TestHelper.FakeDbContext();
            controller = new LicHostController(db);
            controller.ControllerContext = TestHelper.MoqControllerContext(controller);
        }

        /// <summary>
        /// Cleans up all resources of this test
        /// </summary>
        [TestCleanup]
        public void LicHostController_Cleanup()
        {
            controller.Dispose();
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicHostController_Index_Returns_ViewResult()
        {
            // Arrange

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsTrue(result.ViewData.ContainsKey("AllLicServers"));
            Assert.IsTrue(result.ViewData.ContainsKey("AllLicenses"));
            Assert.IsTrue(result.ViewData.ContainsKey("ShownItems"));
            Assert.IsInstanceOfType(result.ViewData["ShownItems"], typeof(List<LicHost>));
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicHostController_Index_WithSearchModel_Returns_ViewResult_WithModel()
        {
            // Arrange
            LicHostSearchModel search = new LicHostSearchModel() { Name = "2" };

            // Act
            ViewResult result = controller.Index(search) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.ViewData.ContainsKey("AllLicServers"));
            Assert.IsTrue(result.ViewData.ContainsKey("AllLicenses"));
            Assert.IsTrue(result.ViewData.ContainsKey("ShownItems"));
            Assert.IsInstanceOfType(result.ViewData["ShownItems"], typeof(List<LicHost>));
            Assert.AreEqual(((List<LicHost>)result.ViewData["ShownItems"])[0].Name, "LicHost2");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicHostController_Details_Returns_ViewResult_WithModel()
        {
            // Arrange
            LicHost LicHost = null;

            // Act
            ViewResult result = controller.Details(1) as ViewResult;
            if (result != null) LicHost = (LicHost)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(LicHost);
            Assert.AreEqual(LicHost.Name, "LicHost1");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicHostController_Details_Returns_HttpNotFoundResult_IfIdUnknown()
        {
            // Arrange

            // Act
            HttpNotFoundResult result = controller.Details(999) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicHostController_Create_Returns_ViewResult()
        {
            // Arrange

            // Act
            ViewResult result = controller.Create() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicHostController_Create_CreatesEntity_Returns_RedirectToAction()
        {
            // Arrange
            LicHost LicHost = new LicHost() { Id = 4, Name = "LicHost4"};
            LicHost chkLicHost = null;
            
            // Act
            RedirectToRouteResult result = controller.Create(LicHost) as RedirectToRouteResult;
            chkLicHost = db.LicHost.FirstOrDefault(g => g.Id == 4);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.RouteValues["action"], "Index");
            Assert.IsNotNull(chkLicHost);
            Assert.AreEqual(chkLicHost.Name, "LicHost4");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicHostController_Edit_Returns_ViewResult_WithModel()
        {
            // Arrange
            LicHost LicHost = null;

            // Act
            ViewResult result = controller.Edit(1) as ViewResult;
            if (result != null) LicHost = (LicHost)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(LicHost);
            Assert.AreEqual(LicHost.Name, "LicHost1");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicHostController_Edit_Returns_HttpNotFoundResult_IfIdUnknown()
        {
            // Arrange

            // Act
            HttpNotFoundResult result = controller.Details(999) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicHostController_Edit_ModifiesEntity_Returns_RedirectToAction()
        {
            // Arrange
            LicHost LicHost = new LicHost() { Id = 3, Name = "LicHost3Mod" };
            LicHost chkLicHost = null;

            // Act
            RedirectToRouteResult result = controller.Edit(LicHost) as RedirectToRouteResult;
            chkLicHost = db.LicHost.FirstOrDefault(g => g.Id == 3);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.RouteValues["action"], "Index");
            Assert.IsNotNull(chkLicHost);
            Assert.AreEqual(chkLicHost.Name, "LicHost3Mod");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicHostController_Delete_Returns_ViewResult_WithModel()
        {
            // Arrange
            LicHost LicHost = null;

            // Act
            ViewResult result = controller.Delete(1) as ViewResult;
            if (result != null) LicHost = (LicHost)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(LicHost);
            Assert.AreEqual(LicHost.Name, "LicHost1");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicHostController_Delete_Returns_HttpNotFoundResult_IfIdUnknown()
        {
            // Arrange

            // Act
            HttpNotFoundResult result = controller.Delete(999) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicHostController_DeleteConfirmed_DeletesEntity_Returns_RedirectToAction()
        {
            // Arrange
            LicHost LicHost = new LicHost() { Id = 5, Name = "LicHost5" };
            LicHost chkLicHost = null;

            // Act
            db.LicHost.Add(LicHost);
            db.SaveChanges();
            RedirectToRouteResult result = controller.DeleteConfirmed(5) as RedirectToRouteResult;
            chkLicHost = db.LicHost.FirstOrDefault(g => g.Id == 5);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.RouteValues["action"], "Index");
            Assert.IsNull(chkLicHost);
        }


    }

}
