﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LicenseTool.WebApplication;
using LicenseTool.WebApplication.Controllers;
using System.IO;
using LicenseTool.WebApplication.Models;

namespace LicenseTool.Tests.Controllers
{

    /// <summary>
    /// Test class for the Report Controller
    /// </summary>
    [TestClass]
    public class ReportControllerTest
    {
        private ReportController controller = null;

        /// <summary>
        /// Initializes this test with the fake contexts
        /// </summary>
        [TestInitialize]
        public void ReportController_Initialize()
        {
            DirectoryInfo top = new DirectoryInfo(Properties.Settings.Default.PathToTestFiles);
            controller = new ReportController(TestHelper.FakeDbContext());
            controller.ControllerContext = TestHelper.MoqControllerContext(controller);
        }

        /// <summary>
        /// Cleans up all resources of this test
        /// </summary>
        [TestCleanup]
        public void ReportController_Cleanup()
        {
            controller.Dispose();
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void ReportController_UsedLicenses_Returns_ViewResult_WithModel()
        {
            // Arrange

            // Act
            ViewResult result = controller.UsedLicenses() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result.ViewData.Model, typeof(ReportSelectionModel));
            Assert.AreEqual(result.ViewName, "UsedLicenses");
            Assert.IsTrue(result.ViewData.ContainsKey("Preview"));
            Assert.IsTrue(result.ViewData.ContainsKey("HeadIcon"));
            Assert.IsTrue(result.ViewData.ContainsKey("Years"));
            Assert.IsTrue(result.ViewData.ContainsKey("Months"));
            Assert.IsTrue(result.ViewData.ContainsKey("ReportFormats"));
            Assert.IsTrue(result.ViewData.ContainsKey("ReportFrames"));
            Assert.IsTrue(result.ViewData.ContainsKey("AllLicServers"));
            Assert.IsTrue(result.ViewData.ContainsKey("AllLicenses"));
            Assert.IsTrue(result.ViewData.ContainsKey("AllGroups"));
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void ReportController_UsedLicensesSheet_Returns_ViewResult_WithModel()
        {
            // Arrange

            // Act
            ViewResult result = controller.UsedLicensesSheet() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result.ViewData.Model, typeof(ReportSelectionModel));
            Assert.AreEqual(result.ViewName, "UsedLicenses");
            Assert.IsTrue(result.ViewData.ContainsKey("Preview"));
            Assert.IsTrue(result.ViewData.ContainsKey("HeadIcon"));
            Assert.IsTrue(result.ViewData.ContainsKey("Years"));
            Assert.IsTrue(result.ViewData.ContainsKey("Months"));
            Assert.IsTrue(result.ViewData.ContainsKey("ReportFormats"));
            Assert.IsTrue(result.ViewData.ContainsKey("ReportFrames"));
            Assert.IsTrue(result.ViewData.ContainsKey("AllLicServers"));
            Assert.IsTrue(result.ViewData.ContainsKey("AllLicenses"));
            Assert.IsTrue(result.ViewData.ContainsKey("AllGroups"));
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void ReportController_UsedLicenses_WithModel_Returns_ViewResult_WithModel()
        {
            // Arrange
            ReportSelectionModel model = new ReportSelectionModel();
            model.BackAction = "UsedLicenses";
            model.PaperFormat = "A4H";
            model.LicServer = 1;
            model.License = 1;
            model.ReportFrame = ReportFrame.Month;

            // Act
            ViewResult result = controller.UsedLicenses(model) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result.ViewData.Model, typeof(ReportSelectionModel));
            Assert.AreEqual(result.ViewName, "UsedLicenses");
            Assert.IsTrue(result.ViewData.ContainsKey("Preview"));
            Assert.IsTrue(result.ViewData.ContainsKey("HeadIcon"));
            Assert.IsTrue(result.ViewData.ContainsKey("Years"));
            Assert.IsTrue(result.ViewData.ContainsKey("Months"));
            Assert.IsTrue(result.ViewData.ContainsKey("ReportFormats"));
            Assert.IsTrue(result.ViewData.ContainsKey("ReportFrames"));
            Assert.IsTrue(result.ViewData.ContainsKey("AllLicServers"));
            Assert.IsTrue(result.ViewData.ContainsKey("AllLicenses"));
            Assert.IsTrue(result.ViewData.ContainsKey("AllGroups"));
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void ReportController_UsedLicensesSheet_WithModel_Returns_ViewResult_WithModel()
        {
            // Arrange
            ReportSelectionModel model = new ReportSelectionModel();
            model.BackAction = "UsedLicensesSheet";
            model.PaperFormat = "A4H";
            model.LicServer = 1;
            model.License = 1;
            model.ReportFrame = ReportFrame.Month;

            // Act
            ViewResult result = controller.UsedLicensesSheet(model) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result.ViewData.Model, typeof(ReportSelectionModel));
            Assert.AreEqual(result.ViewName, "UsedLicenses");
            Assert.IsTrue(result.ViewData.ContainsKey("Preview"));
            Assert.IsTrue(result.ViewData.ContainsKey("HeadIcon"));
            Assert.IsTrue(result.ViewData.ContainsKey("Years"));
            Assert.IsTrue(result.ViewData.ContainsKey("Months"));
            Assert.IsTrue(result.ViewData.ContainsKey("ReportFormats"));
            Assert.IsTrue(result.ViewData.ContainsKey("ReportFrames"));
            Assert.IsTrue(result.ViewData.ContainsKey("AllLicServers"));
            Assert.IsTrue(result.ViewData.ContainsKey("AllLicenses"));
            Assert.IsTrue(result.ViewData.ContainsKey("AllGroups"));
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void ReportController_Report_WithModel_Returns_FileContentResult()
        {
            // Arrange
            ReportSelectionModel model = new ReportSelectionModel();
            model.BackAction = "UsedLicenses";
            model.PaperFormat = "A4H";
            model.LicServer = 1;
            model.License = 1;
            model.ReportFrame = ReportFrame.Month;
            model.Month = (short)DateTime.Now.Month;
            model.Year = DateTime.Now.Year;
            model.ReportStartDate = new DateTime(model.Year, model.Month, 1);
            model.Preview = true;
            model.ReportFormat = ReportFormat.Image;
            model.ReportName = model.BackAction + model.ReportFrame.ToString();

            // Act
            FileContentResult result = controller.Report(model) as FileContentResult;

            // Assert
            Assert.IsNotNull(result);
        }

    }
}
