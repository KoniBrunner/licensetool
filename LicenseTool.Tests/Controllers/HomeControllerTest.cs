﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LicenseTool.WebApplication;
using LicenseTool.WebApplication.Controllers;
using System.IO;

namespace LicenseTool.Tests.Controllers
{

    /// <summary>
    /// Test class for the Home Controller
    /// </summary>
    [TestClass]
    public class HomeControllerTest
    {
        private HomeController controller = null;

        /// <summary>
        /// Initializes this test with the fake contexts
        /// </summary>
        [TestInitialize]
        public void HomeController_Initialize()
        {
            DirectoryInfo top = new DirectoryInfo(Properties.Settings.Default.PathToTestFiles);
            controller = new HomeController(TestHelper.FakeDbContext());
            controller.ControllerContext = TestHelper.MoqControllerContext(controller);
        }

        /// <summary>
        /// Cleans up all resources of this test
        /// </summary>
        [TestCleanup]
        public void HomeController_Cleanup()
        {
            controller.Dispose();
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void HomeController_Index_Returns_ViewResult()
        {
            // Arrange

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void HomeController_About_Returns_ViewResult()
        {
            // Arrange

            // Act
            ViewResult result = controller.About() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void HomeController_Contact_Returns_ViewResult()
        {
            // Arrange

            // Act
            ViewResult result = controller.Contact() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void HomeController_CommandLine_Returns_ViewResult()
        {
            // Arrange

            // Act
            ViewResult result = controller.CommandLine() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void HomeController_WindowsService_Returns_ViewResult()
        {
            // Arrange

            // Act
            ViewResult result = controller.WindowsService() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void HomeController_Upload_Returns_ViewResult_IfNoFiles()
        {
            // Arrange

            // Act
            ViewResult result = controller.Upload() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void HomeController_Upload_Returns_Null_IfWellFile()
        {
            // Arrange
            FileStream testFile = null;
            DirectoryInfo top = new DirectoryInfo(Properties.Settings.Default.PathToTestFiles);
            testFile = new FileInfo(Path.Combine(top.FullName, "EmptyTextFile.txt")).OpenRead();
            HomeController testController = new HomeController();
            testController.ControllerContext = TestHelper.MoqControllerContext(testController, testFile);

            // Act
            ViewResult result = testController.Upload() as ViewResult;
            if (testFile != null) testFile.Close();

            // Assert
            Assert.IsNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void HomeController_UploadProceed_Returns_ViewResult()
        {
            // Arrange

            // Act
            ViewResult result = controller.UploadProceed() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void HomeController_UploadProceed_Configures_ViewBag()
        {
            // Arrange

            // Act
            ViewResult result = controller.UploadProceed() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.ViewData.ContainsKey("ErrorItems"));
            Assert.IsTrue(result.ViewData.ContainsKey("ErrorItemsSelected"));
            Assert.IsTrue(result.ViewData.ContainsKey("DoneItems"));
            Assert.IsTrue(result.ViewData.ContainsKey("DoneItemsSelected"));
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void HomeController_DownloadCsv_WithoutEntity_Returns_ViewResult()
        {
            // Arrange

            // Act
            ViewResult result = controller.DownloadCsv() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void HomeController_DownloadCsv_WithEntity_Returns_FileContentResult()
        {
            // Arrange

            // Act
            FileContentResult resultLicense = controller.DownloadCsv("License") as FileContentResult;
            FileContentResult resultLicenseUsage = controller.DownloadCsv("LicenseUsage") as FileContentResult;
            FileContentResult resultLicHost = controller.DownloadCsv("LicHost") as FileContentResult;
            FileContentResult resultLicUser = controller.DownloadCsv("LicUser") as FileContentResult;
            FileContentResult resultLicServer = controller.DownloadCsv("LicServer") as FileContentResult;

            // Assert
            Assert.IsNotNull(resultLicense);
            Assert.IsNotNull(resultLicenseUsage);
            Assert.IsNotNull(resultLicHost);
            Assert.IsNotNull(resultLicUser);
            Assert.IsNotNull(resultLicServer);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void HomeController_PluginResultHtml_Returns_ViewResult()
        {
            // Arrange

            // Act
            ViewResult result = controller.PluginResultHtml(0) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void HomeController_PluginResultHtml_Configures_ViewBag()
        {
            // Arrange

            // Act
            ViewResult result = controller.PluginResultHtml(0) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.ViewData.ContainsKey("Title"));
            Assert.IsTrue(result.ViewData.ContainsKey("Description"));
            Assert.IsTrue(result.ViewData.ContainsKey("Plugin"));
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void HomeController_PluginResultTable_Returns_ViewResult()
        {
            // Arrange

            // Act
            ViewResult result = controller.PluginResultTable(0) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void HomeController_PluginResultTable_Configures_ViewBag()
        {
            // Arrange

            // Act
            ViewResult result = controller.PluginResultTable(0) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.ViewData.ContainsKey("Title"));
            Assert.IsTrue(result.ViewData.ContainsKey("Description"));
            Assert.IsTrue(result.ViewData.ContainsKey("Plugin"));
            Assert.IsTrue(result.ViewData.ContainsKey("Result"));
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void HomeController_PluginInput_Returns_ViewResult()
        {
            // Arrange

            // Act
            ViewResult result = controller.PluginInput() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }


    }
}
