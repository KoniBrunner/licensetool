﻿using LicenseTool.ClassLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using LicenseTool.WebApplication.Controllers;
using LicenseTool.WebApplication.Models;

namespace LicenseTool.Tests.Controllers
{

    /// <summary>
    /// Test class for the LicUser Controller
    /// </summary>
    [TestClass]
    public class LicUserControllerTest
    {
        private LicUserController controller = null;
        private FakeLicenseToolDatabaseEntities db = null;

        /// <summary>
        /// Initializes this test with the fake contexts
        /// </summary>
        [TestInitialize]
        public void LicUserController_Initialize()
        {
            db = TestHelper.FakeDbContext();
            controller = new LicUserController(db);
            controller.ControllerContext = TestHelper.MoqControllerContext(controller);
        }

        /// <summary>
        /// Cleans up all resources of this test
        /// </summary>
        [TestCleanup]
        public void LicUserController_Cleanup()
        {
            controller.Dispose();
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicUserController_Index_Returns_ViewResult()
        {
            // Arrange

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.ViewData.ContainsKey("AllLicServers"));
            Assert.IsTrue(result.ViewData.ContainsKey("AllLicenses"));
            Assert.IsTrue(result.ViewData.ContainsKey("ShownItems"));
            Assert.IsInstanceOfType(result.ViewData["ShownItems"], typeof(List<LicUser>));
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicUserController_Index_WithSearchModel_Returns_ViewResult_WithModel()
        {
            // Arrange
            LicUserSearchModel search = new LicUserSearchModel() { Name = "2" };

            // Act
            ViewResult result = controller.Index(search) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.ViewData.ContainsKey("AllLicServers"));
            Assert.IsTrue(result.ViewData.ContainsKey("AllLicenses"));
            Assert.IsTrue(result.ViewData.ContainsKey("ShownItems"));
            Assert.IsInstanceOfType(result.ViewData["ShownItems"], typeof(List<LicUser>));
            Assert.AreEqual(((List<LicUser>)result.ViewData["ShownItems"])[0].Name, "LicUser2");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicUserController_Details_Returns_ViewResult_WithModel()
        {
            // Arrange
            LicUser LicUser = null;

            // Act
            ViewResult result = controller.Details(1) as ViewResult;
            if (result != null) LicUser = (LicUser)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(LicUser);
            Assert.AreEqual(LicUser.Name, "LicUser1");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicUserController_Details_Returns_HttpNotFoundResult_IfIdUnknown()
        {
            // Arrange

            // Act
            HttpNotFoundResult result = controller.Details(999) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicUserController_Create_Returns_ViewResult()
        {
            // Arrange

            // Act
            ViewResult result = controller.Create() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicUserController_Create_CreatesEntity_Returns_RedirectToAction()
        {
            // Arrange
            LicUser LicUser = new LicUser() { Id = 4, Name = "LicUser4"};
            LicUser chkLicUser = null;
            
            // Act
            RedirectToRouteResult result = controller.Create(LicUser) as RedirectToRouteResult;
            chkLicUser = db.LicUser.FirstOrDefault(g => g.Id == 4);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.RouteValues["action"], "Index");
            Assert.IsNotNull(chkLicUser);
            Assert.AreEqual(chkLicUser.Name, "LicUser4");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicUserController_Edit_Returns_ViewResult_WithModel()
        {
            // Arrange
            LicUser LicUser = null;

            // Act
            ViewResult result = controller.Edit(1) as ViewResult;
            if (result != null) LicUser = (LicUser)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(LicUser);
            Assert.AreEqual(LicUser.Name, "LicUser1");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicUserController_Edit_Returns_HttpNotFoundResult_IfIdUnknown()
        {
            // Arrange

            // Act
            HttpNotFoundResult result = controller.Details(999) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicUserController_Edit_ModifiesEntity_Returns_RedirectToAction()
        {
            // Arrange
            LicUser LicUser = new LicUser() { Id = 3, Name = "LicUser3Mod" };
            LicUser chkLicUser = null;

            // Act
            RedirectToRouteResult result = controller.Edit(LicUser) as RedirectToRouteResult;
            chkLicUser = db.LicUser.FirstOrDefault(g => g.Id == 3);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.RouteValues["action"], "Index");
            Assert.IsNotNull(chkLicUser);
            Assert.AreEqual(chkLicUser.Name, "LicUser3Mod");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicUserController_Delete_Returns_ViewResult_WithModel()
        {
            // Arrange
            LicUser LicUser = null;

            // Act
            ViewResult result = controller.Delete(1) as ViewResult;
            if (result != null) LicUser = (LicUser)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(LicUser);
            Assert.AreEqual(LicUser.Name, "LicUser1");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicUserController_Delete_Returns_HttpNotFoundResult_IfIdUnknown()
        {
            // Arrange

            // Act
            HttpNotFoundResult result = controller.Delete(999) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicUserController_DeleteConfirmed_DeletesEntity_Returns_RedirectToAction()
        {
            // Arrange
            LicUser LicUser = new LicUser() { Id = 5, Name = "LicUser5" };
            LicUser chkLicUser = null;

            // Act
            db.LicUser.Add(LicUser);
            db.SaveChanges();
            RedirectToRouteResult result = controller.DeleteConfirmed(5) as RedirectToRouteResult;
            chkLicUser = db.LicUser.FirstOrDefault(g => g.Id == 5);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.RouteValues["action"], "Index");
            Assert.IsNull(chkLicUser);
        }


    }

}
