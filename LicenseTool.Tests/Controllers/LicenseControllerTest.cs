﻿using LicenseTool.ClassLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using LicenseTool.WebApplication.Controllers;
using LicenseTool.WebApplication.Models;

namespace LicenseTool.Tests.Controllers
{

    /// <summary>
    /// Test class for the License Controller
    /// </summary>
    [TestClass]
    public class LicenseControllerTest
    {
        private LicenseController controller = null;
        private FakeLicenseToolDatabaseEntities db = null;

        /// <summary>
        /// Initializes this test with the fake contexts
        /// </summary>
        [TestInitialize]
        public void LicenseController_Initialize()
        {
            db = TestHelper.FakeDbContext();
            controller = new LicenseController(db);
            controller.ControllerContext = TestHelper.MoqControllerContext(controller);
        }

        /// <summary>
        /// Cleans up all resources of this test
        /// </summary>
        [TestCleanup]
        public void LicenseController_Cleanup()
        {
            controller.Dispose();
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicenseController_Index_Returns_ViewResult()
        {
            // Arrange

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.ViewData.ContainsKey("AllLicServers"));
            Assert.IsTrue(result.ViewData.ContainsKey("AllTypes"));
            Assert.IsTrue(result.ViewData.ContainsKey("AllStatus"));
            Assert.IsTrue(result.ViewData.ContainsKey("ShownItems"));
            Assert.IsInstanceOfType(result.ViewData["ShownItems"], typeof(List<License>));
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicenseController_Index_WithSearchModel_Returns_ViewResult_WithModel()
        {
            // Arrange
            LicenseSearchModel search = new LicenseSearchModel() { FeatureName = "2" };

            // Act
            ViewResult result = controller.Index(search) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.ViewData.ContainsKey("AllLicServers"));
            Assert.IsTrue(result.ViewData.ContainsKey("AllTypes"));
            Assert.IsTrue(result.ViewData.ContainsKey("AllStatus"));
            Assert.IsTrue(result.ViewData.ContainsKey("ShownItems"));
            Assert.IsInstanceOfType(result.ViewData["ShownItems"], typeof(List<License>));
            Assert.AreEqual(((List<License>)result.ViewData["ShownItems"])[0].FeatureName, "Feature1Server2");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicenseController_Details_Returns_ViewResult_WithModel()
        {
            // Arrange
            License License = null;

            // Act
            ViewResult result = controller.Details(1) as ViewResult;
            if (result != null) License = (License)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(License);
            Assert.AreEqual(License.FeatureName, "Feature1Server1");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicenseController_Details_Returns_HttpNotFoundResult_IfIdUnknown()
        {
            // Arrange

            // Act
            HttpNotFoundResult result = controller.Details(999) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicenseController_Create_Returns_ViewResult()
        {
            // Arrange

            // Act
            ViewResult result = controller.Create() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.ViewData.ContainsKey("LicServer"));
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicenseController_Create_CreatesEntity_Returns_RedirectToAction()
        {
            // Arrange
            License License = new License() { Id = 4, FeatureName = "Feature4Server1", LicServer = 1 };
            License chkLicense = null;
            
            // Act
            RedirectToRouteResult result = controller.Create(License) as RedirectToRouteResult;
            chkLicense = db.License.FirstOrDefault(g => g.Id == 4);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.RouteValues["action"], "Index");
            Assert.IsNotNull(chkLicense);
            Assert.AreEqual(chkLicense.FeatureName, "Feature4Server1");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicenseController_Edit_Returns_ViewResult_WithModel()
        {
            // Arrange
            License License = null;

            // Act
            ViewResult result = controller.Edit(1) as ViewResult;
            if (result != null) License = (License)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(License);
            Assert.AreEqual(License.FeatureName, "Feature1Server1");
            Assert.IsTrue(result.ViewData.ContainsKey("LicServer"));
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicenseController_Edit_Returns_HttpNotFoundResult_IfIdUnknown()
        {
            // Arrange

            // Act
            HttpNotFoundResult result = controller.Details(999) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicenseController_Edit_ModifiesEntity_Returns_RedirectToAction()
        {
            // Arrange
            License License = new License() { Id = 3, FeatureName = "Feature1Server3Mod", LicServer = 3 };
            License chkLicense = null;

            // Act
            RedirectToRouteResult result = controller.Edit(License) as RedirectToRouteResult;
            chkLicense = db.License.FirstOrDefault(g => g.Id == 3);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.RouteValues["action"], "Index");
            Assert.IsNotNull(chkLicense);
            Assert.AreEqual(chkLicense.FeatureName, "Feature1Server3Mod");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicenseController_Delete_Returns_ViewResult_WithModel()
        {
            // Arrange
            License License = null;

            // Act
            ViewResult result = controller.Delete(1) as ViewResult;
            if (result != null) License = (License)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(License);
            Assert.AreEqual(License.FeatureName, "Feature1Server1");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicenseController_Delete_Returns_HttpNotFoundResult_IfIdUnknown()
        {
            // Arrange

            // Act
            HttpNotFoundResult result = controller.Delete(999) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicenseController_DeleteConfirmed_DeletesEntity_Returns_RedirectToAction()
        {
            // Arrange
            License License = new License() { Id = 5, FeatureName = "Feature5Server1", LicServer = 1 };
            License chkLicense = null;

            // Act
            db.License.Add(License);
            db.SaveChanges();
            RedirectToRouteResult result = controller.DeleteConfirmed(5) as RedirectToRouteResult;
            chkLicense = db.License.FirstOrDefault(g => g.Id == 5);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.RouteValues["action"], "Index");
            Assert.IsNull(chkLicense);
        }


    }

}
