﻿using LicenseTool.ClassLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using LicenseTool.WebApplication.Controllers;
using LicenseTool.WebApplication.Models;

namespace LicenseTool.Tests.Controllers
{

    /// <summary>
    /// Test class for the LicServer Controller
    /// </summary>
    [TestClass]
    public class LicServerControllerTest
    {
        private LicServerController controller = null;
        private FakeLicenseToolDatabaseEntities db = null;

        /// <summary>
        /// Initializes this test with the fake contexts
        /// </summary>
        [TestInitialize]
        public void LicServerController_Initialize()
        {
            db = TestHelper.FakeDbContext();
            controller = new LicServerController(db);
            controller.ControllerContext = TestHelper.MoqControllerContext(controller);
        }

        /// <summary>
        /// Cleans up all resources of this test
        /// </summary>
        [TestCleanup]
        public void LicServerController_Cleanup()
        {
            controller.Dispose();
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicServerController_Index_Returns_ViewResult()
        {
            // Arrange

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.ViewData.ContainsKey("ShownItems"));
            Assert.IsInstanceOfType(result.ViewData["ShownItems"], typeof(List<LicServer>));
            Assert.IsTrue(result.ViewData.ContainsKey("AllServerAndVendorStatus"));
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicServerController_Index_WithSearchModel_Returns_ViewResult_WithModel()
        {
            // Arrange
            LicServerSearchModel search = new LicServerSearchModel() { Name = "2" };

            // Act
            ViewResult result = controller.Index(search) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.ViewData.ContainsKey("ShownItems"));
            Assert.IsInstanceOfType(result.ViewData["ShownItems"], typeof(List<LicServer>));
            Assert.AreEqual(((List<LicServer>)result.ViewData["ShownItems"])[0].Name, "Server2");
            Assert.IsTrue(result.ViewData.ContainsKey("AllServerAndVendorStatus"));
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicServerController_Details_Returns_ViewResult_WithModel()
        {
            // Arrange
            LicServer LicServer = null;

            // Act
            ViewResult result = controller.Details(1) as ViewResult;
            if (result != null) LicServer = (LicServer)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(LicServer);
            Assert.AreEqual(LicServer.Name, "Server1");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicServerController_Details_Returns_HttpNotFoundResult_IfIdUnknown()
        {
            // Arrange

            // Act
            HttpNotFoundResult result = controller.Details(999) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicServerController_Create_Returns_ViewResult()
        {
            // Arrange

            // Act
            ViewResult result = controller.Create() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.ViewData.ContainsKey("LicHost1"));
            Assert.IsTrue(result.ViewData.ContainsKey("LicHost2"));
            Assert.IsTrue(result.ViewData.ContainsKey("LicHost3"));
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicServerController_Create_CreatesEntity_Returns_RedirectToAction()
        {
            // Arrange
            LicServer LicServer = new LicServer() { Id = 4, Name = "Server4"};
            LicServer chkLicServer = null;
            
            // Act
            RedirectToRouteResult result = controller.Create(LicServer) as RedirectToRouteResult;
            chkLicServer = db.LicServer.FirstOrDefault(g => g.Id == 4);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.RouteValues["action"], "Index");
            Assert.IsNotNull(chkLicServer);
            Assert.AreEqual(chkLicServer.Name, "Server4");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicServerController_Edit_Returns_ViewResult_WithModel()
        {
            // Arrange
            LicServer LicServer = null;

            // Act
            ViewResult result = controller.Edit(1) as ViewResult;
            if (result != null) LicServer = (LicServer)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(LicServer);
            Assert.AreEqual(LicServer.Name, "Server1");
            Assert.IsTrue(result.ViewData.ContainsKey("LicHost1"));
            Assert.IsTrue(result.ViewData.ContainsKey("LicHost2"));
            Assert.IsTrue(result.ViewData.ContainsKey("LicHost3"));
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicServerController_Edit_Returns_HttpNotFoundResult_IfIdUnknown()
        {
            // Arrange

            // Act
            HttpNotFoundResult result = controller.Details(999) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicServerController_Edit_ModifiesEntity_Returns_RedirectToAction()
        {
            // Arrange
            LicServer LicServer = new LicServer() { Id = 3, Name = "Server3Mod" };
            LicServer chkLicServer = null;

            // Act
            RedirectToRouteResult result = controller.Edit(LicServer) as RedirectToRouteResult;
            chkLicServer = db.LicServer.FirstOrDefault(g => g.Id == 3);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.RouteValues["action"], "Index");
            Assert.IsNotNull(chkLicServer);
            Assert.AreEqual(chkLicServer.Name, "Server3Mod");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicServerController_Delete_Returns_ViewResult_WithModel()
        {
            // Arrange
            LicServer LicServer = null;

            // Act
            ViewResult result = controller.Delete(1) as ViewResult;
            if (result != null) LicServer = (LicServer)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(LicServer);
            Assert.AreEqual(LicServer.Name, "Server1");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicServerController_Delete_Returns_HttpNotFoundResult_IfIdUnknown()
        {
            // Arrange

            // Act
            HttpNotFoundResult result = controller.Delete(999) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void LicServerController_DeleteConfirmed_DeletesEntity_Returns_RedirectToAction()
        {
            // Arrange
            LicServer LicServer = new LicServer() { Id = 5, Name = "Server5" };
            LicServer chkLicServer = null;

            // Act
            db.LicServer.Add(LicServer);
            db.SaveChanges();
            RedirectToRouteResult result = controller.DeleteConfirmed(5) as RedirectToRouteResult;
            chkLicServer = db.LicServer.FirstOrDefault(g => g.Id == 5);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.RouteValues["action"], "Index");
            Assert.IsNull(chkLicServer);
        }


    }

}
