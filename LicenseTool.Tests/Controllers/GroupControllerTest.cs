﻿using LicenseTool.ClassLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using LicenseTool.WebApplication.Controllers;
using LicenseTool.WebApplication.Models;

namespace LicenseTool.Tests.Controllers
{

    /// <summary>
    /// Test class for the Group Controller
    /// </summary>
    [TestClass]
    public class GroupControllerTest
    {
        private GroupController controller = null;
        private FakeLicenseToolDatabaseEntities db = null;

        /// <summary>
        /// Initializes this test with the fake contexts
        /// </summary>
        [TestInitialize]
        public void GroupController_Initialize()
        {
            db = TestHelper.FakeDbContext();
            controller = new GroupController(db);
            controller.ControllerContext = TestHelper.MoqControllerContext(controller);
        }

        /// <summary>
        /// Cleans up all resources of this test
        /// </summary>
        [TestCleanup]
        public void GroupController_Cleanup()
        {
            controller.Dispose();
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void GroupController_Index_Returns_ViewResult()
        {
            // Arrange

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.ViewData.ContainsKey("ShownItems"));
            Assert.IsInstanceOfType(result.ViewData["ShownItems"], typeof(List<Group>));
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void GroupController_Index_WithSearchModel_Returns_ViewResult_WithModel()
        {
            // Arrange
            GroupSearchModel search = new GroupSearchModel() { Name = "2" };

            // Act
            ViewResult result = controller.Index(search) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.ViewData.ContainsKey("ShownItems"));
            Assert.IsInstanceOfType(result.ViewData["ShownItems"], typeof(List<Group>));
            Assert.AreEqual(((List<Group>)result.ViewData["ShownItems"])[0].Name, "Group2");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void GroupController_Details_Returns_ViewResult_WithModel()
        {
            // Arrange
            Group group = null;

            // Act
            ViewResult result = controller.Details(1) as ViewResult;
            if (result != null) group = (Group)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(group);
            Assert.AreEqual(group.Name, "Group1");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void GroupController_Details_Returns_HttpNotFoundResult_IfIdUnknown()
        {
            // Arrange

            // Act
            HttpNotFoundResult result = controller.Details(999) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void GroupController_Create_Returns_ViewResult()
        {
            // Arrange

            // Act
            ViewResult result = controller.Create() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void GroupController_Create_CreatesEntity_Returns_RedirectToAction()
        {
            // Arrange
            Group group = new Group() { Id = 4, Name = "Group4", Description = "Desc4"};
            Group chkGroup = null;
            
            // Act
            RedirectToRouteResult result = controller.Create(group) as RedirectToRouteResult;
            chkGroup = db.Group.FirstOrDefault(g => g.Id == 4);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.RouteValues["action"], "Index");
            Assert.IsNotNull(chkGroup);
            Assert.AreEqual(chkGroup.Name, "Group4");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void GroupController_Edit_Returns_ViewResult_WithModel()
        {
            // Arrange
            Group group = null;

            // Act
            ViewResult result = controller.Edit(1) as ViewResult;
            if (result != null) group = (Group)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(group);
            Assert.AreEqual(group.Name, "Group1");
            Assert.IsTrue(result.ViewData.ContainsKey("AllLicUsers"));
            Assert.IsTrue(result.ViewData.ContainsKey("AllRelGroupUser"));
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void GroupController_Edit_Returns_HttpNotFoundResult_IfIdUnknown()
        {
            // Arrange

            // Act
            HttpNotFoundResult result = controller.Details(999) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void GroupController_Edit_ModifiesEntity_Returns_RedirectToAction()
        {
            // Arrange
            Group group = new Group() { Id = 3, Name = "Group3", Description = "Desc3Mod" };
            Group chkGroup = null;

            // Act
            RedirectToRouteResult result = controller.Edit(group, null) as RedirectToRouteResult;
            chkGroup = db.Group.FirstOrDefault(g => g.Id == 3);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.RouteValues["action"], "Index");
            Assert.IsNotNull(chkGroup);
            Assert.AreEqual(chkGroup.Description, "Desc3Mod");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void GroupController_Edit_ModifiesGroupAssignment()
        {
            // Arrange
            Group group = new Group() { Id = 3, Name = "Group3", Description = "Desc3GrpAss" };
            List<int> SelectedGroupUser = new List<int>() { 1, 2, 3 };
            Group chkGroup = null;

            // Act
            RedirectToRouteResult result = controller.Edit(group, SelectedGroupUser) as RedirectToRouteResult;
            chkGroup = db.Group.FirstOrDefault(g => g.Id == 3);
            List<RelGroupUser> rels = db.RelGroupUser.Where(g => g.GroupId == 3).ToList();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.RouteValues["action"], "Index");
            Assert.IsNotNull(chkGroup);
            Assert.AreEqual(chkGroup.Description, "Desc3GrpAss");
            Assert.AreEqual(rels.Count, 3);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void GroupController_Delete_Returns_ViewResult_WithModel()
        {
            // Arrange
            Group group = null;

            // Act
            ViewResult result = controller.Delete(1) as ViewResult;
            if (result != null) group = (Group)result.ViewData.Model;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(group);
            Assert.AreEqual(group.Name, "Group1");
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void GroupController_Delete_Returns_HttpNotFoundResult_IfIdUnknown()
        {
            // Arrange

            // Act
            HttpNotFoundResult result = controller.Delete(999) as HttpNotFoundResult;

            // Assert
            Assert.IsNotNull(result);
        }

        /// <summary>
        /// Test method
        /// </summary>
        [TestMethod]
        public void GroupController_DeleteConfirmed_DeletesEntity_Returns_RedirectToAction()
        {
            // Arrange
            Group group = new Group() { Id = 5, Name = "Group5", Description = "Desc5Mod" };
            Group chkGroup = null;

            // Act
            db.Group.Add(group);
            db.SaveChanges();
            RedirectToRouteResult result = controller.DeleteConfirmed(5) as RedirectToRouteResult;
            chkGroup = db.Group.FirstOrDefault(g => g.Id == 5);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(result.RouteValues["action"], "Index");
            Assert.IsNull(chkGroup);
        }


    }

}
