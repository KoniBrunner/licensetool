﻿using LicenseTool.ClassLibrary;
using Moq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LicenseTool.Tests
{

    //http://romiller.com/2012/02/14/testing-with-a-fake-dbcontext/

    /// <summary>
    /// The fake database context used for testing
    /// </summary>
    /// <seealso cref="ILicenseToolContext"/>
    public partial class FakeLicenseToolDatabaseEntities : DbContext, ILicenseToolContext
    {

        /// <summary>
        /// Constructor initializes all properties with fake sets
        /// </summary>
        public FakeLicenseToolDatabaseEntities()
        {
            this.License = new FakeLicenseSet();
            this.LicenseUsage = new FakeLicenseUsageSet();
            this.LicHost = new FakeLicHostSet();
            this.LicServer = new FakeLicServerSet();
            this.LicUser = new FakeLicUserSet();
            this.UsageReportView = new FakeUsageReportViewSet();
            this.Group = new FakeGroupSet();
            this.RelGroupUser = new FakeRelGroupUserSet();
            this.SentMessages = new FakeSentMessagesSet();
        }

        /// <summary>
        /// Just returns an exception if there is any code first request
        /// </summary>
        /// <param name="modelBuilder">ignored</param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }

        /// <summary>
        /// Sets an entity to modified
        /// </summary>
        /// <param name="entity">The entity to be set to modified</param>
        public void SetModified(object entity)
        {
            if (entity is License)
                Copy(entity, License.Find(((License)entity).Id));
            if (entity is LicenseUsage)
                Copy(entity, LicenseUsage.Find(((LicenseUsage)entity).Id));
            if (entity is LicHost)
                Copy(entity, LicHost.Find(((LicHost)entity).Id));
            if (entity is LicServer)
                Copy(entity, LicServer.Find(((LicServer)entity).Id));
            if (entity is LicUser)
                Copy(entity, LicUser.Find(((LicUser)entity).Id));
            if (entity is Group)
                Copy(entity, Group.Find(((Group)entity).Id));
            if (entity is RelGroupUser)
                Copy(entity, RelGroupUser.Find(((RelGroupUser)entity).Id));
            if (entity is SentMessages)
                Copy(entity, SentMessages.Find(((SentMessages)entity).Id));
            if (entity is UsageReportView)
            {
                UsageReportView u = entity as UsageReportView;
                Copy(entity, UsageReportView.Find(new object[] { u.Year, u.Month, u.Day, u.Hour, u.Minute, u.FeatureId }));
            }
        }

        /// <summary>
        /// Is doing nothing on the faked context
        /// </summary>
        /// <returns>Always 0</returns>
        public override int SaveChanges()
        {
            return 0;
        }

        /// <summary>
        /// Copies one entity to an other
        /// </summary>
        /// <param name="source">The source entity</param>
        /// <param name="target">The target entity</param>
        private void Copy(object source, object target)
        {
            Type t = source.GetType();
            PropertyInfo[] properties = t.GetProperties();
            foreach (PropertyInfo pi in properties)
            {
                if (pi.CanWrite)
                {
                    pi.SetValue(target, pi.GetValue(source, null), null);
                }
            }
        }

        /// <summary>
        /// Holding the faked entity set for License
        /// </summary>
        /// <seealso cref="FakeLicenseSet"/>
        public IDbSet<License> License { get; set; }

        /// <summary>
        /// Holding the faked entity set for LicenseUsage
        /// </summary>
        /// <seealso cref="FakeLicenseUsageSet"/>
        public IDbSet<LicenseUsage> LicenseUsage { get; set; }

        /// <summary>
        /// Holding the faked entity set for LicHost
        /// </summary>
        /// <seealso cref="FakeLicHostSet"/>
        public IDbSet<LicHost> LicHost { get; set; }

        /// <summary>
        /// Holding the faked entity set for LicServer
        /// </summary>
        /// <seealso cref="FakeLicServerSet"/>
        public IDbSet<LicServer> LicServer { get; set; }

        /// <summary>
        /// Holding the faked entity set for LicUser
        /// </summary>
        /// <seealso cref="FakeLicUserSet"/>
        public IDbSet<LicUser> LicUser { get; set; }

        /// <summary>
        /// Holding the faked entity set for UsageReportView
        /// </summary>
        /// <seealso cref="FakeUsageReportViewSet"/>
        public IDbSet<UsageReportView> UsageReportView { get; set; }

        /// <summary>
        /// Holding the faked entity set for Group
        /// </summary>
        /// <seealso cref="FakeGroupSet"/>
        public IDbSet<Group> Group { get; set; }

        /// <summary>
        /// Holding the faked entity set for RelGroupUser
        /// </summary>
        /// <seealso cref="FakeRelGroupUserSet"/>
        public IDbSet<RelGroupUser> RelGroupUser { get; set; }

        /// <summary>
        /// Holding the faked entity set for SentMessages
        /// </summary>
        /// <seealso cref="FakeSentMessagesSet"/>
        public IDbSet<SentMessages> SentMessages { get; set; }
    }

    /// <summary>
    /// The fake Group set
    /// </summary>
    public class FakeGroupSet : FakeDbSet<Group>
    {
        /// <summary>
        /// Finds an entity in the set
        /// </summary>
        /// <param name="keyValues">The key values of the entity to be found</param>
        /// <returns>The found entity or null</returns>
        public override Group Find(params object[] keyValues)
        {
            return this.SingleOrDefault(d => d.Id == (int)keyValues.Single());
        }
    }

    /// <summary>
    /// The fake License set
    /// </summary>
    public class FakeLicenseSet : FakeDbSet<License>
    {
        /// <summary>
        /// Finds an entity in the set
        /// </summary>
        /// <param name="keyValues">The key values of the entity to be found</param>
        /// <returns>The found entity or null</returns>
        public override License Find(params object[] keyValues)
        {
            return this.SingleOrDefault(e => e.Id == (int)keyValues.Single());
        }
    }

    /// <summary>
    /// The fake LicenseUsage set
    /// </summary>
    public class FakeLicenseUsageSet : FakeDbSet<LicenseUsage>
    {
        /// <summary>
        /// Finds an entity in the set
        /// </summary>
        /// <param name="keyValues">The key values of the entity to be found</param>
        /// <returns>The found entity or null</returns>
        public override LicenseUsage Find(params object[] keyValues)
        {
            return this.SingleOrDefault(d => d.Id == (int)keyValues.Single());
        }
    }

    /// <summary>
    /// The fake LicHost set
    /// </summary>
    public class FakeLicHostSet : FakeDbSet<LicHost>
    {
        /// <summary>
        /// Finds an entity in the set
        /// </summary>
        /// <param name="keyValues">The key values of the entity to be found</param>
        /// <returns>The found entity or null</returns>
        public override LicHost Find(params object[] keyValues)
        {
            return this.SingleOrDefault(e => e.Id == (int)keyValues.Single());
        }
    }

    /// <summary>
    /// The fake LicServer set
    /// </summary>
    public class FakeLicServerSet : FakeDbSet<LicServer>
    {
        /// <summary>
        /// Finds an entity in the set
        /// </summary>
        /// <param name="keyValues">The key values of the entity to be found</param>
        /// <returns>The found entity or null</returns>
        public override LicServer Find(params object[] keyValues)
        {
            return this.SingleOrDefault(d => d.Id == (int)keyValues.Single());
        }
    }

    /// <summary>
    /// The fake LicUser set
    /// </summary>
    public class FakeLicUserSet : FakeDbSet<LicUser>
    {
        /// <summary>
        /// Finds an entity in the set
        /// </summary>
        /// <param name="keyValues">The key values of the entity to be found</param>
        /// <returns>The found entity or null</returns>
        public override LicUser Find(params object[] keyValues)
        {
            return this.SingleOrDefault(e => e.Id == (int)keyValues.Single());
        }
    }

    /// <summary>
    /// The fake UsageReportView set
    /// </summary>
    public class FakeUsageReportViewSet : FakeDbSet<UsageReportView>
    {
        /// <summary>
        /// Finds an entity in the set
        /// </summary>
        /// <param name="keyValues">The key values of the entity to be found</param>
        /// <returns>The found entity or null</returns>
        public override UsageReportView Find(params object[] keyValues)
        {
            return this.SingleOrDefault(d => d.Year == (int)keyValues[0] && d.Month == (int)keyValues[1] &&
                d.Day == (int)keyValues[2] && d.Hour == (int)keyValues[3] && d.Minute == (int)keyValues[4] && 
                d.FeatureId == (int)keyValues[5]);
        }
    }

    /// <summary>
    /// The fake RelGroupUser set
    /// </summary>
    public class FakeRelGroupUserSet : FakeDbSet<RelGroupUser>
    {
        /// <summary>
        /// Finds an entity in the set
        /// </summary>
        /// <param name="keyValues">The key values of the entity to be found</param>
        /// <returns>The found entity or null</returns>
        public override RelGroupUser Find(params object[] keyValues)
        {
            return this.SingleOrDefault(e => e.Id == (int)keyValues.Single());
        }
    }

    /// <summary>
    /// The fake SentMessages set
    /// </summary>
    public class FakeSentMessagesSet : FakeDbSet<SentMessages>
    {
        /// <summary>
        /// Finds an entity in the set
        /// </summary>
        /// <param name="keyValues">The key values of the entity to be found</param>
        /// <returns>The found entity or null</returns>
        public override SentMessages Find(params object[] keyValues)
        {
            return this.SingleOrDefault(d => d.Id == (int)keyValues.Single());
        }
    }

    /// <summary>
    /// The base fake set
    /// </summary>
    /// <typeparam name="T">The type this set works for</typeparam>
    public class FakeDbSet<T> : IDbSet<T>
        where T : class
    {
        ObservableCollection<T> _data;
        IQueryable _query;

        /// <summary>
        /// Constructor initializing the class
        /// </summary>
        public FakeDbSet()
        {
            _data = new ObservableCollection<T>();
            _query = _data.AsQueryable();
        }

        /// <summary>
        /// Finds an entity in the set
        /// </summary>
        /// <param name="keyValues">The key values of the entity to be found</param>
        /// <returns>The found entity or null</returns>
        public virtual T Find(params object[] keyValues)
        {
            throw new NotImplementedException("Derive from FakeDbSet<T> and override Find");
        }

        /// <summary>
        /// Adds an entity to the set
        /// </summary>
        /// <param name="item">The entity to be added</param>
        /// <returns>The added entity</returns>
        public T Add(T item)
        {
            _data.Add(item);
            return item;
        }

        /// <summary>
        /// Removes an entity from the set
        /// </summary>
        /// <param name="item">The entity to be removed</param>
        /// <returns>The removed entity</returns>
        public T Remove(T item)
        {
            _data.Remove(item);
            return item;
        }

        /// <summary>
        /// Adds an entity to the set
        /// </summary>
        /// <param name="item">The entity to be added</param>
        /// <returns>The added entity</returns>
        public T Attach(T item)
        {
            _data.Add(item);
            return item;
        }

        /// <summary>
        /// Removes an entity from the set
        /// </summary>
        /// <param name="item">The entity to be removed</param>
        /// <returns>The removed entity</returns>
        public T Detach(T item)
        {
            _data.Remove(item);
            return item;
        }

        /// <summary>
        /// Creates a new instance of an entity
        /// </summary>
        /// <returns>The created entity</returns>
        public T Create()
        {
            return Activator.CreateInstance<T>();
        }

        /// <summary>
        /// Creates a new instance of an entity of specific type
        /// </summary>
        /// <typeparam name="TDerivedEntity">The type this method works for</typeparam>
        /// <returns>The created entity</returns>
        public TDerivedEntity Create<TDerivedEntity>() where TDerivedEntity : class, T
        {
            return Activator.CreateInstance<TDerivedEntity>();
        }

        /// <summary>
        /// Gets the local entities
        /// </summary>
        /// <returns>The local entities</returns>
        public ObservableCollection<T> Local
        {
            get { return _data; }
        }

        /// <summary>
        /// Gets the type of the entities in that set
        /// </summary>
        /// <returns>The type of the entities</returns>
        Type IQueryable.ElementType
        {
            get { return _query.ElementType; }
        }

        /// <summary>
        /// Gets an expression
        /// </summary>
        /// <returns>The expression</returns>
        System.Linq.Expressions.Expression IQueryable.Expression
        {
            get { return _query.Expression; }
        }

        /// <summary>
        /// Gets a provider
        /// </summary>
        /// <returns>The provider</returns>
        IQueryProvider IQueryable.Provider
        {
            get { return _query.Provider; }
        }

        /// <summary>
        /// Gets an enumerator
        /// </summary>
        /// <returns>The enumerator</returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _data.GetEnumerator();
        }

        /// <summary>
        /// Gets an enumerator
        /// </summary>
        /// <returns>The enumerator</returns>
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return _data.GetEnumerator();
        }
    }
}
