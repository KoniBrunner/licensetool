﻿using LicenseTool.ClassLibrary;
using LicenseTool.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace LicenseTool.PluginsWeb
{

    /// <summary>
    /// Plugin to get all expiring licenses
    /// </summary>
    public class ExpiringLicensesWeb : ExpiringLicenses, IWebPlugin
    {

        /// <summary>
        /// Constructor initializing the instance
        /// </summary>
        public ExpiringLicensesWeb()
            : base()
        {
        }

        /// <summary>
        /// Constructor initializing the instance
        /// </summary>
        /// <param name="db">The databse context to be used</param>
        /// <exception cref="ArgumentNullException">If <paramref name="db"/> is <c>null</c></exception>
        public ExpiringLicensesWeb(ILicenseToolContext db)
            : base(db)
        {
        }

        /// <summary>
        /// Gets the url path of the plugin icon
        /// </summary>
        /// <param name="context">The http context</param>
        /// <returns>The url path to the plugin icon to be shown</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="context"/> is <c>null</c></exception>
        public string GetIconPath(HttpContext context)
        {
            if (context == null) throw new ArgumentNullException("context");
            return GetIconPath(new HttpContextWrapper(context));
        }

        /// <summary>
        /// Gets the url path of the plugin icon
        /// </summary>
        /// <param name="context">The http context</param>
        /// <returns>The url path to the plugin icon to be shown</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="context"/> is <c>null</c></exception>
        public string GetIconPath(HttpContextBase context)
        {
            if (context == null) throw new ArgumentNullException("context");
            var requestContext = new RequestContext(context, new RouteData());
            var urlHelper = new UrlHelper(requestContext);
            return urlHelper.Content("~/Images/basket.png");
        }

        /// <summary>
        /// Renders the plugin on the index page
        /// </summary>
        /// <param name="context">The http context</param>
        /// <param name="pluginId">The id of that plugin</param>
        /// <returns>The html string of the plugin to be shown on the index page</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="context"/> is <c>null</c></exception>
        public string RenderIndex(HttpContext context, int pluginId)
        {
            if (context == null) throw new ArgumentNullException("context");
            return RenderIndex(new HttpContextWrapper(context), pluginId);
        }

        /// <summary>
        /// Renders the plugin on the index page
        /// </summary>
        /// <param name="context">The http context</param>
        /// <param name="pluginId">The id of that plugin</param>
        /// <returns>The html string of the plugin to be shown on the index page</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="context"/> is <c>null</c></exception>
        public string RenderIndex(HttpContextBase context, int pluginId)
        {
            if (context == null) throw new ArgumentNullException("context");
            var requestContext = new RequestContext(context, new RouteData());
            var urlHelper = new UrlHelper(requestContext);
            string ret = "<div class=\"content-group\">";
            ret += "<a href=\"" + urlHelper.Action("PluginResultTable", "Home", new { pluginId = pluginId }) + "\">";
            ret += "<img src=\"" + GetIconPath(context) + "\" class=\"needstooltip\" title=\"" + this.Name + "\" /><br />";
            ret += this.Name + "</a>\n";
            ret += "</div>";
            return ret;
        }

        /// <summary>
        /// Renders the plugin on the table result page
        /// </summary>
        /// <param name="context">The http context</param>
        /// <returns>The html string array of the plugin to be shown on the table result page</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="context"/> is <c>null</c></exception>
        public string[][] BuildResultTable(HttpContext context)
        {
            if (context == null) throw new ArgumentNullException("context");
            return BuildResultTable(new HttpContextWrapper(context));
        }

        /// <summary>
        /// Renders the plugin on the table result page
        /// </summary>
        /// <param name="context">The http context</param>
        /// <returns>The html string array of the plugin to be shown on the table result page</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="context"/> is <c>null</c></exception>
        public string[][] BuildResultTable(HttpContextBase context)
        {
            if (context == null) throw new ArgumentNullException("context");
            var requestContext = new RequestContext(context, new RouteData());
            var urlHelper = new UrlHelper(requestContext);

            string[][] result = new string[0][];
            using (LicenseToolDatabaseEntities db = new LicenseToolDatabaseEntities())
            {
                var license = db.License.Include(l => l.NavLicServer);

                DateTime checkDate = DateTime.Now.AddMonths(1);
                List<License> foundItems = db.License.Where(l => l.OrderEndDate < checkDate)
                    .OrderBy(l => l.NavLicServer.Name).ThenBy(l => l.FeatureName).ToList();

                result = new string[foundItems.Count + 1][];

                result[0] = new string[4];
                result[0][0] = LicenseTool.ResourceLibrary.Model.LicServer;
                result[0][1] = LicenseTool.ResourceLibrary.Model.License;
                result[0][2] = LicenseTool.ResourceLibrary.Model.OrderName;
                result[0][3] = LicenseTool.ResourceLibrary.Model.OrderEndDate;

                for (int i = 0; i < foundItems.Count; i++)
                {
                    result[i + 1] = new string[4];
                    result[i + 1][0] = "<a href=\"" + urlHelper.Action("Index", "LicServer", new { serverId = foundItems[i].LicServer }) + "\">" + foundItems[i].NavLicServer.Name + "</a>";
                    result[i + 1][1] = "<a href=\"" + urlHelper.Action("Index", "License", new { licenseId = foundItems[i].Id }) + "\">" + foundItems[i].FeatureName + "</a>";
                    result[i + 1][2] = foundItems[i].OrderName;
                    result[i + 1][3] = foundItems[i].OrderEndDate == null ? "" : foundItems[i].OrderEndDate.Value.ToString("d");
                }
            }

            return result;
        }

        /// <summary>
        /// Renders the plugin on the html result page
        /// </summary>
        /// <param name="context">The http context</param>
        /// <returns>The html string of the plugin to be shown on the html result page</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="context"/> is <c>null</c></exception>
        public string RenderResultHtml(HttpContext context)
        {
            if (context == null) throw new ArgumentNullException("context");
            return RenderResultHtml(new HttpContextWrapper(context));
        }

        /// <summary>
        /// Renders the plugin on the html result page
        /// </summary>
        /// <param name="context">The http context</param>
        /// <returns>The html string of the plugin to be shown on the html result page</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="context"/> is <c>null</c></exception>
        public string RenderResultHtml(HttpContextBase context)
        {
            if (context == null) throw new ArgumentNullException("context");
            throw new NotImplementedException();
        }



    }

}
