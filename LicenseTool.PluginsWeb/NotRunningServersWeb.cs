﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using LicenseTool.Plugins;
using LicenseTool.PluginsWeb;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using LicenseTool.ClassLibrary;

namespace LicenseTool.PluginsWeb
{

    /// <summary>
    /// Plugin to get all not running servers
    /// </summary>
    public class NotRunningServersWeb : NotRunningServers, IWebPlugin
    {

        /// <summary>
        /// Constructor initializing the instance
        /// </summary>
        public NotRunningServersWeb()
            : base()
        {
        }

        /// <summary>
        /// Constructor initializing the instance
        /// </summary>
        /// <param name="db">The databse context to be used</param>
        /// <exception cref="ArgumentNullException">If <paramref name="db"/> is <c>null</c></exception>
        public NotRunningServersWeb(ILicenseToolContext db)
            : base(db)
        {
        }

        /// <summary>
        /// Gets the url path of the plugin icon
        /// </summary>
        /// <param name="context">The http context</param>
        /// <returns>The url path to the plugin icon to be shown</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="context"/> is <c>null</c></exception>
        public string GetIconPath(HttpContext context)
        {
            if (context == null) throw new ArgumentNullException("context");
            return GetIconPath(new HttpContextWrapper(context));
        }

        /// <summary>
        /// Gets the url path of the plugin icon
        /// </summary>
        /// <param name="context">The http context</param>
        /// <returns>The url path to the plugin icon to be shown</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="context"/> is <c>null</c></exception>
        public string GetIconPath(HttpContextBase context)
        {
            if (context == null) throw new ArgumentNullException("context");
            var requestContext = new RequestContext(context, new RouteData());
            var urlHelper = new UrlHelper(requestContext);
            return urlHelper.Content("~/Images/bluescreen.png");
        }

        /// <summary>
        /// Renders the plugin on the index page
        /// </summary>
        /// <param name="context">The http context</param>
        /// <param name="pluginId">The id of that plugin</param>
        /// <returns>The html string of the plugin to be shown on the index page</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="context"/> is <c>null</c></exception>
        public string RenderIndex(HttpContext context, int pluginId)
        {
            if (context == null) throw new ArgumentNullException("context");
            return RenderIndex(new HttpContextWrapper(context), pluginId);
        }

        /// <summary>
        /// Renders the plugin on the index page
        /// </summary>
        /// <param name="context">The http context</param>
        /// <param name="pluginId">The id of that plugin</param>
        /// <returns>The html string of the plugin to be shown on the index page</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="context"/> is <c>null</c></exception>
        public string RenderIndex(HttpContextBase context, int pluginId)
        {
            if (context == null) throw new ArgumentNullException("context");
            var requestContext = new RequestContext(context, new RouteData());
            var urlHelper = new UrlHelper(requestContext);
            string ret = "<div class=\"content-group\">";
            ret += "<a href=\"" + urlHelper.Action("PluginResultTable", "Home", new { pluginId = pluginId }) + "\">";
            ret += "<img src=\"" + GetIconPath(context) + "\" class=\"needstooltip\" title=\"" + this.Name + "\" /><br />";
            ret += this.Name + "</a>\n";
            ret += "</div>";
            return ret;
        }


        /// <summary>
        /// Renders the plugin on the html result page
        /// </summary>
        /// <param name="context">The http context</param>
        /// <returns>The html string of the plugin to be shown on the html result page</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="context"/> is <c>null</c></exception>
        public string RenderResultHtml(HttpContext context)
        {
            if (context == null) throw new ArgumentNullException("context");
            return RenderResultHtml(new HttpContextWrapper(context));
        }

        /// <summary>
        /// Renders the plugin on the html result page
        /// </summary>
        /// <param name="context">The http context</param>
        /// <returns>The html string of the plugin to be shown on the html result page</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="context"/> is <c>null</c></exception>
        public string RenderResultHtml(HttpContextBase context)
        {
            if (context == null) throw new ArgumentNullException("context");
            throw new NotImplementedException();
        }

        /// <summary>
        /// Renders the plugin on the table result page
        /// </summary>
        /// <param name="context">The http context</param>
        /// <returns>The html string array of the plugin to be shown on the table result page</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="context"/> is <c>null</c></exception>
        public string[][] BuildResultTable(HttpContext context)
        {
            if (context == null) throw new ArgumentNullException("context");
            return BuildResultTable(new HttpContextWrapper(context));
        }

        /// <summary>
        /// Renders the plugin on the table result page
        /// </summary>
        /// <param name="context">The http context</param>
        /// <returns>The html string array of the plugin to be shown on the table result page</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="context"/> is <c>null</c></exception>
        public string[][] BuildResultTable(HttpContextBase context)
        {
            if (context == null) throw new ArgumentNullException("context");
            return base.BuildResultTable();
        }
    }

}