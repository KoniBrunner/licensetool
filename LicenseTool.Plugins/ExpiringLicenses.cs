﻿using LicenseTool.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace LicenseTool.Plugins
{

    /// <summary>
    /// Plugin to get all expiring licenses
    /// </summary>
    public class ExpiringLicenses : IPlugin
    {

        private ILicenseToolContext db = null;

        /// <summary>
        /// Constructor initializing the instance
        /// </summary>
        public ExpiringLicenses()
        {
            this.db = new LicenseToolDatabaseEntities();
        }

        /// <summary>
        /// Constructor initializing the instance
        /// </summary>
        /// <param name="db">The databse context to be used</param>
        /// <exception cref="ArgumentNullException">If <paramref name="db"/> is <c>null</c></exception>
        public ExpiringLicenses(ILicenseToolContext db)
        {
            if (db == null) throw new ArgumentNullException("db");
            this.db = db;
        }

        /// <summary>
        /// The name of this plugin
        /// </summary>
        public string Name
        {
            get
            {
                return LicenseTool.ResourceLibrary.Common.ExpiringLicenses;
            }
        }

        /// <summary>
        /// The description of this plugin
        /// </summary>
        public string Description
        {
            get
            {
                return LicenseTool.ResourceLibrary.Common.ExpiringLicensesDesc;
            }
        }

        /// <summary>
        /// The result of the plugin as a table
        /// </summary>
        /// <returns>Plugin result as a table</returns>
        public virtual string[][] BuildResultTable()
        {
            string[][] result = new string[0][];

            var license = db.License.Include(l => l.NavLicServer);

            DateTime checkDate = DateTime.Now.AddMonths(1);
            List<License> foundItems = db.License.Where(l => l.OrderEndDate < checkDate)
                .OrderBy(l => l.NavLicServer.Name).ThenBy(l => l.FeatureName).ToList();

            if (foundItems.Count > 0)
            {
                result = new string[foundItems.Count + 1][];

                result[0] = new string[4];
                result[0][0] = LicenseTool.ResourceLibrary.Model.LicServer;
                result[0][1] = LicenseTool.ResourceLibrary.Model.License;
                result[0][2] = LicenseTool.ResourceLibrary.Model.OrderName;
                result[0][3] = LicenseTool.ResourceLibrary.Model.OrderEndDate;

                for (int i = 0; i < foundItems.Count; i++)
                {
                    result[i + 1] = new string[4];
                    result[i + 1][0] = foundItems[i].NavLicServer.Name;
                    result[i + 1][1] = foundItems[i].FeatureName;
                    result[i + 1][2] = foundItems[i].OrderName;
                    result[i + 1][3] = foundItems[i].OrderEndDate == null ? "" : foundItems[i].OrderEndDate.Value.ToString("d");
                }
            }

            return result;
        }
    }
}
