﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LicenseTool.Plugins
{

    /// <summary>
    /// An interface describing a plugin used in the service
    /// </summary>
    public interface IPlugin
    {
        /// <summary>
        /// Name of the plugin
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Description of the plugin
        /// </summary>
        string Description { get; }

        /// <summary>
        /// The result of the plugin as a table
        /// </summary>
        /// <returns>Plugin result as a table</returns>
        string[][] BuildResultTable();
    }

}
