﻿using LicenseTool.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LicenseTool.Plugins
{

    /// <summary>
    /// Plugin to get all not running servers
    /// </summary>
    public class NotRunningServers : IPlugin
    {
        private ILicenseToolContext db = null;

        /// <summary>
        /// Constructor initializing the instance
        /// </summary>
        public NotRunningServers()
        {
            this.db = new LicenseToolDatabaseEntities();
        }

        /// <summary>
        /// Constructor initializing the instance
        /// </summary>
        /// <param name="db">The databse context to be used</param>
        /// <exception cref="ArgumentNullException">If <paramref name="db"/> is <c>null</c></exception>
        public NotRunningServers(ILicenseToolContext db)
        {
            if (db == null) throw new ArgumentNullException("db");
            this.db = db;
        }

        /// <summary>
        /// The name of this plugin
        /// </summary>
        public string Name
        {
            get
            {
                return LicenseTool.ResourceLibrary.Common.NotRunningServers;
            }
        }

        /// <summary>
        /// The description of this plugin
        /// </summary>
        public string Description
        {
            get
            {
                return LicenseTool.ResourceLibrary.Common.NotRunningServersDesc;
            }
        }

        /// <summary>
        /// The result of the plugin as a table
        /// </summary>
        /// <returns>Plugin result as a table</returns>
        public string[][] BuildResultTable()
        {
            string[][] result = new string[0][];

            List<LicServer> foundItems = db.LicServer.Where(s => s.ServerStatus != (short)ServerAndVendorStatus.UP ||
                s.VendorStatus != (short)ServerAndVendorStatus.UP)
                .OrderBy(s => s.Name).ToList();

            if (foundItems.Count > 0)
            {
                result = new string[foundItems.Count + 1][];

                result[0] = new string[3];
                result[0][0] = LicenseTool.ResourceLibrary.Model.LicServer;
                result[0][1] = LicenseTool.ResourceLibrary.Model.ServerStatus;
                result[0][2] = LicenseTool.ResourceLibrary.Model.VendorStatus;

                for (int i = 0; i < foundItems.Count; i++)
                {
                    result[i + 1] = new string[3];
                    result[i + 1][0] = foundItems[i].Name;
                    result[i + 1][1] = EnumHelper.GetEnumValueName(typeof(ServerAndVendorStatus),
                        (Enum)Enum.ToObject(typeof(ServerAndVendorStatus), foundItems[i].ServerStatus));
                    result[i + 1][2] = EnumHelper.GetEnumValueName(typeof(ServerAndVendorStatus),
                        (Enum)Enum.ToObject(typeof(ServerAndVendorStatus), foundItems[i].VendorStatus));
                }
            }

            return result;
        }
    }
}
