﻿using System;
using System.Web;
using System.Web.Optimization;

namespace LicenseTool.WebApplication
{

    /// <summary>
    /// Configures the bundles
    /// </summary>
    public class BundleConfig
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        /// <summary>
        /// Registers the used bundles
        /// </summary>
        /// <param name="bundles">The actual bundle collection</param>
        /// <exception cref="ArgumentNullException">If <paramref name="bundles"/> is <c>null</c></exception>
        public static void RegisterBundles(BundleCollection bundles)
        {
            if (log.IsDebugEnabled) log.Debug("RegisterBundles");

            if (bundles == null) throw new ArgumentNullException("bundles");

            bundles.Add(new ScriptBundle("~/bundles/dropzonescripts").Include(
                                 "~/Scripts/dropzone/dropzone.js"));
            bundles.Add(new StyleBundle("~/bundles/dropzonescss").Include(
                                 "~/Content/Dropzone.css"));            
            
            
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryi18n").Include(
                        "~/Scripts/jquery-ui-i18n.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/globalize").Include(
                        "~/Scripts/cldr.js",
                        "~/Scripts/cldr/event.js",
                        "~/Scripts/cldr/supplemental.js",
                        "~/Scripts/cldr/unresolved.js",
                        "~/Scripts/globalize.js",
                        "~/Scripts/globalize/message.js",
                        "~/Scripts/globalize/number.js",
                        "~/Scripts/globalize/plural.js",
                        "~/Scripts/globalize/date.js",
                        "~/Scripts/globalize/currency.js",
                        "~/Scripts/globalize/relative-time.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/imagesloaded").Include(
                        "~/Scripts/imagesloaded.pkgd.js"));            

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/cssp").Include("~/Content/print.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));

            if (log.IsDebugEnabled) log.Debug("RegisterBundles Done");
        }
    }
}