﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace LicenseTool.WebApplication
{

    /// <summary>
    /// Configures the http routes
    /// </summary>
    public class RouteConfig
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Registers http routes
        /// </summary>
        /// <param name="routes">The actual route collection</param>
        /// <exception cref="ArgumentNullException">If <paramref name="routes"/> is <c>null</c></exception>
        public static void RegisterRoutes(RouteCollection routes)
        {
            if (log.IsDebugEnabled) log.Debug("RegisterRoutes");

            if (routes == null) throw new ArgumentNullException("routes");

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "LicenseTool.WebApplication.Controllers" }
            );

            if (log.IsDebugEnabled) log.Debug("RegisterRoutes Done");
        }
    }
}