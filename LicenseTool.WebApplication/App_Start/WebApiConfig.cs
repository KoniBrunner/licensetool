﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace LicenseTool.WebApplication
{

    /// <summary>
    /// Configures the web api
    /// </summary>
    public static class WebApiConfig
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Registers api routes
        /// </summary>
        /// <param name="config">The actual http configuration</param>
        /// <exception cref="ArgumentNullException">If <paramref name="config"/> is <c>null</c></exception>
        public static void Register(HttpConfiguration config)
        {
            if (log.IsDebugEnabled) log.Debug("Register");

            if (config == null) throw new ArgumentNullException("config");

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Uncomment the following line of code to enable query support for actions with an IQueryable or IQueryable<T> return type.
            // To avoid processing unexpected or malicious queries, use the validation settings on QueryableAttribute to validate incoming queries.
            // For more information, visit http://go.microsoft.com/fwlink/?LinkId=279712.
            //config.EnableQuerySupport();

            if (log.IsDebugEnabled) log.Debug("Register Done");
        }
    }
}