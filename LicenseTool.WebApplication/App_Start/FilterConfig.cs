﻿using System.Web;
using System.Web.Mvc;

namespace LicenseTool.WebApplication
{

    /// <summary>
    /// Configures the filters
    /// </summary>
    public class FilterConfig
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Registers the used filters
        /// </summary>
        /// <param name="filters">The actual filter collection</param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            if (log.IsDebugEnabled) log.Debug("RegisterGlobalFilters");

            if (log.IsDebugEnabled) log.Debug("RegisterGlobalFilters Done");
            filters.Add(new HandleErrorAttribute());
        }
    }
}