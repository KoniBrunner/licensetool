﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;

namespace LicenseTool.WebApplication
{

    /// <summary>
    /// Class to export csv files
    /// </summary>
    public class CsvExporter
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Builds the csv content from a collection
        /// </summary>
        /// <param name="collection">The collection to build the csv from</param>
        /// <returns>The csv content</returns>
        public static string GetCsvData(IList collection)
        {
            if (log.IsDebugEnabled) log.Debug("CollectionToCsv");

            if (collection == null) throw new ArgumentNullException("collection", "Value can not be null or nothing!");
            StringBuilder sb = new StringBuilder();
            for (int index = 0; index < collection.Count; index++)
            {
                object item = collection[index];
                if (index == 0)
                {
                    sb.Append(GetCsvItemData(item, true));
                }
                sb.Append(GetCsvItemData(item, false));
                sb.Append(Environment.NewLine);
            }

            if (log.IsDebugEnabled) log.Debug("CollectionToCsv Done");
            return sb.ToString();
        }

        /// <summary>
        /// Gets a csv row
        /// </summary>
        /// <param name="obj">The object to build the row from</param>
        /// <param name="isHeader">True if the header should be built</param>
        /// <returns>The csv row</returns>
        private static string GetCsvItemData(object obj, bool isHeader)
        {
            if (log.IsDebugEnabled) log.Debug("ObjectToCsvData");

            if (obj == null)
            {
                throw new ArgumentNullException("obj", "Value can not be null or Nothing!");
            }
            StringBuilder sb = new StringBuilder();
            Type t = obj.GetType();
            PropertyInfo[] pi = t.GetProperties();
            for (int index = 0; index < pi.Length; index++)
            {
                if (pi[index].GetGetMethod().IsVirtual) continue;
                if (isHeader)
                    sb.Append(pi[index].Name);
                else
                    sb.Append(pi[index].GetValue(obj, null));
                if (index < pi.Length - 1)
                {
                    sb.Append(",");
                }
            }

            if (log.IsDebugEnabled) log.Debug("ObjectToCsvData Done");
            return sb.ToString();
        }

    }
}