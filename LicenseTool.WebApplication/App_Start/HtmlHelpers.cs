﻿using LicenseTool.ClassLibrary;
using LicenseTool.PluginsWeb;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using Expression = System.Linq.Expressions.Expression;

namespace LicenseTool.WebApplication
{

    /// <summary>
    /// Html helper extension class
    /// </summary>
    public static class HtmlHelpers
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Renders the list of plugins
        /// </summary>
        /// <typeparam name="TModel">The model of the actual view</typeparam>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <returns>A html string of all plugins</returns>
        public static MvcHtmlString PluginList<TModel>(this HtmlHelper<TModel> htmlHelper)
        {
            if (log.IsDebugEnabled) log.Debug("PluginList");

            string retVal = "";

            foreach (string plug in WebApplication.Properties.Settings.Default.Plugins)
            {
                IWebPlugin item = Activator.CreateInstance(Type.GetType(plug), new object[] { }) as IWebPlugin;
                retVal += item.RenderIndex(htmlHelper.ViewContext.HttpContext, WebApplication.Properties.Settings.Default.Plugins.IndexOf(plug));
            }

            if (log.IsDebugEnabled) log.Debug("PluginList Done");
            return new MvcHtmlString(retVal);
        }

        /// <summary>
        /// Gets all attributes of a collection or object and returns them as dictionary
        /// </summary>
        /// <param name="htmlAttributes">The collection or object</param>
        /// <returns>A dictionary with all attributes</returns>
        private static IDictionary<string, object> GetAttributes(object htmlAttributes)
        {
            if (log.IsDebugEnabled) log.Debug("GetAttributes");

            IDictionary<string, object> attributes = null;
            if (htmlAttributes is IDictionary<string, object>)
                attributes = htmlAttributes as IDictionary<string, object>;
            else
                attributes = new RouteValueDictionary(htmlAttributes);

            if (log.IsDebugEnabled) log.Debug("GetAttributes Done");
            return attributes;
        }

        //http://stackoverflow.com/questions/2386365/maxlength-attribute-of-a-text-box-from-the-dataannotations-stringlength-in-asp-n
        /// <summary>
        /// Gets an editor for a specific property for search purposes
        /// </summary>
        /// <typeparam name="TModel">The model of the actual view</typeparam>
        /// <typeparam name="TProperty">The type of the property we have to build the editor for</typeparam>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="expression">The expression to the property</param>
        /// <param name="templateName">The template name to be used if any</param>
        /// <param name="htmlAttributes">Html attributes to be applied to the editor</param>
        /// <returns>A html string with the editor for the property</returns>
        public static MvcHtmlString SearchEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            string templateName,
            object htmlAttributes)
        {
            if (log.IsDebugEnabled) log.Debug("SearchEditorFor");

            IDictionary<string, object> attributes = GetAttributes(htmlAttributes);
            PrepareSearchAttributes(htmlHelper, expression, attributes);

            if (log.IsDebugEnabled) log.Debug("SearchEditorFor Done");
            return SizedEditorFor(htmlHelper, expression, templateName, attributes);
        }

        /// <summary>
        /// Prepares all html attributes for the search editor
        /// </summary>
        /// <typeparam name="TModel">The model of the actual view</typeparam>
        /// <typeparam name="TProperty">The type of the property we have to build the editor for</typeparam>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="expression">The expression to the property</param>
        /// <param name="attributes">Predefined html attributes to be applied to the editor</param>
        private static void PrepareSearchAttributes<TModel, TProperty>(HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IDictionary<string, object> attributes)
        {
            if (log.IsDebugEnabled) log.Debug("PrepareSearchAttributes");

            string htmlClass = "";
            if (attributes.ContainsKey("class"))
                htmlClass = attributes["class"] + " resetWidth";
            else
                htmlClass = "resetWidth";

            string toolTip = GetToolTip(htmlHelper, expression);
            if (string.IsNullOrEmpty(toolTip))
                toolTip = LicenseTool.ResourceLibrary.Common.SearchToolTip;
            attributes.Add("title", toolTip);
            htmlClass += " needstooltip";

            if (attributes.ContainsKey("class"))
                attributes["class"] += htmlClass;
            else
                attributes.Add("class", htmlClass);

            if (log.IsDebugEnabled) log.Debug("PrepareSearchAttributes Done");
        }

        /// <summary>
        /// Gets an editor for a specific property for search purposes
        /// </summary>
        /// <typeparam name="TModel">The model of the actual view</typeparam>
        /// <typeparam name="TProperty">The type of the property we have to build the editor for</typeparam>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="expression">The expression to the property</param>
        /// <param name="htmlAttributes">Html attributes to be applied to the editor</param>
        /// <returns>A html string with the editor for the property</returns>
        public static MvcHtmlString SearchEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            object htmlAttributes)
        {
            if (log.IsDebugEnabled) log.Debug("SearchEditorFor");

            IDictionary<string, object> attributes = GetAttributes(htmlAttributes);
            PrepareSearchAttributes(htmlHelper, expression, attributes);

            if (log.IsDebugEnabled) log.Debug("SearchEditorFor Done");
            return SizedEditorFor(htmlHelper, expression, "Search", attributes);
        }

        /// <summary>
        /// Gets an editor for a specific property for search purposes
        /// </summary>
        /// <typeparam name="TModel">The model of the actual view</typeparam>
        /// <typeparam name="TProperty">The type of the property we have to build the editor for</typeparam>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="expression">The expression to the property</param>
        /// <returns>A html string with the editor for the property</returns>
        public static MvcHtmlString SearchEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression)
        {
            if (log.IsDebugEnabled) log.Debug("SearchEditorFor");

            IDictionary<string, object> attributes = new RouteValueDictionary();
            PrepareSearchAttributes(htmlHelper, expression, attributes);

            if (log.IsDebugEnabled) log.Debug("SearchEditorFor Done");
            return SizedEditorFor(htmlHelper, expression, "Search", attributes);
        }

        /// <summary>
        /// Gets a sized editor for a specific property
        /// </summary>
        /// <typeparam name="TModel">The model of the actual view</typeparam>
        /// <typeparam name="TProperty">The type of the property we have to build the editor for</typeparam>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="expression">The expression to the property</param>
        /// <returns>A html string with the editor for the property</returns>
        public static MvcHtmlString SizedEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression)
        {
            if (log.IsDebugEnabled) log.Debug("SizedEditorFor");

            IDictionary<string, object> attributes = new RouteValueDictionary();
            attributes.Add("class", "resetWidth");

            if (log.IsDebugEnabled) log.Debug("SizedEditorFor Done");
            return SizedEditorFor(htmlHelper, expression, null, attributes);
        }

        /// <summary>
        /// Gets a sized editor for a specific property
        /// </summary>
        /// <typeparam name="TModel">The model of the actual view</typeparam>
        /// <typeparam name="TProperty">The type of the property we have to build the editor for</typeparam>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="expression">The expression to the property</param>
        /// <param name="htmlAttributes">Html attributes to be applied to the editor</param>
        /// <returns>A html string with the editor for the property</returns>
        public static MvcHtmlString SizedEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            object htmlAttributes)
        {
            if (log.IsDebugEnabled) log.Debug("SizedEditorFor");

            if (log.IsDebugEnabled) log.Debug("SizedEditorFor Done");
            return SizedEditorFor(htmlHelper, expression, null, htmlAttributes);
        }

        /// <summary>
        /// Gets a sized editor for a specific property
        /// </summary>
        /// <typeparam name="TModel">The model of the actual view</typeparam>
        /// <typeparam name="TProperty">The type of the property we have to build the editor for</typeparam>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="expression">The expression to the property</param>
        /// <param name="templateName">The template name to be used if any</param>
        /// <param name="htmlAttributes">Html attributes to be applied to the editor</param>
        /// <returns>A html string with the editor for the property</returns>
        public static MvcHtmlString SizedEditorFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            string templateName,
            object htmlAttributes)
        {
            if (log.IsDebugEnabled) log.Debug("SizedEditorFor");

            IDictionary<string, object> attributes = GetAttributes(htmlAttributes);
            string htmlClass = (string)attributes["class"];
            if (htmlClass == null) htmlClass = "";

            if (!htmlClass.Contains("needstooltip"))
            {
                string toolTip = GetToolTip(htmlHelper, expression);
                if (!string.IsNullOrEmpty(toolTip))
                {
                    attributes.Add("title", toolTip);
                    htmlClass += " needstooltip";
                }
            }

            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, new ViewDataDictionary<TModel>(htmlHelper.ViewDataContainer.ViewData));
            PropertyInfo propInfo = metadata.ContainerType.GetProperty(metadata.PropertyName);
            SetupLength(propInfo, attributes);

            if (propInfo.PropertyType == typeof(DateTime) || propInfo.PropertyType == typeof(DateTime?))
                htmlClass += " datefield";

            if (attributes.ContainsKey("class"))
                attributes["class"] = htmlClass;
            else
                attributes.Add("class", htmlClass);

            if (attributes.ContainsKey("data-val"))
                attributes["data-val"] = "false";
            else
                attributes.Add("data-val", "false");

            if (log.IsDebugEnabled) log.Debug("SizedEditorFor Done");
            if (!string.IsNullOrEmpty(templateName))
                return htmlHelper.EditorFor(expression, templateName, new { HtmlAttributes = attributes });
            else
                return htmlHelper.EditorFor(expression, "Sized", new { HtmlAttributes = attributes });
        }

        /// <summary>
        /// Gets the enum value name for a specific value
        /// </summary>
        /// <typeparam name="TModel">The model of the actual view</typeparam>
        /// <typeparam name="TValue">The type of the value we have to get the name for</typeparam>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="expression">The expression to the property</param>
        /// <param name="enumType">The type of the enum the value is of</param>
        /// <returns>The name of the value</returns>
        public static MvcHtmlString DisplayLocalizedForEnum<TModel, TValue>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TValue>> expression, Type enumType)
        {
            if (log.IsDebugEnabled) log.Debug("DisplayLocalizedForEnum");

            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, new ViewDataDictionary<TModel>(htmlHelper.ViewDataContainer.ViewData));
            string ret = "";
            if (metadata.Model != null)
            {
                Enum enumValue = (Enum)Enum.ToObject(enumType, metadata.Model);
                ret = EnumHelper.GetEnumValueName(enumType, enumValue);
            }

            if (log.IsDebugEnabled) log.Debug("DisplayLocalizedForEnum Done");
            return new MvcHtmlString(ret);
        }

        /// <summary>
        /// Gets a drop down for a specific property for search purposes
        /// </summary>
        /// <typeparam name="TModel">The model of the actual view</typeparam>
        /// <typeparam name="TProperty">The type of the property we have to build the drop down for</typeparam>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="expression">The expression to the property</param>
        /// <param name="selectList">The select list to be attached to the drop down</param>
        /// <returns>A html string with the drop down for the property</returns>
        public static MvcHtmlString SearchDropDownListFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            IEnumerable<SelectListItem> selectList)
        {
            if (log.IsDebugEnabled) log.Debug("SearchDropDownListFor");

            if (log.IsDebugEnabled) log.Debug("SearchDropDownListFor Done");
            return SearchDropDownListFor<TModel, TProperty>(
                htmlHelper,
                expression,
                selectList,
                null,
                null);
        }

        /// <summary>
        /// Gets a drop down for a specific property for search purposes
        /// </summary>
        /// <typeparam name="TModel">The model of the actual view</typeparam>
        /// <typeparam name="TProperty">The type of the property we have to build the drop down for</typeparam>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="expression">The expression to the property</param>
        /// <param name="selectList">The select list to be attached to the drop down</param>
        /// <param name="htmlAttributes">Html attributes to be applied to the drop down</param>
        /// <returns>A html string with the drop down for the property</returns>
        public static MvcHtmlString SearchDropDownListFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            IEnumerable<SelectListItem> selectList,
            object htmlAttributes)
        {
            if (log.IsDebugEnabled) log.Debug("SearchDropDownListFor");

            if (log.IsDebugEnabled) log.Debug("SearchDropDownListFor Done");
            return SearchDropDownListFor<TModel, TProperty>(
                htmlHelper,
                expression,
                selectList,
                null,
                htmlAttributes);
        }

        /// <summary>
        /// Gets a drop down for a specific property for search purposes
        /// </summary>
        /// <typeparam name="TModel">The model of the actual view</typeparam>
        /// <typeparam name="TProperty">The type of the property we have to build the drop down for</typeparam>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="expression">The expression to the property</param>
        /// <param name="selectList">The select list to be attached to the drop down</param>
        /// <param name="optionLabel">The template name to be used if any</param>
        /// <returns>A html string with the drop down for the property</returns>
        public static MvcHtmlString SearchDropDownListFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            IEnumerable<SelectListItem> selectList,
            string optionLabel)
        {
            if (log.IsDebugEnabled) log.Debug("SearchDropDownListFor");

            if (log.IsDebugEnabled) log.Debug("SearchDropDownListFor Done");
            return SearchDropDownListFor<TModel, TProperty>(
                htmlHelper,
                expression,
                selectList,
                optionLabel,
                null);
        }

        /// <summary>
        /// Gets a drop down for a specific property for search purposes
        /// </summary>
        /// <typeparam name="TModel">The model of the actual view</typeparam>
        /// <typeparam name="TProperty">The type of the property we have to build the drop down for</typeparam>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="expression">The expression to the property</param>
        /// <param name="selectList">The select list to be attached to the drop down</param>
        /// <param name="optionLabel">The template name to be used if any</param>
        /// <param name="htmlAttributes">Html attributes to be applied to the drop down</param>
        /// <returns>A html string with the drop down for the property</returns>
        public static MvcHtmlString SearchDropDownListFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            IEnumerable<SelectListItem> selectList,
            string optionLabel,
            object htmlAttributes)
        {
            if (log.IsDebugEnabled) log.Debug("SearchDropDownListFor");

            IDictionary<string, object> attributes = new RouteValueDictionary(htmlAttributes);
            string htmlClass = "resetWidth filter";
            
            string toolTip = GetToolTip(htmlHelper, expression);
            if (!string.IsNullOrEmpty(toolTip))
            {
                attributes.Add("title", toolTip);
                htmlClass += " needstooltip";
            }

            attributes.Add("size", "1");
            attributes.Add("class", htmlClass);
            attributes.Add("data-val", "false");

            if (log.IsDebugEnabled) log.Debug("SearchDropDownListFor Done");
            return htmlHelper.DropDownListFor(expression, selectList, optionLabel, attributes);
        }

        //public static MvcHtmlString LabelForModelProp<TModel>(this HtmlHelper<TModel> helper, TModel model, PropertyInfo propInfo)
        //{
        //    if (log.IsDebugEnabled) log.Debug("LabelForModelProp");

        //    if (log.IsDebugEnabled) log.Debug("LabelForModelProp Done");
        //    return LabelForModelProp(helper, model, propInfo.Name);
        //}

        //public static MvcHtmlString LabelForModelProp<TModel>(this HtmlHelper<TModel> helper, TModel model, string propName)
        //{
        //    if (log.IsDebugEnabled) log.Debug("LabelForModelProp");

        //    LambdaExpression exp = GetLambdaExpression(model, propName);

        //    if (log.IsDebugEnabled) log.Debug("LabelForModelProp Done");
        //    return LabelExtensions.LabelFor(helper, (dynamic)exp);
        //}

        //public static MvcHtmlString EditorForModelProp<TModel>(this HtmlHelper<TModel> helper, TModel model, PropertyInfo propInfo)
        //{
        //    if (log.IsDebugEnabled) log.Debug("EditorForModelProp");

        //    if (log.IsDebugEnabled) log.Debug("EditorForModelProp Done");
        //    return EditorForModelProp(helper, model, propInfo.Name);
        //}

        //public static MvcHtmlString EditorForModelProp<TModel>(this HtmlHelper<TModel> helper, TModel model, string propName, object additionalViewData = null)
        //{
        //    if (log.IsDebugEnabled) log.Debug("EditorForModelProp");

        //    LambdaExpression exp = GetLambdaExpression(model, propName);

        //    if (log.IsDebugEnabled) log.Debug("EditorForModelProp Done");
        //    if (additionalViewData == null)
        //        return EditorExtensions.EditorFor(helper, (dynamic)exp);
        //    return InputExtensions.TextBoxFor(helper, (dynamic)exp, additionalViewData);
        //}

        //public static MvcHtmlString ValidationMessageForModelProp<TModel>(this HtmlHelper<TModel> helper, TModel model, PropertyInfo propInfo)
        //{
        //    if (log.IsDebugEnabled) log.Debug("ValidationMessageForModelProp");

        //    if (log.IsDebugEnabled) log.Debug("ValidationMessageForModelProp Done");
        //    return ValidationMessageForModelProp(helper, model, propInfo.Name);
        //}

        //public static MvcHtmlString ValidationMessageForModelProp<TModel>(this HtmlHelper<TModel> helper, TModel model, string propName)
        //{
        //    if (log.IsDebugEnabled) log.Debug("ValidationMessageForModelProp");

        //    LambdaExpression exp = GetLambdaExpression(model, propName);

        //    if (log.IsDebugEnabled) log.Debug("ValidationMessageForModelProp Done");
        //    return ValidationExtensions.ValidationMessageFor(helper, (dynamic)exp);
        //}

        //public static MvcHtmlString DisplayNameForModelProp<TModel>(this HtmlHelper<TModel> helper, TModel model, PropertyInfo propInfo)
        //{
        //    if (log.IsDebugEnabled) log.Debug("DisplayNameForModelProp");

        //    if (log.IsDebugEnabled) log.Debug("DisplayNameForModelProp Done");
        //    return DisplayNameForModelProp(helper, model, propInfo.Name);
        //}

        //public static MvcHtmlString DisplayNameForModelProp<TModel>(this HtmlHelper<TModel> helper, TModel model, string propName)
        //{
        //    if (log.IsDebugEnabled) log.Debug("DisplayNameForModelProp");

        //    LambdaExpression exp = GetLambdaExpression(model, propName);

        //    if (log.IsDebugEnabled) log.Debug("DisplayNameForModelProp Done");
        //    return DisplayNameExtensions.DisplayNameFor(helper, (dynamic)exp);
        //}

        //public static MvcHtmlString DisplayForModelProp<TModel>(this HtmlHelper<TModel> helper, TModel model, PropertyInfo propInfo)
        //{
        //    if (log.IsDebugEnabled) log.Debug("DisplayForModelProp");

        //    if (log.IsDebugEnabled) log.Debug("DisplayForModelProp Done");
        //    return DisplayForModelProp(helper, model, propInfo.Name);
        //}

        //public static MvcHtmlString DisplayForModelProp<TModel>(this HtmlHelper<TModel> helper, TModel model, string propName)
        //{
        //    if (log.IsDebugEnabled) log.Debug("DisplayForModelProp");

        //    LambdaExpression exp = GetLambdaExpression(model, propName);

        //    if (log.IsDebugEnabled) log.Debug("DisplayForModelProp Done");
        //    return DisplayExtensions.DisplayFor(helper, (dynamic)exp);
        //}

        //public static MvcHtmlString DisplayForEnumerableModelProp<TModel>(this HtmlHelper<IEnumerable<TModel>> helper, TModel model, PropertyInfo propInfo)
        //{
        //    if (log.IsDebugEnabled) log.Debug("DisplayForEnumerableModelProp");

        //    if (log.IsDebugEnabled) log.Debug("DisplayForEnumerableModelProp Done");
        //    return DisplayForEnumerableModelProp(helper, model, propInfo.Name);
        //}

        //public static MvcHtmlString DisplayForEnumerableModelProp<TModel>(this HtmlHelper<IEnumerable<TModel>> helper, TModel model, string propName)
        //{
        //    if (log.IsDebugEnabled) log.Debug("DisplayForEnumerableModelProp");

        //    LambdaExpression exp = GetLambdaExpressionEnumerable(model, propName);

        //    if (log.IsDebugEnabled) log.Debug("DisplayForEnumerableModelProp Done");
        //    return DisplayExtensions.DisplayFor(helper, (dynamic)exp);
        //}

        /// <summary>
        /// Configures the length of a text field
        /// </summary>
        /// <param name="propInfo">The property info to get the string length from</param>
        /// <param name="attributes">Html attributes where the length has to be filled</param>
        private static void SetupLength(PropertyInfo propInfo, IDictionary<string, object> attributes)
        {
            if (log.IsDebugEnabled) log.Debug("SetupLength");

            StringLengthAttribute stringLength = propInfo.GetCustomAttributes(typeof(StringLengthAttribute), false).FirstOrDefault() as StringLengthAttribute;
            if (stringLength != null)
            {
                int maxLen = 20;
                if (stringLength.MaximumLength < maxLen)
                    maxLen = stringLength.MaximumLength;
                attributes.Add("maxlength", stringLength.MaximumLength);
                attributes.Add("size", maxLen);
            }
            else
            {
                if (propInfo.PropertyType == typeof(int) || propInfo.PropertyType == typeof(int?))
                {
                    attributes.Add("size", "2");
                }
                else if (propInfo.PropertyType == typeof(short) || propInfo.PropertyType == typeof(short?))
                {
                    attributes.Add("maxlength", "3");
                    attributes.Add("size", "3");
                }
                else if (propInfo.PropertyType == typeof(DateTime) || propInfo.PropertyType == typeof(DateTime?))
                {
                    attributes.Add("size", "10");
                }
                else if (propInfo.PropertyType == typeof(double) || propInfo.PropertyType == typeof(double?))
                {
                    attributes.Add("size", "10");
                }
                else
                {
                    attributes.Add("size", "5");
                }
            }
            if (log.IsDebugEnabled) log.Debug("SetupLength Done");
        }

        //private static LambdaExpression GetLambdaExpression<TModel>(TModel model, string propName)
        //{
        //    if (log.IsDebugEnabled) log.Debug("GetLambdaExpression");

        //    ParameterExpression fieldName = Expression.Parameter(typeof(TModel), propName);
        //    Expression fieldExpr = Expression.PropertyOrField(Expression.Constant(model), propName);
        //    LambdaExpression exp = Expression.Lambda(
        //        typeof(Func<,>).MakeGenericType(typeof(TModel), fieldExpr.Type),
        //        fieldExpr,
        //        fieldName);

        //    if (log.IsDebugEnabled) log.Debug("GetLambdaExpression Done");
        //    return exp;
        //}

        //private static LambdaExpression GetLambdaExpressionEnumerable<TModel>(TModel model, string propName)
        //{
        //    if (log.IsDebugEnabled) log.Debug("GetLambdaExpressionEnumerable");

        //    ParameterExpression fieldName = Expression.Parameter(typeof(IEnumerable<TModel>), propName);
        //    Expression fieldExpr = Expression.PropertyOrField(Expression.Constant(model), propName);
        //    LambdaExpression exp = Expression.Lambda(
        //        typeof(Func<,>).MakeGenericType(typeof(IEnumerable<TModel>), fieldExpr.Type),
        //        fieldExpr,
        //        fieldName);

        //    if (log.IsDebugEnabled) log.Debug("GetLambdaExpressionEnumerable Done");
        //    return exp;
        //}

        /// <summary>
        /// Gets the tooltip of a property
        /// </summary>
        /// <typeparam name="TModel">The model of the actual view</typeparam>
        /// <typeparam name="TProperty">The type of the property we have to get the tooltip from</typeparam>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="expression">The expression to the property</param>
        /// <returns>The tooltip string</returns>
        private static string GetToolTip<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            if (log.IsDebugEnabled) log.Debug("GetToolTip");

            PropertyInfo propInfo = GetProperty(expression);
            DisplayAttribute disp = propInfo.GetCustomAttribute<DisplayAttribute>();
            if (disp == null)
            {
                Type propDecType = GetPropertyDeclaringType(expression);
                MetadataTypeAttribute metadataType = (MetadataTypeAttribute)propDecType.GetCustomAttributes(typeof(MetadataTypeAttribute), true).FirstOrDefault();
                if (metadataType != null)
                {
                    propInfo = metadataType.MetadataClassType.GetProperty(propInfo.Name);
                    if (propInfo != null)
                    {
                        disp = (DisplayAttribute)propInfo.GetCustomAttributes(typeof(DisplayAttribute), true).SingleOrDefault();
                    }
                }
            }

            if (log.IsDebugEnabled) log.Debug("GetToolTip Done");
            if (disp == null || string.IsNullOrEmpty(disp.GetDescription())) return null;
            return disp.GetDescription();
        }

        //private static TAttribute GetAttribute<TAttribute>(PropertyInfo prop)
        //{
        //    if (log.IsDebugEnabled) log.Debug("GetAttribute");

        //    if (log.IsDebugEnabled) log.Debug("GetAttribute Done");
        //    return (TAttribute)prop.GetCustomAttributes(typeof(TAttribute), true).FirstOrDefault();
        //}

        //private static TAttribute GetAttribute<TAttribute>(Type sourceType)
        //{
        //    if (log.IsDebugEnabled) log.Debug("GetAttribute");

        //    if (log.IsDebugEnabled) log.Debug("GetAttribute Done");
        //    return (TAttribute)sourceType.GetCustomAttributes(typeof(TAttribute), true).FirstOrDefault();
        //}

        /// <summary>
        /// Gets the property info of an expression
        /// </summary>
        /// <typeparam name="TModel">The model of the actual view</typeparam>
        /// <typeparam name="TValue">The type of the value we have to get the property info from</typeparam>
        /// <param name="selector">The lync expression</param>
        /// <returns>The property info for the property</returns>
        private static PropertyInfo GetProperty<TModel, TValue>(Expression<Func<TModel, TValue>> selector)
        {
            if (log.IsDebugEnabled) log.Debug("GetProperty");

            Expression body = selector;
            if (body is LambdaExpression)
                body = ((LambdaExpression)body).Body;
            if (log.IsDebugEnabled) log.Debug("GetProperty Done");
            switch (body.NodeType)
            {
                case ExpressionType.MemberAccess:
                    return (PropertyInfo)((MemberExpression)body).Member;
                default:
                    throw new InvalidOperationException();
            }
        }

        /// <summary>
        /// Gets the declaring type of an expression
        /// </summary>
        /// <typeparam name="TModel">The model of the actual view</typeparam>
        /// <typeparam name="TValue">The type of the value we have to get the declaring type from</typeparam>
        /// <param name="selector">The lync expression</param>
        /// <returns>The declaring type for the property</returns>
        private static Type GetPropertyDeclaringType<TModel, TValue>(Expression<Func<TModel, TValue>> selector)
        {
            if (log.IsDebugEnabled) log.Debug("GetPropertyDeclaringType");

            Expression body = selector;
            if (selector is LambdaExpression)
                body = ((LambdaExpression)selector).Body;

            switch (body.NodeType)
            {
                case ExpressionType.MemberAccess:
                    if (log.IsDebugEnabled) log.Debug("GetPropertyDeclaringType Done");
                    return ((MemberExpression)body).Member.DeclaringType;
                default:
                    if (log.IsDebugEnabled) log.Debug("GetPropertyDeclaringType Done");
                    throw new InvalidOperationException();
            }
        }

        //private static MvcHtmlString NullableBoolCheckBox<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        //{
        //    if (log.IsDebugEnabled) log.Debug("NullableBoolCheckBox");

        //    string content = htmlHelper.EditorFor(expression).ToString();
        //    content = "<div style='visibility: hidden; position:absolute'>" + content + "</div>";
        //    ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
        //    var tagBuilder = new TagBuilder("input");
        //    tagBuilder.Attributes.Add("id", metadata.PropertyName);
        //    tagBuilder.Attributes.Add("class", "ThreeStageCheckBox");
        //    tagBuilder.Attributes.Add("type", "checkbox");
        //    tagBuilder.Attributes.Add("data-value", metadata.Model == null ? string.Empty : metadata.Model.ToString());

        //    if (log.IsDebugEnabled) log.Debug("NullableBoolCheckBox Done");
        //    return new MvcHtmlString(tagBuilder + content);
        //}

        // -------------------------------------------------------
        // ActionLinkWithImage
        // -------------------------------------------------------

        private const string TEMP_REPLACE_STRING = "###!!??!!###";

        /// <summary>
        /// Decorades an ActionLink with an image
        /// </summary>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="linkText">The inner text of the anchor element</param>
        /// <param name="imageSrc">Path to the image to be included</param>
        /// <param name="imageAlt">Alternative text for the image</param>
        /// <param name="imageHtmlAttributes">Additional html attributes for the image like width and height</param>
        /// <param name="actionLinkString">The html string from the action link</param>
        /// <returns>The actionLinkString including the image </returns>
        private static MvcHtmlString DecoradeActionLinkWithImage(HtmlHelper htmlHelper, string linkText, string imageSrc, string imageAlt, object imageHtmlAttributes, MvcHtmlString actionLinkString)
        {
            if (log.IsDebugEnabled) log.Debug("DecoradeActionLinkWithImage");

            IDictionary<string, object> attributes = new RouteValueDictionary(imageHtmlAttributes);
            var url = new UrlHelper(htmlHelper.ViewContext.RequestContext);
            var imgBuilder = new TagBuilder("img");
            imgBuilder.MergeAttribute("src", url.Content(imageSrc));
            foreach (var attribute in attributes)
                imgBuilder.MergeAttribute(attribute.Key, attribute.Value.ToString(), true);
            if (!string.IsNullOrEmpty(imageAlt)) imgBuilder.MergeAttribute("alt", imageAlt);
            string imgHtml = null;
            if (!string.IsNullOrEmpty(linkText))
            {
                imgHtml = imgBuilder.ToString(TagRenderMode.SelfClosing);
                imgHtml += "<br />" + linkText;
            }
            else
            {
                if (!string.IsNullOrEmpty(imageAlt))
                {
                    imgBuilder.MergeAttribute("title", url.Content(imageAlt));
                    imgBuilder.MergeAttribute("class", "needstooltip");
                }
                imgHtml = imgBuilder.ToString(TagRenderMode.SelfClosing);
            }

            if (log.IsDebugEnabled) log.Debug("DecoradeActionLinkWithImage Done");
            return new MvcHtmlString(actionLinkString.ToString().Replace(TEMP_REPLACE_STRING, imgHtml));
        }

        /// <summary>
        /// Returns an anchor element (a element) that contains the virtual path of the specified action including an image
        /// </summary>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="linkText">The inner text of the anchor element</param>
        /// <param name="actionName">The name of the action</param>
        /// <param name="imageSrc">Path to the image to be included</param>
        /// <param name="imageAlt">Alternative text for the image</param>
        /// <returns>An anchor element (a element) inclusing an image</returns>
        public static MvcHtmlString ActionLinkWithImage(this HtmlHelper htmlHelper, string linkText, string actionName, string imageSrc, string imageAlt)
        {
            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage");

            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage Done");
            return DecoradeActionLinkWithImage(htmlHelper, linkText, imageSrc, imageAlt, null, htmlHelper.ActionLink(TEMP_REPLACE_STRING, actionName));
        }

        /// <summary>
        /// Returns an anchor element (a element) that contains the virtual path of the specified action including an image
        /// </summary>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="linkText">The inner text of the anchor element</param>
        /// <param name="actionName">The name of the action</param>
        /// <param name="routeValues">An object that contains the parameters for a route</param>
        /// <param name="imageSrc">Path to the image to be included</param>
        /// <param name="imageAlt">Alternative text for the image</param>
        /// <returns>An anchor element (a element) inclusing an image</returns>
        public static MvcHtmlString ActionLinkWithImage(this HtmlHelper htmlHelper, string linkText, string actionName, object routeValues, string imageSrc, string imageAlt)
        {
            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage");

            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage Done");
            return DecoradeActionLinkWithImage(htmlHelper, linkText, imageSrc, imageAlt, null, htmlHelper.ActionLink(TEMP_REPLACE_STRING, actionName, routeValues));
        }

        /// <summary>
        /// Returns an anchor element (a element) that contains the virtual path of the specified action including an image
        /// </summary>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="linkText">The inner text of the anchor element</param>
        /// <param name="actionName">The name of the action</param>
        /// <param name="routeValues">An object that contains the parameters for a route</param>
        /// <param name="imageSrc">Path to the image to be included</param>
        /// <param name="imageAlt">Alternative text for the image</param>
        /// <returns>An anchor element (a element) inclusing an image</returns>
        public static MvcHtmlString ActionLinkWithImage(this HtmlHelper htmlHelper, string linkText, string actionName, RouteValueDictionary routeValues, string imageSrc, string imageAlt)
        {
            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage");

            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage Done");
            return DecoradeActionLinkWithImage(htmlHelper, linkText, imageSrc, imageAlt, null, htmlHelper.ActionLink(TEMP_REPLACE_STRING, actionName, routeValues));
        }

        /// <summary>
        /// Returns an anchor element (a element) that contains the virtual path of the specified action including an image
        /// </summary>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="linkText">The inner text of the anchor element</param>
        /// <param name="actionName">The name of the action</param>
        /// <param name="controllerName">The name of the controller</param>
        /// <param name="imageSrc">Path to the image to be included</param>
        /// <param name="imageAlt">Alternative text for the image</param>
        /// <returns>An anchor element (a element) inclusing an image</returns>
        public static MvcHtmlString ActionLinkWithImage(this HtmlHelper htmlHelper, string linkText, string actionName, string controllerName, string imageSrc, string imageAlt)
        {
            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage");

            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage Done");
            return DecoradeActionLinkWithImage(htmlHelper, linkText, imageSrc, imageAlt, null, htmlHelper.ActionLink(TEMP_REPLACE_STRING, actionName, controllerName));
        }

        /// <summary>
        /// Returns an anchor element (a element) that contains the virtual path of the specified action including an image
        /// </summary>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="linkText">The inner text of the anchor element</param>
        /// <param name="actionName">The name of the action</param>
        /// <param name="routeValues">An object that contains the parameters for a route</param>
        /// <param name="htmlAttributes">An object that contains the HTML attributes to set for the element</param>
        /// <param name="imageSrc">Path to the image to be included</param>
        /// <param name="imageAlt">Alternative text for the image</param>
        /// <returns>An anchor element (a element) inclusing an image</returns>
        public static MvcHtmlString ActionLinkWithImage(this HtmlHelper htmlHelper, string linkText, string actionName, object routeValues, object htmlAttributes, string imageSrc, string imageAlt)
        {
            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage");

            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage Done");
            return DecoradeActionLinkWithImage(htmlHelper, linkText, imageSrc, imageAlt, null, htmlHelper.ActionLink(TEMP_REPLACE_STRING, actionName, routeValues, htmlAttributes));
        }

        /// <summary>
        /// Returns an anchor element (a element) that contains the virtual path of the specified action including an image
        /// </summary>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="linkText">The inner text of the anchor element</param>
        /// <param name="actionName">The name of the action</param>
        /// <param name="routeValues">An object that contains the parameters for a route</param>
        /// <param name="htmlAttributes">An object that contains the HTML attributes to set for the element</param>
        /// <param name="imageSrc">Path to the image to be included</param>
        /// <param name="imageAlt">Alternative text for the image</param>
        /// <returns>An anchor element (a element) inclusing an image</returns>
        public static MvcHtmlString ActionLinkWithImage(this HtmlHelper htmlHelper, string linkText, string actionName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes, string imageSrc, string imageAlt)
        {
            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage");

            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage Done");
            return DecoradeActionLinkWithImage(htmlHelper, linkText, imageSrc, imageAlt, null, htmlHelper.ActionLink(TEMP_REPLACE_STRING, actionName, routeValues, htmlAttributes));
        }

        /// <summary>
        /// Returns an anchor element (a element) that contains the virtual path of the specified action including an image
        /// </summary>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="linkText">The inner text of the anchor element</param>
        /// <param name="actionName">The name of the action</param>
        /// <param name="controllerName">The name of the controller</param>
        /// <param name="routeValues">An object that contains the parameters for a route</param>
        /// <param name="htmlAttributes">An object that contains the HTML attributes to set for the element</param>
        /// <param name="imageSrc">Path to the image to be included</param>
        /// <param name="imageAlt">Alternative text for the image</param>
        /// <returns>An anchor element (a element) inclusing an image</returns>
        public static MvcHtmlString ActionLinkWithImage(this HtmlHelper htmlHelper, string linkText, string actionName, string controllerName, object routeValues, object htmlAttributes, string imageSrc, string imageAlt)
        {
            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage");

            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage Done");
            return DecoradeActionLinkWithImage(htmlHelper, linkText, imageSrc, imageAlt, null, htmlHelper.ActionLink(TEMP_REPLACE_STRING, actionName, controllerName, routeValues, htmlAttributes));
        }

        /// <summary>
        /// Returns an anchor element (a element) that contains the virtual path of the specified action including an image
        /// </summary>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="linkText">The inner text of the anchor element</param>
        /// <param name="actionName">The name of the action</param>
        /// <param name="controllerName">The name of the controller</param>
        /// <param name="routeValues">An object that contains the parameters for a route</param>
        /// <param name="htmlAttributes">An object that contains the HTML attributes to set for the element</param>
        /// <param name="imageSrc">Path to the image to be included</param>
        /// <param name="imageAlt">Alternative text for the image</param>
        /// <returns>An anchor element (a element) inclusing an image</returns>
        public static MvcHtmlString ActionLinkWithImage(this HtmlHelper htmlHelper, string linkText, string actionName, string controllerName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes, string imageSrc, string imageAlt)
        {
            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage");

            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage Done");
            return DecoradeActionLinkWithImage(htmlHelper, linkText, imageSrc, imageAlt, null, htmlHelper.ActionLink(TEMP_REPLACE_STRING, actionName, controllerName, routeValues, htmlAttributes));
        }

        /// <summary>
        /// Returns an anchor element (a element) that contains the virtual path of the specified action including an image
        /// </summary>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="linkText">The inner text of the anchor element</param>
        /// <param name="actionName">The name of the action</param>
        /// <param name="controllerName">The name of the controller</param>
        /// <param name="protocol">The protocol for the URL, such as "http" or "https"</param>
        /// <param name="hostName">The host name for the URL</param>
        /// <param name="fragment">The URL fragment name (the anchor name)</param>
        /// <param name="routeValues">An object that contains the parameters for a route</param>
        /// <param name="htmlAttributes">An object that contains the HTML attributes to set for the element</param>
        /// <param name="imageSrc">Path to the image to be included</param>
        /// <param name="imageAlt">Alternative text for the image</param>
        /// <returns>An anchor element (a element) inclusing an image</returns>
        public static MvcHtmlString ActionLinkWithImage(this HtmlHelper htmlHelper, string linkText, string actionName, string controllerName, string protocol, string hostName, string fragment, object routeValues, object htmlAttributes, string imageSrc, string imageAlt)
        {
            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage");

            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage Done");
            return DecoradeActionLinkWithImage(htmlHelper, linkText, imageSrc, imageAlt, null, htmlHelper.ActionLink(TEMP_REPLACE_STRING, actionName, controllerName, protocol, hostName, fragment, routeValues, htmlAttributes));
        }

        /// <summary>
        /// Returns an anchor element (a element) that contains the virtual path of the specified action including an image
        /// </summary>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="linkText">The inner text of the anchor element</param>
        /// <param name="actionName">The name of the action</param>
        /// <param name="controllerName">The name of the controller</param>
        /// <param name="protocol">The protocol for the URL, such as "http" or "https"</param>
        /// <param name="hostName">The host name for the URL</param>
        /// <param name="fragment">The URL fragment name (the anchor name)</param>
        /// <param name="routeValues">An object that contains the parameters for a route</param>
        /// <param name="htmlAttributes">An object that contains the HTML attributes to set for the element</param>
        /// <param name="imageSrc">Path to the image to be included</param>
        /// <param name="imageAlt">Alternative text for the image</param>
        /// <returns>An anchor element (a element) inclusing an image</returns>
        public static MvcHtmlString ActionLinkWithImage(this HtmlHelper htmlHelper, string linkText, string actionName, string controllerName, string protocol, string hostName, string fragment, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes, string imageSrc, string imageAlt)
        {
            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage");

            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage Done");
            return DecoradeActionLinkWithImage(htmlHelper, linkText, imageSrc, imageAlt, null, htmlHelper.ActionLink(TEMP_REPLACE_STRING, actionName, controllerName, protocol, hostName, fragment, routeValues, htmlAttributes));
        }

        /// <summary>
        /// Returns an anchor element (a element) that contains the virtual path of the specified action including an image
        /// </summary>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="linkText">The inner text of the anchor element</param>
        /// <param name="actionName">The name of the action</param>
        /// <param name="imageSrc">Path to the image to be included</param>
        /// <param name="imageAlt">Alternative text for the image</param>
        /// <param name="imageHtmlAttributes">An object that contains the HTML attributes to set for the element</param>
        /// <returns>An anchor element (a element) inclusing an image</returns>
        public static MvcHtmlString ActionLinkWithImage(this HtmlHelper htmlHelper, string linkText, string actionName, string imageSrc, string imageAlt, object imageHtmlAttributes)
        {
            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage");

            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage Done");
            return DecoradeActionLinkWithImage(htmlHelper, linkText, imageSrc, imageAlt, imageHtmlAttributes, htmlHelper.ActionLink(TEMP_REPLACE_STRING, actionName));
        }

        /// <summary>
        /// Returns an anchor element (a element) that contains the virtual path of the specified action including an image
        /// </summary>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="linkText">The inner text of the anchor element</param>
        /// <param name="actionName">The name of the action</param>
        /// <param name="routeValues">An object that contains the parameters for a route</param>
        /// <param name="imageSrc">Path to the image to be included</param>
        /// <param name="imageAlt">Alternative text for the image</param>
        /// <param name="imageHtmlAttributes">An object that contains the HTML attributes to set for the element</param>
        /// <returns>An anchor element (a element) inclusing an image</returns>
        public static MvcHtmlString ActionLinkWithImage(this HtmlHelper htmlHelper, string linkText, string actionName, object routeValues, string imageSrc, string imageAlt, object imageHtmlAttributes)
        {
            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage");

            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage Done");
            return DecoradeActionLinkWithImage(htmlHelper, linkText, imageSrc, imageAlt, imageHtmlAttributes, htmlHelper.ActionLink(TEMP_REPLACE_STRING, actionName, routeValues));
        }

        /// <summary>
        /// Returns an anchor element (a element) that contains the virtual path of the specified action including an image
        /// </summary>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="linkText">The inner text of the anchor element</param>
        /// <param name="actionName">The name of the action</param>
        /// <param name="routeValues">An object that contains the parameters for a route</param>
        /// <param name="imageSrc">Path to the image to be included</param>
        /// <param name="imageAlt">Alternative text for the image</param>
        /// <param name="imageHtmlAttributes">An object that contains the HTML attributes to set for the element</param>
        /// <returns>An anchor element (a element) inclusing an image</returns>
        public static MvcHtmlString ActionLinkWithImage(this HtmlHelper htmlHelper, string linkText, string actionName, RouteValueDictionary routeValues, string imageSrc, string imageAlt, object imageHtmlAttributes)
        {
            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage");

            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage Done");
            return DecoradeActionLinkWithImage(htmlHelper, linkText, imageSrc, imageAlt, imageHtmlAttributes, htmlHelper.ActionLink(TEMP_REPLACE_STRING, actionName, routeValues));
        }

        /// <summary>
        /// Returns an anchor element (a element) that contains the virtual path of the specified action including an image
        /// </summary>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="linkText">The inner text of the anchor element</param>
        /// <param name="actionName">The name of the action</param>
        /// <param name="controllerName">The name of the controller</param>
        /// <param name="imageSrc">Path to the image to be included</param>
        /// <param name="imageAlt">Alternative text for the image</param>
        /// <param name="imageHtmlAttributes">An object that contains the HTML attributes to set for the element</param>
        /// <returns>An anchor element (a element) inclusing an image</returns>
        public static MvcHtmlString ActionLinkWithImage(this HtmlHelper htmlHelper, string linkText, string actionName, string controllerName, string imageSrc, string imageAlt, object imageHtmlAttributes)
        {
            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage");

            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage Done");
            return DecoradeActionLinkWithImage(htmlHelper, linkText, imageSrc, imageAlt, imageHtmlAttributes, htmlHelper.ActionLink(TEMP_REPLACE_STRING, actionName, controllerName));
        }

        /// <summary>
        /// Returns an anchor element (a element) that contains the virtual path of the specified action including an image
        /// </summary>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="linkText">The inner text of the anchor element</param>
        /// <param name="actionName">The name of the action</param>
        /// <param name="routeValues">An object that contains the parameters for a route</param>
        /// <param name="htmlAttributes">An object that contains the HTML attributes to set for the element</param>
        /// <param name="imageSrc">Path to the image to be included</param>
        /// <param name="imageAlt">Alternative text for the image</param>
        /// <param name="imageHtmlAttributes">An object that contains the HTML attributes to set for the element</param>
        /// <returns>An anchor element (a element) inclusing an image</returns>
        public static MvcHtmlString ActionLinkWithImage(this HtmlHelper htmlHelper, string linkText, string actionName, object routeValues, object htmlAttributes, string imageSrc, string imageAlt, object imageHtmlAttributes)
        {
            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage");

            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage Done");
            return DecoradeActionLinkWithImage(htmlHelper, linkText, imageSrc, imageAlt, imageHtmlAttributes, htmlHelper.ActionLink(TEMP_REPLACE_STRING, actionName, routeValues, htmlAttributes));
        }

        /// <summary>
        /// Returns an anchor element (a element) that contains the virtual path of the specified action including an image
        /// </summary>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="linkText">The inner text of the anchor element</param>
        /// <param name="actionName">The name of the action</param>
        /// <param name="routeValues">An object that contains the parameters for a route</param>
        /// <param name="htmlAttributes">An object that contains the HTML attributes to set for the element</param>
        /// <param name="imageSrc">Path to the image to be included</param>
        /// <param name="imageAlt">Alternative text for the image</param>
        /// <param name="imageHtmlAttributes">An object that contains the HTML attributes to set for the element</param>
        /// <returns>An anchor element (a element) inclusing an image</returns>
        public static MvcHtmlString ActionLinkWithImage(this HtmlHelper htmlHelper, string linkText, string actionName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes, string imageSrc, string imageAlt, object imageHtmlAttributes)
        {
            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage");

            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage Done");
            return DecoradeActionLinkWithImage(htmlHelper, linkText, imageSrc, imageAlt, imageHtmlAttributes, htmlHelper.ActionLink(TEMP_REPLACE_STRING, actionName, routeValues, htmlAttributes));
        }

        /// <summary>
        /// Returns an anchor element (a element) that contains the virtual path of the specified action including an image
        /// </summary>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="linkText">The inner text of the anchor element</param>
        /// <param name="actionName">The name of the action</param>
        /// <param name="controllerName">The name of the controller</param>
        /// <param name="routeValues">An object that contains the parameters for a route</param>
        /// <param name="htmlAttributes">An object that contains the HTML attributes to set for the element</param>
        /// <param name="imageSrc">Path to the image to be included</param>
        /// <param name="imageAlt">Alternative text for the image</param>
        /// <param name="imageHtmlAttributes">An object that contains the HTML attributes to set for the element</param>
        /// <returns>An anchor element (a element) inclusing an image</returns>
        public static MvcHtmlString ActionLinkWithImage(this HtmlHelper htmlHelper, string linkText, string actionName, string controllerName, object routeValues, object htmlAttributes, string imageSrc, string imageAlt, object imageHtmlAttributes)
        {
            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage");

            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage Done");
            return DecoradeActionLinkWithImage(htmlHelper, linkText, imageSrc, imageAlt, imageHtmlAttributes, htmlHelper.ActionLink(TEMP_REPLACE_STRING, actionName, controllerName, routeValues, htmlAttributes));
        }

        /// <summary>
        /// Returns an anchor element (a element) that contains the virtual path of the specified action including an image
        /// </summary>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="linkText">The inner text of the anchor element</param>
        /// <param name="actionName">The name of the action</param>
        /// <param name="controllerName">The name of the controller</param>
        /// <param name="routeValues">An object that contains the parameters for a route</param>
        /// <param name="htmlAttributes">An object that contains the HTML attributes to set for the element</param>
        /// <param name="imageSrc">Path to the image to be included</param>
        /// <param name="imageAlt">Alternative text for the image</param>
        /// <param name="imageHtmlAttributes">An object that contains the HTML attributes to set for the element</param>
        /// <returns>An anchor element (a element) inclusing an image</returns>
        public static MvcHtmlString ActionLinkWithImage(this HtmlHelper htmlHelper, string linkText, string actionName, string controllerName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes, string imageSrc, string imageAlt, object imageHtmlAttributes)
        {
            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage");

            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage Done");
            return DecoradeActionLinkWithImage(htmlHelper, linkText, imageSrc, imageAlt, imageHtmlAttributes, htmlHelper.ActionLink(TEMP_REPLACE_STRING, actionName, controllerName, routeValues, htmlAttributes));
        }

        /// <summary>
        /// Returns an anchor element (a element) that contains the virtual path of the specified action including an image
        /// </summary>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="linkText">The inner text of the anchor element</param>
        /// <param name="actionName">The name of the action</param>
        /// <param name="controllerName">The name of the controller</param>
        /// <param name="protocol">The protocol for the URL, such as "http" or "https"</param>
        /// <param name="hostName">The host name for the URL</param>
        /// <param name="fragment">The URL fragment name (the anchor name)</param>
        /// <param name="routeValues">An object that contains the parameters for a route</param>
        /// <param name="htmlAttributes">An object that contains the HTML attributes to set for the element</param>
        /// <param name="imageSrc">Path to the image to be included</param>
        /// <param name="imageAlt">Alternative text for the image</param>
        /// <param name="imageHtmlAttributes">An object that contains the HTML attributes to set for the element</param>
        /// <returns>An anchor element (a element) inclusing an image</returns>
        public static MvcHtmlString ActionLinkWithImage(this HtmlHelper htmlHelper, string linkText, string actionName, string controllerName, string protocol, string hostName, string fragment, object routeValues, object htmlAttributes, string imageSrc, string imageAlt, object imageHtmlAttributes)
        {
            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage");

            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage Done");
            return DecoradeActionLinkWithImage(htmlHelper, linkText, imageSrc, imageAlt, imageHtmlAttributes, htmlHelper.ActionLink(TEMP_REPLACE_STRING, actionName, controllerName, protocol, hostName, fragment, routeValues, htmlAttributes));
        }

        /// <summary>
        /// Returns an anchor element (a element) that contains the virtual path of the specified action including an image
        /// </summary>
        /// <param name="htmlHelper">The helper to be extended</param>
        /// <param name="linkText">The inner text of the anchor element</param>
        /// <param name="actionName">The name of the action</param>
        /// <param name="controllerName">The name of the controller</param>
        /// <param name="protocol">The protocol for the URL, such as "http" or "https"</param>
        /// <param name="hostName">The host name for the URL</param>
        /// <param name="fragment">The URL fragment name (the anchor name)</param>
        /// <param name="routeValues">An object that contains the parameters for a route</param>
        /// <param name="htmlAttributes">An object that contains the HTML attributes to set for the element</param>
        /// <param name="imageSrc">Path to the image to be included</param>
        /// <param name="imageAlt">Alternative text for the image</param>
        /// <param name="imageHtmlAttributes">An object that contains the HTML attributes to set for the element</param>
        /// <returns>An anchor element (a element) inclusing an image</returns>
        public static MvcHtmlString ActionLinkWithImage(this HtmlHelper htmlHelper, string linkText, string actionName, string controllerName, string protocol, string hostName, string fragment, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes, string imageSrc, string imageAlt, object imageHtmlAttributes)
        {
            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage");

            if (log.IsDebugEnabled) log.Debug("ActionLinkWithImage Done");
            return DecoradeActionLinkWithImage(htmlHelper, linkText, imageSrc, imageAlt, imageHtmlAttributes, htmlHelper.ActionLink(TEMP_REPLACE_STRING, actionName, controllerName, protocol, hostName, fragment, routeValues, htmlAttributes));
        }

    }
}