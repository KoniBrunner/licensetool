﻿using LicenseTool.ClassLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace LicenseTool.WebApplication
{

    /// <summary>
    /// Class with some static helper functions
    /// </summary>
    public class Helpers
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Builds a select list for an enum type
        /// </summary>
        /// <param name="enumType">The enum type to build the list from</param>
        /// <returns>The select list from the enum type with all the values in it</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="enumType"/> is <c>null</c></exception>
        public static SelectList EnumAsSelectList(Type enumType)
        {
            if (log.IsDebugEnabled) log.Debug("EnumAsSelectList");

            if (enumType == null) throw new ArgumentNullException("enumType");
            List<SelectListItem> types = new List<SelectListItem>();
            foreach (int val in Enum.GetValues(enumType))
            {
                SelectListItem itm = new SelectListItem();
                itm.Text = EnumHelper.GetEnumValueName(enumType, (Enum)Enum.ToObject(enumType, val));
                itm.Value = val.ToString();
                types.Add(itm);
            }

            if (log.IsDebugEnabled) log.Debug("EnumAsSelectList Done");
            return new SelectList(types, "Value", "Text");
        }

        /// <summary>
        /// Gets the default search paging size
        /// </summary>
        /// <returns>The page size</returns>
        public static int GetPageSize()
        {
            if (log.IsDebugEnabled) log.Debug("GetPageSize");

            //TODO from session

            if (log.IsDebugEnabled) log.Debug("GetPageSize Done");
            return Properties.Settings.Default.DefaultPageSize;
        }

        /// <summary>
        /// Casts a dynamic object
        /// </summary>
        /// <param name="obj">The object to be casted</param>
        /// <param name="castTo">The type to cast to</param>
        /// <returns>The casted object as dynamic</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="obj"/> is <c>null</c></exception>
        /// <exception cref="ArgumentNullException">If <paramref name="castTo"/> is <c>null</c></exception>
        public static dynamic CastDynamic(dynamic obj, Type castTo)
        {
            if (log.IsDebugEnabled) log.Debug("CastDynamic");

            if (obj == null) throw new ArgumentNullException("obj");
            if (castTo == null) throw new ArgumentNullException("castTo");

            if (log.IsDebugEnabled) log.Debug("CastDynamic Done");
            return Convert.ChangeType(obj, castTo);
        }
    
    }
}