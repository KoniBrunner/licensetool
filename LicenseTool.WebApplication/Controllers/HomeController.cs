﻿using LicenseTool.ClassLibrary;
using LicenseTool.PluginsWeb;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace LicenseTool.WebApplication.Controllers
{

    /// <summary>
    /// Controller for the Home entity
    /// </summary>
    public class HomeController : BaseController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private ILicenseToolContext db = null;

        private Regex notAllowedFileExt = new Regex("(\\.exe|\\.bat|\\.com|\\.cmd|\\.dll)$", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        /// <summary>
        /// Constructor initializing the instance
        /// </summary>
        public HomeController()
        {
            if (log.IsDebugEnabled) log.Debug("HomeController");

            this.db = new LicenseToolDatabaseEntities();

            if (log.IsDebugEnabled) log.Debug("HomeController Done");
        }

        /// <summary>
        /// Constructor initializing the instance
        /// </summary>
        /// <param name="db">The databse context to be used</param>
        public HomeController(ILicenseToolContext db)
        {
            if (log.IsDebugEnabled) log.Debug("HomeController");

            this.db = db;

            if (log.IsDebugEnabled) log.Debug("HomeController Done");
        }

        /// <summary>
        /// Shows the home page
        /// </summary>
        /// <returns>The view result</returns>
        public ActionResult Index()
        {
            if (log.IsDebugEnabled) log.Debug("Index");

            if (log.IsDebugEnabled) log.Debug("Index Done");
            return View();
        }

        /// <summary>
        /// Shows the error page
        /// </summary>
        /// <param name="id">The http error number</param>
        /// <returns>The view result</returns>
        public ActionResult Error(string id)
        {
            if (log.IsDebugEnabled) log.Debug("Error");

            ViewBag.ErrorId = id;

            if (log.IsDebugEnabled) log.Debug("Error Done");
            return View();
        }

        /// <summary>
        /// Shows the about page
        /// </summary>
        /// <returns>The view result</returns>
        public ActionResult About()
        {
            if (log.IsDebugEnabled) log.Debug("About");

            if (log.IsDebugEnabled) log.Debug("About Done");
            return View();
        }

        /// <summary>
        /// Shows the contact page
        /// </summary>
        /// <returns>The view result</returns>
        public ActionResult Contact()
        {
            if (log.IsDebugEnabled) log.Debug("Contact");

            if (log.IsDebugEnabled) log.Debug("Contact Done");
            return View();
        }

        /// <summary>
        /// Shows the command line tool page
        /// </summary>
        /// <returns>The view result</returns>
        public ActionResult CommandLine()
        {
            if (log.IsDebugEnabled) log.Debug("CommandLine");

            if (log.IsDebugEnabled) log.Debug("CommandLine Done");
            return View();
        }

        /// <summary>
        /// Shows the windows service page
        /// </summary>
        /// <returns>The view result</returns>
        public ActionResult WindowsService()
        {
            if (log.IsDebugEnabled) log.Debug("WindowsService");

            if (log.IsDebugEnabled) log.Debug("WindowsService Done");
            return View();
        }

        /// <summary>
        /// Shows the data uplaod page
        /// </summary>
        /// <returns>The view result</returns>
        public ActionResult Upload()
        {
            if (log.IsDebugEnabled) log.Debug("Upload");

            bool wasUpload = false;
            bool isSavedSuccessfully = true;
            try
            {
                int cnt = Request.Files.Count;
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase upload = Request.Files[0];
                    wasUpload = true;
                    if (upload == null || upload.ContentLength == 0) continue;
                    string fileName = Path.GetFileName(upload.FileName);
                    string rootPath = Server.MapPath("~/");
                    string extractPath = Properties.Settings.Default.WatchLocation;
                    if (extractPath.StartsWith("~")) extractPath = rootPath + extractPath.Substring(1);
                    string path = Path.Combine(extractPath, fileName);
                    if (System.IO.File.Exists(path))
                        System.IO.File.Delete(path);
                    upload.SaveAs(path);

                    if (fileName.ToLower().EndsWith(".zip"))
                    {
                        using (ZipArchive archive = ZipFile.OpenRead(path))
                        {
                            foreach (ZipArchiveEntry entry in archive.Entries)
                            {
                                if (!notAllowedFileExt.IsMatch(entry.Name))
                                {
                                    string epath = Path.Combine(extractPath, entry.Name);
                                    if (System.IO.File.Exists(epath))
                                        System.IO.File.Delete(epath);
                                    entry.ExtractToFile(epath);
                                }
                            }
                        }
                        System.IO.File.Delete(path);
                    }

                }
            }
            catch (Exception)
            {
                isSavedSuccessfully = false;
            }
            if (!wasUpload)
            {
                if (log.IsDebugEnabled) log.Debug("Upload Done");
                return View();
            }
            else
            {
                if (log.IsDebugEnabled) log.Debug("Upload Done");
                if (!isSavedSuccessfully)
                {
                    return Json(new { Message = "Error in saving file" });
                }
                return null;
            }
        }

        /// <summary>
        /// Shows the upload proceed page
        /// </summary>
        /// <returns>The view result</returns>
        public ActionResult UploadProceed()
        {
            if (log.IsDebugEnabled) log.Debug("UploadProceed");

            string rootPath = Server.MapPath("~/");
            string todoPath = Properties.Settings.Default.WatchLocation;
            string donePath = Properties.Settings.Default.MoveToLocation;
            if (todoPath.StartsWith("~")) todoPath = rootPath + todoPath.Substring(2);
            if (donePath.StartsWith("~")) donePath = rootPath + donePath.Substring(2);

            DirectoryInfo todoDir = new DirectoryInfo(todoPath);
            DirectoryInfo doneDir = new DirectoryInfo(donePath);

            if (!todoDir.Exists) todoDir.Create();
            if (!doneDir.Exists) doneDir.Create();

            FileInfo[] preTodo = todoDir.GetFiles();
            FileInfo[] preDone = doneDir.GetFiles();

            Gather prg = new Gather();
            prg.GatherDirectory(new DirectoryInfo(todoPath), new DirectoryInfo(donePath));

            todoDir.Refresh();
            doneDir.Refresh();

            FileInfo[] postTodo = todoDir.GetFiles();
            FileInfo[] postDone = doneDir.GetFiles();

            ViewBag.ErrorItems = postTodo;
            ViewBag.ErrorItemsSelected = postTodo.Select(i => i.Name).ToArray();
            ViewBag.DoneItems = postDone;
            ViewBag.DoneItemsSelected = postDone.Select(i => i.Name).ToArray();

            if (log.IsDebugEnabled) log.Debug("UploadProceed Done");
            return View();
        }

        /// <summary>
        /// Post of upload proceed
        /// </summary>
        /// <param name="DeleteError">If button DeleteError is hit</param>
        /// <param name="DeleteDone">If button DeleteDone is hit</param>
        /// <param name="DeleteAll">If button DeleteAll is hit</param>
        /// <param name="ErrorItems">List of error items to be deleted</param>
        /// <param name="DoneItems">List of done itmes to be deleted</param>
        /// <returns>The view result</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadProceed(string DeleteError = null, string DeleteDone = null,
            string DeleteAll = null, List<string> ErrorItems = null, List<string> DoneItems = null)
        {
            if (log.IsDebugEnabled) log.Debug("UploadProceed");

            string rootPath = Server.MapPath("~/");
            string todoPath = Properties.Settings.Default.WatchLocation;
            string donePath = Properties.Settings.Default.MoveToLocation;
            if (todoPath.StartsWith("~")) todoPath = rootPath + todoPath.Substring(2);
            if (donePath.StartsWith("~")) donePath = rootPath + donePath.Substring(2);

            DirectoryInfo todoDir = new DirectoryInfo(todoPath);
            DirectoryInfo doneDir = new DirectoryInfo(donePath);

            if (DeleteError != null)
            {
                foreach (string fileName in ErrorItems)
                    System.IO.File.Delete(Path.Combine(todoPath, fileName));
            }
            if (DeleteDone != null)
            {
                foreach (string fileName in DoneItems)
                    System.IO.File.Delete(Path.Combine(donePath, fileName));
            }
            if (DeleteAll != null)
            {
                FileInfo[] todo = todoDir.GetFiles();
                FileInfo[] done = doneDir.GetFiles();

                foreach (FileInfo file in todo)
                    file.Delete();
                foreach (FileInfo file in done)
                    file.Delete();
            }

            todoDir.Refresh();
            doneDir.Refresh();

            FileInfo[] postTodo = todoDir.GetFiles();
            FileInfo[] postDone = doneDir.GetFiles();

            ViewBag.ErrorItems = postTodo;
            ViewBag.ErrorItemsSelected = postTodo.Select(i => i.Name).ToArray();
            ViewBag.DoneItems = postDone;
            ViewBag.DoneItemsSelected = postDone.Select(i => i.Name).ToArray();

            if (log.IsDebugEnabled) log.Debug("UploadProceed Done");
            return View();
        }

        /// <summary>
        /// Shows the download csv page
        /// </summary>
        /// <param name="entity">The entity type name to be downloaded</param>
        /// <returns>The file content result</returns>
        public ActionResult DownloadCsv(string entity = null)
        {
            if (log.IsDebugEnabled) log.Debug("UploadProceed");

            if (entity == null) return View();
            string ret = null;
            byte[] bytes = null;
            switch (entity)
            {
                case "License":
                    ret = CsvExporter.GetCsvData(db.License.ToList());
                    bytes = new byte[ret.Length * sizeof(char)];
                    Buffer.BlockCopy(ret.ToCharArray(), 0, bytes, 0, bytes.Length);
                    if (log.IsDebugEnabled) log.Debug("UploadProceed Done");
                    return File(bytes, "text/csv", "Licenses.csv");
                case "LicenseUsage":
                    ret = CsvExporter.GetCsvData(db.LicenseUsage.ToList());
                    bytes = new byte[ret.Length * sizeof(char)];
                    Buffer.BlockCopy(ret.ToCharArray(), 0, bytes, 0, bytes.Length);
                    if (log.IsDebugEnabled) log.Debug("UploadProceed Done");
                    return File(bytes, "text/csv", "LicenseUsage.csv");
                case "LicHost":
                    ret = CsvExporter.GetCsvData(db.LicHost.ToList());
                    bytes = new byte[ret.Length * sizeof(char)];
                    Buffer.BlockCopy(ret.ToCharArray(), 0, bytes, 0, bytes.Length);
                    if (log.IsDebugEnabled) log.Debug("UploadProceed Done");
                    return File(bytes, "text/csv", "LicHost.csv");
                case "LicUser":
                    ret = CsvExporter.GetCsvData(db.LicUser.ToList());
                    bytes = new byte[ret.Length * sizeof(char)];
                    Buffer.BlockCopy(ret.ToCharArray(), 0, bytes, 0, bytes.Length);
                    if (log.IsDebugEnabled) log.Debug("UploadProceed Done");
                    return File(bytes, "text/csv", "LicUser.csv");
                case "LicServer":
                    ret = CsvExporter.GetCsvData(db.LicServer.ToList());
                    bytes = new byte[ret.Length * sizeof(char)];
                    Buffer.BlockCopy(ret.ToCharArray(), 0, bytes, 0, bytes.Length);
                    if (log.IsDebugEnabled) log.Debug("UploadProceed Done");
                    return File(bytes, "text/csv", "LicServer.csv");
            }
            if (log.IsDebugEnabled) log.Debug("UploadProceed Done");
            return new HttpNotFoundResult();
        }

        /// <summary>
        /// Shows the plugin result page in html format
        /// </summary>
        /// <param name="pluginId">The id of the plugin</param>
        /// <returns>The view result</returns>
        public ActionResult PluginResultHtml(int pluginId)
        {
            if (log.IsDebugEnabled) log.Debug("PluginResultHtml");

            string plug = WebApplication.Properties.Settings.Default.Plugins[pluginId];
            IWebPlugin item = Activator.CreateInstance(Type.GetType(plug), new object[] { db }) as IWebPlugin;
            ViewBag.Title = item.Name;
            ViewBag.Description = item.Description;
            ViewBag.Plugin = item;

            if (log.IsDebugEnabled) log.Debug("PluginResultHtml Done");
            return View();
        }

        /// <summary>
        /// Shows the plugin result page in table format
        /// </summary>
        /// <param name="pluginId">The id of the plugin</param>
        /// <returns>The view result</returns>
        public ActionResult PluginResultTable(int pluginId)
        {
            if (log.IsDebugEnabled) log.Debug("PluginResultTable");

            string plug = WebApplication.Properties.Settings.Default.Plugins[pluginId];
            IWebPlugin item = Activator.CreateInstance(Type.GetType(plug), new object[] { db }) as IWebPlugin;
            ViewBag.Title = item.Name;
            ViewBag.Description = item.Description;
            ViewBag.Plugin = item;
            ViewBag.Result = item.BuildResultTable();

            if (log.IsDebugEnabled) log.Debug("PluginResultTable Done");
            return View();
        }

        /// <summary>
        /// Shows the plugin input page
        /// </summary>
        /// <returns>The view result</returns>
        public ActionResult PluginInput()
        {
            if (log.IsDebugEnabled) log.Debug("PluginInput");

            if (log.IsDebugEnabled) log.Debug("PluginInput Done");
            return View();
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (log.IsDebugEnabled) log.Debug("Dispose");

            db.Dispose();
            base.Dispose(disposing);

            if (log.IsDebugEnabled) log.Debug("Dispose Done");
        }
    }
}
