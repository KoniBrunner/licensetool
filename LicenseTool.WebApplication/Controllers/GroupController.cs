﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LicenseTool.ClassLibrary;
using LicenseTool.WebApplication.Models;

namespace LicenseTool.WebApplication.Controllers
{

    /// <summary>
    /// Controller for the Group entity
    /// </summary>
    public class GroupController : BaseController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private ILicenseToolContext db = null;

        /// <summary>
        /// Constructor initializing the instance
        /// </summary>
        public GroupController()
        {
            if (log.IsDebugEnabled) log.Debug("GroupController");

            this.db = new LicenseToolDatabaseEntities();

            if (log.IsDebugEnabled) log.Debug("GroupController Done");
        }

        /// <summary>
        /// Constructor initializing the instance
        /// </summary>
        /// <param name="db">The databse context to be used</param>
        public GroupController(ILicenseToolContext db)
        {
            if (log.IsDebugEnabled) log.Debug("GroupController");

            this.db = db;

            if (log.IsDebugEnabled) log.Debug("GroupController Done");
        }

        /// <summary>
        /// Shows the list of all entities and passes it to <see cref="Index(GroupSearchModel)"/>
        /// </summary>
        /// <returns>The view result</returns>
        public ActionResult Index()
        {
            if (log.IsDebugEnabled) log.Debug("Index");

            GroupSearchModel search = new GroupSearchModel();
            search.SortColumn = "Name";

            if (log.IsDebugEnabled) log.Debug("Index Done");
            return Index(search);
        }

        /// <summary>
        /// Shows the list of all entities
        /// </summary>
        /// <param name="search">The search model to be applied</param>
        /// <returns>The view result</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(GroupSearchModel search)
        {
            if (log.IsDebugEnabled) log.Debug("Index");

            IQueryable<Group> foundItems = db.Group;
            if (!string.IsNullOrEmpty(search.Name))
            {
                string[] tsts = search.Name.ToLower().Trim(new char[] { '*' }).Split(new char[] { '*' });
                foreach (string tst in tsts)
                    foundItems = foundItems.Where(l => l.Name.ToLower().Contains(tst));
            }
            if (!string.IsNullOrEmpty(search.Description))
            {
                string[] tsts = search.Description.ToLower().Trim(new char[] { '*' }).Split(new char[] { '*' });
                foreach (string tst in tsts)
                    foundItems = foundItems.Where(l => l.Description.ToLower().Contains(tst));
            }
            search.MaxItems = foundItems.Count();
            search.PageSize = Helpers.GetPageSize();
            foundItems = foundItems.Distinct();
            if (search.SortColumn == "Name")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(s => s.Name);
                else
                    foundItems = foundItems.OrderByDescending(s => s.Name);
            }
            if (search.SortColumn == "Description")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(s => s.Description);
                else
                    foundItems = foundItems.OrderByDescending(s => s.Description);
            }
            ViewBag.ShownItems = foundItems.Skip(search.PagingStartIndex * Helpers.GetPageSize())
                .Take(Helpers.GetPageSize()).ToList();

            if (log.IsDebugEnabled) log.Debug("Index Done");
            return View(search);
        }

        /// <summary>
        /// Shows the details of an entity
        /// </summary>
        /// <param name="id">The id of the entity to be shown</param>
        /// <returns>The view result if entity has been found</returns>
        /// <remarks>The method redirects to <see cref="HomeController.Error"/> if the entity with given id is not found</remarks>
        public ActionResult Details(int id = 0)
        {
            if (log.IsDebugEnabled) log.Debug("Details");

            Group group = db.Group.Find(id);
            if (group == null)
            {
                return RedirectToAction("Error", "Home", new { id = "404", aspxerrorpath = Request.Url.AbsolutePath });
            }

            if (log.IsDebugEnabled) log.Debug("Details Done");
            return View(group);
        }

        /// <summary>
        /// Shows the create new entity page
        /// </summary>
        /// <returns>The view result</returns>
        public ActionResult Create()
        {
            if (log.IsDebugEnabled) log.Debug("Create");

            if (log.IsDebugEnabled) log.Debug("Create Done");
            return View();
        }

        /// <summary>
        /// Post to create the new entity
        /// </summary>
        /// <param name="group">The entity to be created and stored in database</param>
        /// <returns>The view result if anything is wrong</returns>
        /// <remarks>Redirects to <see cref="Index()"/> if creation was succesfull</remarks>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Group group)
        {
            if (log.IsDebugEnabled) log.Debug("Create");

            if (ModelState.IsValid)
            {
                db.Group.Add(group);
                db.SaveChanges();

                if (log.IsDebugEnabled) log.Debug("Create RedirectToAction Done");
                return RedirectToAction("Index");
            }

            if (log.IsDebugEnabled) log.Debug("Create Done");
            return View(group);
        }

        /// <summary>
        /// Shows the edit page for an entity
        /// </summary>
        /// <param name="id">The id of the entity to be edited</param>
        /// <returns>The view result if entity has been found</returns>
        /// <remarks>The method redirects to <see cref="HomeController.Error"/> if the entity with given id is not found</remarks>
        public ActionResult Edit(int id = 0)
        {
            if (log.IsDebugEnabled) log.Debug("Edit");
            var groups = db.Group.Include(l => l.NavRelGroupUser);

            Group group = groups.FirstOrDefault(g=>g.Id==id);
            if (group == null)
            {
                if (log.IsDebugEnabled) log.Debug("Edit HttpNotFound Done");
                return RedirectToAction("Error", "Home", new { id = "404", aspxerrorpath = Request.Url.AbsolutePath });
            }

            List<LicUser> users = new List<LicUser>(db.LicUser.OrderBy(s => s.Name));
            foreach (RelGroupUser usr in group.NavRelGroupUser)
            {
                if (users.Contains(usr.NavLicUser))
                    users.Remove(usr.NavLicUser);
            }

            MultiSelectList lst = new MultiSelectList(users.OrderBy(u => u.DisplayName), "Id", "DisplayName");
            ViewBag.AllLicUsers = lst;

            List<LicUser> usersNav = group.NavRelGroupUser.Select(r=>r.NavLicUser).ToList();
            ViewBag.AllRelGroupUser = new MultiSelectList(usersNav.OrderBy(u => u.DisplayName), "Id", "DisplayName");

            if (log.IsDebugEnabled) log.Debug("Edit Done");
            return View(group);
        }

        /// <summary>
        /// Post to edit the new entity
        /// </summary>
        /// <param name="group">The entity to be changed and stored in database</param>
        /// <param name="SelectedGroupUser">The users in this group</param>
        /// <returns>The view result if anything is wrong</returns>
        /// <remarks>Redirects to <see cref="Index()"/> if change was succesfull</remarks>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Group group, List<int> SelectedGroupUser = null)
        {
            if (log.IsDebugEnabled) log.Debug("Edit");

            if (ModelState.IsValid)
            {
                IQueryable<RelGroupUser> act = db.RelGroupUser.Where(r => r.GroupId == group.Id);
                if (SelectedGroupUser == null)
                    SelectedGroupUser = new List<int>();

                //remove all which are no longer selected
                act.Where(r => !SelectedGroupUser.Contains(r.UserId)).ToList().ForEach(r => db.RelGroupUser.Remove(r));
                List<int> actUsers = act.Select(r => r.UserId).ToList();
                //remove all selected which are already persistet
                SelectedGroupUser.Where(u => actUsers.Contains(u)).ToList().ForEach(u => SelectedGroupUser.Remove(u));
                //add all remaining to the db
                SelectedGroupUser.ForEach(u => db.RelGroupUser.Add(new RelGroupUser() { UserId = u, GroupId = group.Id }));

                db.SetModified(group);
                db.SaveChanges();

                if (log.IsDebugEnabled) log.Debug("Edit RedirectToAction Done");
                return RedirectToAction("Index");
            }

            if (log.IsDebugEnabled) log.Debug("Edit Done");
            return View(group);
        }

        /// <summary>
        /// Shows the delete page for an entity
        /// </summary>
        /// <param name="id">The id of the entity to be deleted</param>
        /// <returns>The view result if entity has been found</returns>
        /// <remarks>The method redirects to <see cref="HomeController.Error"/> if the entity with given id is not found</remarks>
        public ActionResult Delete(int id = 0)
        {
            if (log.IsDebugEnabled) log.Debug("Delete");

            Group group = db.Group.Find(id);
            if (group == null)
            {
                if (log.IsDebugEnabled) log.Debug("Delete HttpNotFound Done");
                return RedirectToAction("Error", "Home", new { id = "404", aspxerrorpath = Request.Url.AbsolutePath });
            }

            if (log.IsDebugEnabled) log.Debug("Delete Done");
            return View(group);
        }

        /// <summary>
        /// Post to delete the specified entity
        /// </summary>
        /// <param name="id">The id of the entity to be deleted</param>
        /// <returns>The redirect action to <see cref="Index()"/></returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (log.IsDebugEnabled) log.Debug("DeleteConfirmed");

            Group group = db.Group.Find(id);
            db.Group.Remove(group);
            db.SaveChanges();

            if (log.IsDebugEnabled) log.Debug("DeleteConfirmed Done");
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (log.IsDebugEnabled) log.Debug("Dispose");

            db.Dispose();
            base.Dispose(disposing);

            if (log.IsDebugEnabled) log.Debug("Dispose Done");
        }
    }
}