﻿using LicenseTool.ClassLibrary;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using LicenseTool.WebApplication.Models;
using LicenseTool.WebApplication.Views.Report;
using eres = LicenseTool.ResourceLibrary.Enum;
using System.Drawing;
using System.Drawing.Imaging;

namespace LicenseTool.WebApplication.Controllers
{

    /// <summary>
    /// The report format
    /// </summary>
    public enum ReportFormat
    {
        /// <summary>
        /// File type PDF
        /// </summary>
        [Display(Name = "PDF", ResourceType = typeof(eres))]
        PDF = 1,

        /// <summary>
        /// File type EXCEL
        /// </summary>
        [Display(Name = "EXCEL", ResourceType = typeof(eres))]
        Excel = 2,

        /// <summary>
        /// File type WORD
        /// </summary>
        [Display(Name = "WORD", ResourceType = typeof(eres))]
        Word = 3,

        /// <summary>
        /// File type IMAGE (TIF)
        /// </summary>
        [Display(Name = "IMAGE", ResourceType = typeof(eres))]
        Image = 4
    }

    /// <summary>
    /// The report time frame
    /// </summary>
    public enum ReportFrame
    {

        /// <summary>
        /// Reort time frame 1 year
        /// </summary>
        [Display(Name = "YEAR", ResourceType = typeof(eres))]
        Year = 1,

        /// <summary>
        /// Reort time frame 1 month
        /// </summary>
        [Display(Name = "MONTH", ResourceType = typeof(eres))]
        Month = 2,

        /// <summary>
        /// Reort time frame 1 day
        /// </summary>
        [Display(Name = "DAY", ResourceType = typeof(eres))]
        Day = 3
    }

    //with help from: http://dotnetawesome.blogspot.ch/2013/09/microsoft-report-in-mvc-4.html
    /// <summary>
    /// Controller for reports
    /// </summary>
    public class ReportController : BaseController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private ILicenseToolContext db = null;
        private Regex reg = new Regex("[a-zA-Z0-9]*", RegexOptions.Compiled);
        private string[] enMonths = DateTimeFormatInfo.CurrentInfo.MonthNames;

        /// <summary>
        /// Constructor initializing the instance
        /// </summary>
        public ReportController()
        {
            if (log.IsDebugEnabled) log.Debug("ReportController");

            this.db = new LicenseToolDatabaseEntities();

            if (log.IsDebugEnabled) log.Debug("ReportController Done");
        }

        /// <summary>
        /// Constructor initializing the instance
        /// </summary>
        /// <param name="db">The databse context to be used</param>
        public ReportController(ILicenseToolContext db)
        {
            if (log.IsDebugEnabled) log.Debug("ReportController");

            this.db = db;

            if (log.IsDebugEnabled) log.Debug("ReportController Done");
        }

        /// <summary>
        /// Starting page for used licenses
        /// </summary>
        /// <retutrns>The view result</retutrns>
        public ActionResult UsedLicenses()
        {
            if (log.IsDebugEnabled) log.Debug("UsedLicenses");

            ReportSelectionModel model = new ReportSelectionModel();
            model.BackAction = "UsedLicenses";
            model.PaperFormat = "A4H";

            if (log.IsDebugEnabled) log.Debug("UsedLicenses Done");
            return UsedLicenses(model);
        }

        /// <summary>
        /// Starting page for used licenses sheet
        /// </summary>
        /// <retutrns>The view result</retutrns>
        public ActionResult UsedLicensesSheet()
        {
            if (log.IsDebugEnabled) log.Debug("UsedLicensesSheet");

            ReportSelectionModel model = new ReportSelectionModel();
            model.BackAction = "UsedLicensesSheet";
            model.PaperFormat = "A4V";

            if (log.IsDebugEnabled) log.Debug("UsedLicensesSheet Done");
            return UsedLicenses(model);
        }

        /// <summary>
        /// Second page for used licenses sheet
        /// </summary>
        /// <param name="model">The report selections</param>
        /// <param name="preview">Not null if in preview mode</param>
        /// <param name="downloadPDF">Not null if PDF has to be downloaded</param>
        /// <param name="downloadExcel">Not null if Excel has to be downloaded</param>
        /// <param name="downloadWord">Not null if Word has to be downloaded</param>
        /// <param name="downloadImage">Not null if Image has to be downloaded</param>
        /// <retutrns>The view result</retutrns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UsedLicensesSheet(ReportSelectionModel model, string preview = null,
            string downloadPDF = null, string downloadExcel = null, string downloadWord = null, string downloadImage = null)
        {
            if (log.IsDebugEnabled) log.Debug("UsedLicensesSheet");

            if (log.IsDebugEnabled) log.Debug("UsedLicensesSheet Done");
            return UsedLicenses(model, preview, downloadPDF, downloadExcel, downloadWord, downloadImage);
        }

        /// <summary>
        /// Second page for used licenses
        /// </summary>
        /// <param name="model">The report selections</param>
        /// <param name="preview">Not null if in preview mode</param>
        /// <param name="downloadPDF">Not null if PDF has to be downloaded</param>
        /// <param name="downloadExcel">Not null if Excel has to be downloaded</param>
        /// <param name="downloadWord">Not null if Word has to be downloaded</param>
        /// <param name="downloadImage">Not null if Image has to be downloaded</param>
        /// <retutrns>The view or file content result based on options</retutrns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UsedLicenses(ReportSelectionModel model, string preview = null,
            string downloadPDF = null, string downloadExcel = null, string downloadWord = null, string downloadImage = null)
        {
            if (log.IsDebugEnabled) log.Debug("UsedLicenses");

            ViewBag.Preview = false;

            if (model.BackAction == "UsedLicensesSheet")
                ViewBag.HeadIcon = "sheet.png";
            else
                ViewBag.HeadIcon = "report.png";

            int stYear = Properties.Settings.Default.ReportingStartYear;
            int ndYear = DateTime.Now.Year;
            List<SelectListItem> years = new List<SelectListItem>();
            for (int y = stYear; y <= ndYear; y++ )
            {
                SelectListItem itm = new SelectListItem();
                itm.Text = y.ToString();
                itm.Value = y.ToString();
                years.Add(itm);
            }
            ViewBag.Years = new SelectList(years, "Value", "Text", ndYear.ToString());

            List<SelectListItem> months = new List<SelectListItem>();
            for (int m = 1; m <= 12; m++)
            {
                SelectListItem itm = new SelectListItem();
                itm.Text = enMonths[m-1];
                itm.Value = m.ToString();
                months.Add(itm);
            }
            ViewBag.Months = new SelectList(months, "Value", "Text", DateTime.Now.Month.ToString());

            ViewBag.ReportFormats = Helpers.EnumAsSelectList(typeof(ReportFormat));
            ViewBag.ReportFrames = Helpers.EnumAsSelectList(typeof(ReportFrame));
            List<LicServer> servers = new List<LicServer>(db.LicServer.OrderBy(s => s.Name));
            if (model.LicServer > 0)
            { 
                SelectList lst = new SelectList(servers, "Id", "Name", model.LicServer);
                ViewBag.AllLicServers = lst;
                List<License> licenses = new List<License>(db.License.Where(l => l.LicServer == model.LicServer).OrderBy(s => s.FeatureName));
                ViewBag.AllLicenses = new SelectList(licenses, "Id", "FeatureName");
            }
            else
            {
                SelectList lst = new SelectList(servers, "Id", "Name");
                ViewBag.AllLicServers = lst;
                List<License> licenses = new List<License>();
                ViewBag.AllLicenses = new SelectList(licenses, "Id", "FeatureName");
            }
            ViewBag.AllGroups = new SelectList(db.Group.ToList(), "Id", "Name");
            if (preview == null && downloadPDF == null && downloadExcel == null && downloadWord == null && downloadImage == null)
            {
                if (model.ReportFrame > 0)
                {
                    switch (model.ReportFrame)
                    {
                        case ReportFrame.Month:
                            if (model.Month == 0) model.Month = (short)DateTime.Now.Month;
                            if (model.Year == 0) model.Year = DateTime.Now.Year;
                            break;
                        case ReportFrame.Year:
                            if (model.Year == 0) model.Year = DateTime.Now.Year;
                            break;
                        case ReportFrame.Day:
                            model.ReportStartDate = DateTime.Now;
                            break;
                    }
                }
            }
            if (preview != null)
            {
                switch (model.ReportFrame)
                {
                    case ReportFrame.Month:
                        model.ReportStartDate = new DateTime(model.Year, model.Month, 1);
                        break;
                    case ReportFrame.Year:
                        model.ReportStartDate = new DateTime(model.Year, 1, 1);
                        model.Month = 1;
                        break;
                    case ReportFrame.Day:
                        model.Month = (short)model.ReportStartDate.Month;
                        model.Year = model.ReportStartDate.Year;
                        break;
                }
                model.Preview = true;
                model.ReportFormat = ReportFormat.Image;
                model.ReportName = model.BackAction + model.ReportFrame.ToString();
            }
            if (downloadPDF != null)
            {
                model.Preview = false;
                model.ReportFormat = ReportFormat.PDF;
                if (log.IsDebugEnabled) log.Debug("UsedLicenses PDF Done");
                return Report(model);
            }
            if (downloadExcel != null)
            {
                model.Preview = false;
                model.ReportFormat = ReportFormat.Excel;
                if (log.IsDebugEnabled) log.Debug("UsedLicenses Excel Done");
                return Report(model);
            }
            if (downloadWord != null)
            {
                model.Preview = false;
                model.ReportFormat = ReportFormat.Word;
                if (log.IsDebugEnabled) log.Debug("UsedLicenses Word Done");
                return Report(model);
            }
            if (downloadImage != null)
            {
                model.Preview = false;
                model.ReportFormat = ReportFormat.Image;
                if (log.IsDebugEnabled) log.Debug("UsedLicenses Image Done");
                return Report(model);
            }

            if (log.IsDebugEnabled) log.Debug("UsedLicenses Done");
            return View("UsedLicenses", model);
        }

        /// <summary>
        /// Exports a report
        /// </summary>
        /// <param name="model">The report selections</param>
        /// <retutrns>The view result</retutrns>
        public ActionResult Report(ReportSelectionModel model)
        {
            if (log.IsDebugEnabled) log.Debug("Report");

            LocalReport lr = new LocalReport();

            if (!reg.IsMatch(model.ReportName))
                return new HttpNotFoundResult();

            string path = Path.Combine(Server.MapPath("~/Views/Report"), model.ReportName + "_" + Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName.ToLower() + ".rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                switch (model.BackAction)
                {
                    case "UsedLicenses":
                        if (log.IsDebugEnabled) log.Debug("Report UsedLicenses Done");
                        return UsedLicenses(model);
                    case "UsedLicensesSheet":
                        if (log.IsDebugEnabled) log.Debug("Report UsedLicenses Done");
                        return UsedLicenses(model);
                }
            }

            string license = db.LicServer.Find(new object[] { model.LicServer }).Name;
            string feature = db.License.Find(new object[] { model.License }).FeatureName;
            int? maxLic = db.License.Find(new object[] { model.License }).NumLicenses;
            string group = "";
            if (model.ReportGroup!=null)
                group = db.Group.Find(new object[] { model.ReportGroup }).Name;
            ReportParameter rp1 = new ReportParameter("Month", enMonths[model.Month - 1]);
            ReportParameter rp2 = new ReportParameter("Year", model.Year.ToString());
            ReportParameter rp3 = new ReportParameter("Date", model.ReportStartDate.ToShortDateString());
            ReportParameter rp4 = new ReportParameter("License", license);
            ReportParameter rp5 = new ReportParameter("Feature", feature);
            ReportParameter rp6 = new ReportParameter("Group", group);

            if (maxLic.HasValue)
            {
                ReportParameter rp7 = new ReportParameter("MaxLicenses", maxLic.Value.ToString());
                lr.SetParameters(new ReportParameter[] { rp1, rp2, rp3, rp4, rp5, rp6, rp7 });
            }
            else
            {
                lr.SetParameters(new ReportParameter[] { rp1, rp2, rp3, rp4, rp5, rp6 });
            }

            IQueryable<UsageReportView> usages = null;
            ReportDataSource rd = null;
            IQueryable<int> usersInGroup = db.LicUser.Select(u => u.Id);
            if (model.ReportGroup!=null && model.ReportGroup.Value > 0)
                usersInGroup = db.RelGroupUser.Where(r => r.GroupId == model.ReportGroup.Value).Select(r => r.UserId);

            IQueryable<int> allFeatures = db.License.Select(l => l.Id);
            if (model.ReportGroup != null && model.ReportGroup.Value > 0)
                allFeatures = db.License.Where(l => l.NavLicenseUsage.Any(
                u => usersInGroup.Contains(u.LicUser))).Select(l => l.Id);

            switch (model.BackAction)
            {
                case "UsedLicenses":
                    switch (model.ReportFrame)
                    {
                        case ReportFrame.Day:
                            usages = db.UsageReportView.Where(u => allFeatures.Contains(u.FeatureId) &&
                                u.FeatureId == model.License && u.Hour > -1 && u.Minute == -1 && u.Year == model.Year &&
                                u.Month == model.Month && u.Day == model.ReportStartDate.Day);
                            break;
                        case ReportFrame.Month:
                            usages = db.UsageReportView.Where(u => allFeatures.Contains(u.FeatureId) &&
                                u.FeatureId == model.License && u.Hour == -1 && u.Day > -1 && u.Year == model.Year &&
                                u.Month == model.Month);
                            break;
                        case ReportFrame.Year:
                            usages = db.UsageReportView.Where(u => allFeatures.Contains(u.FeatureId) &&
                                u.FeatureId == model.License && u.Day == -1 && u.Year == model.Year);
                            break;
                    }
                    break;
                case "UsedLicensesSheet":
                    switch (model.ReportFrame)
                    {
                        case ReportFrame.Day:
                            usages = db.UsageReportView.Where(u => allFeatures.Contains(u.FeatureId) &&
                                u.FeatureId == model.License && u.Minute > -1 && u.Year == model.Year &&
                                u.Month == model.Month && u.Day == model.ReportStartDate.Day);
                            break;
                        case ReportFrame.Month:
                            usages = db.UsageReportView.Where(u => allFeatures.Contains(u.FeatureId) &&
                                u.FeatureId == model.License && u.Hour > -1 && u.Minute == -1 && u.Day > -1 && u.Year == model.Year &&
                                u.Month == model.Month);
                            break;
                        case ReportFrame.Year:
                            usages = db.UsageReportView.Where(u => allFeatures.Contains(u.FeatureId) &&
                                u.FeatureId == model.License && u.Day > -1 && u.Hour == -1 && u.Year == model.Year);
                            break;
                    }
                    break;
            }

            //TODO check if filter by group is correct working

            rd = new ReportDataSource("UsageReportDataSet",
                usages.OrderBy(u => u.Year).ThenBy(u => u.Month).ThenBy(u => u.Day).ThenBy(u => u.Hour).ThenBy(u => u.Minute).ToList());
            lr.DataSources.Add(rd);

            string mimeType;
            string encoding;
            string fileNameExtension;
            string deviceInfo = "<DeviceInfo>" +
                "  <OutputFormat>" + model.ReportFormat.ToString() + "</OutputFormat>";

            switch (model.PaperFormat)
            {
                case "A4V":
                    deviceInfo +=
                        "  <PageWidth>210mm</PageWidth>" +
                        "  <PageHeight>297mm</PageHeight>";
                    break;
                default:
                    deviceInfo +=
                        "  <PageWidth>297mm</PageWidth>" +
                        "  <PageHeight>210mm</PageHeight>";
                    break;
            }
            deviceInfo += "  <MarginTop>10mm</MarginTop>" +
                "  <MarginLeft>10mm</MarginLeft>" +
                "  <MarginRight>10mm</MarginRight>" +
                "  <MarginBottom>10mm</MarginBottom>" +
                "</DeviceInfo>";
            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                model.ReportFormat.ToString(),
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);

            if (model.Preview)
            {
                using (MemoryStream jpg = new MemoryStream())
                {
                    using (MemoryStream ms = new MemoryStream(renderedBytes))
                    {
                        using (Bitmap bmp = new Bitmap(ms))
                        {
                            bmp.Save(jpg, ImageFormat.Jpeg);
                        }
                    }
                    if (log.IsDebugEnabled) log.Debug("Report Preview Done");
                    return File(jpg.GetBuffer(), "image/jpeg",
                        model.ReportName + "_" + model.Year + "_" + model.Month + "_" + license + "_" + feature + "." + fileNameExtension);
                }
            }
            else
            {
                if (log.IsDebugEnabled) log.Debug("Report Done");
                return File(renderedBytes, mimeType,
                    model.ReportName + "_" + model.Year + "_" + model.Month + "_" + license + "_" + feature + "." + fileNameExtension);
            }
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (log.IsDebugEnabled) log.Debug("Dispose");

            db.Dispose();
            base.Dispose(disposing);

            if (log.IsDebugEnabled) log.Debug("Dispose Done");
        }
    }
}
