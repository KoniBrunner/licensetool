﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LicenseTool.ClassLibrary;
using LicenseTool.WebApplication.Models;

namespace LicenseTool.WebApplication.Controllers
{

    /// <summary>
    /// Controller for the LicUser entity
    /// </summary>
    public class LicUserController : BaseController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private ILicenseToolContext db = null;

        /// <summary>
        /// Constructor initializing the instance
        /// </summary>
        public LicUserController()
        {
            if (log.IsDebugEnabled) log.Debug("LicUserController");

            this.db = new LicenseToolDatabaseEntities();

            if (log.IsDebugEnabled) log.Debug("LicUserController Done");
        }

        /// <summary>
        /// Constructor initializing the instance
        /// </summary>
        /// <param name="db">The databse context to be used</param>
        public LicUserController(ILicenseToolContext db)
        {
            if (log.IsDebugEnabled) log.Debug("LicUserController");

            this.db = db;

            if (log.IsDebugEnabled) log.Debug("LicUserController Done");
        }

        /// <summary>
        /// Shows the list of all entities. Constructs a search entity and passes it to <see cref="Index(LicUserSearchModel)"/>
        /// </summary>
        /// <param name="licenseId">The id of the License the index has to be filtered</param>
        /// <param name="serverId">The id of the LicServer the index has to be filtered</param>
        /// <param name="userId">The id of the LicUser the index has to be filtered</param>
        /// <param name="hostId">The id of the LicHost the index has to be filtered</param>
        /// <returns>The view result</returns>
        public ActionResult Index(int licenseId = 0, int serverId = 0, int userId = 0, int hostId = 0)
        {
            if (log.IsDebugEnabled) log.Debug("Index");

            LicUserSearchModel search = new LicUserSearchModel();
            search.SelectedLicense = licenseId;
            search.SelectedLicServer = serverId;
            search.SelectedLicHost = hostId;
            search.SelectedLicUser = userId;
            search.SortColumn = "Name";
            if (licenseId > 0)
            {
                License lic = db.License.FirstOrDefault(l => l.Id == licenseId);
                if (lic != null) search.SelectedLicServer = lic.LicServer;
            }

            if (log.IsDebugEnabled) log.Debug("Index Done");
            return Index(search);
        }

        /// <summary>
        /// Shows the list of all entities
        /// </summary>
        /// <param name="search">The search model to be applied</param>
        /// <returns>The view result</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(LicUserSearchModel search)
        {
            if (log.IsDebugEnabled) log.Debug("Index");

            List<LicServer> servers = new List<LicServer>(db.LicServer.OrderBy(s => s.Name));
            IQueryable<LicUser> foundItems = null;
            if (search.SelectedLicServer == 0)
            {
                SelectList lst = new SelectList(servers, "Id", "Name");
                ViewBag.AllLicServers = lst;
                List<License> licenses = new List<License>();
                ViewBag.AllLicenses = new SelectList(licenses, "Id", "FeatureName");
                foundItems = db.LicUser;
            }
            else
            {
                SelectList lst = new SelectList(servers, "Id", "Name", search.SelectedLicServer);
                ViewBag.AllLicServers = lst;
                List<License> licenses = new List<License>(db.License.Where(l => l.LicServer == search.SelectedLicServer).OrderBy(s => s.FeatureName));
                if (search.SelectedLicense == 0)
                {
                    ViewBag.AllLicenses = new SelectList(licenses, "Id", "FeatureName");
                    foundItems = db.LicUser.Where(h => h.NavLicenseUsage.Any(u => u.NavLicense.NavLicServer.Id == search.SelectedLicServer));
                }
                else
                {
                    ViewBag.AllLicenses = new SelectList(licenses, "Id", "FeatureName", search.SelectedLicense);
                    foundItems = db.LicUser.Where(h => h.NavLicenseUsage.Any(u => u.NavLicense.Id == search.SelectedLicense));
                }
            }
            if (search.SelectedLicHost>0)
            {
                foundItems = foundItems.Where(l => l.NavLicenseUsage.Any(u => u.LicHost == search.SelectedLicHost && u.LicUser==l.Id));
            }
            if (search.SelectedLicUser > 0)
            {
                foundItems = foundItems.Where(h => h.Id == search.SelectedLicUser);
            }
            if (!string.IsNullOrEmpty(search.Name))
            {
                string[] tsts = search.Name.ToLower().Trim(new char[] { '*' }).Split(new char[] { '*' });
                foreach (string tst in tsts)
                    foundItems = foundItems.Where(l => l.Name.ToLower().Contains(tst));
            }
            if (!string.IsNullOrEmpty(search.Email))
            {
                string[] tsts = search.Email.ToLower().Trim(new char[] { '*' }).Split(new char[] { '*' });
                foreach (string tst in tsts)
                    foundItems = foundItems.Where(l => l.Email.ToLower().Contains(tst));
            }
            if (!string.IsNullOrEmpty(search.FullName))
            {
                string[] tsts = search.FullName.ToLower().Trim(new char[] { '*' }).Split(new char[] { '*' });
                foreach (string tst in tsts)
                    foundItems = foundItems.Where(l => l.FullName.ToLower().Contains(tst));
            }
            if (!string.IsNullOrEmpty(search.PhoneNumber))
            {
                string[] tsts = search.PhoneNumber.ToLower().Trim(new char[] { '*' }).Split(new char[] { '*' });
                foreach (string tst in tsts)
                    foundItems = foundItems.Where(l => l.PhoneNumber.ToLower().Contains(tst));
            }
            search.MaxItems = foundItems.Count();
            search.PageSize = Helpers.GetPageSize();
            foundItems = foundItems.Distinct();
            if (search.SortColumn == "Name")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(s => s.Name);
                else
                    foundItems = foundItems.OrderByDescending(s => s.Name);
            }
            if (search.SortColumn == "FullName")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(s => s.FullName);
                else
                    foundItems = foundItems.OrderByDescending(s => s.FullName);
            }
            if (search.SortColumn == "PhoneNumber")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(s => s.PhoneNumber);
                else
                    foundItems = foundItems.OrderByDescending(s => s.PhoneNumber);
            }
            if (search.SortColumn == "Email")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(s => s.Email);
                else
                    foundItems = foundItems.OrderByDescending(s => s.Email);
            }
            ViewBag.ShownItems = foundItems.Skip(search.PagingStartIndex * Helpers.GetPageSize())
                .Take(Helpers.GetPageSize()).ToList();

            if (log.IsDebugEnabled) log.Debug("Index Done");
            return View(search);
        }

        /// <summary>
        /// Shows the details of an entity
        /// </summary>
        /// <param name="id">The id of the entity to be shown</param>
        /// <returns>The view result if entity has been found</returns>
        /// <remarks>The method redirects to <see cref="HomeController.Error"/> if the entity with given id is not found</remarks>
        public ActionResult Details(int id = 0)
        {
            if (log.IsDebugEnabled) log.Debug("Details");

            LicUser licuser = db.LicUser.Find(id);
            if (licuser == null)
            {
                if (log.IsDebugEnabled) log.Debug("Details HttpNotFound Done");
                return RedirectToAction("Error", "Home", new { id = "404", aspxerrorpath = Request.Url.AbsolutePath });
            }

            if (log.IsDebugEnabled) log.Debug("Details Done");
            return View(licuser);
        }

        /// <summary>
        /// Shows the create new entity page
        /// </summary>
        /// <returns>The view result</returns>
        public ActionResult Create()
        {
            if (log.IsDebugEnabled) log.Debug("Create");


            if (log.IsDebugEnabled) log.Debug("Create Done");
            return View();
        }

        /// <summary>
        /// Post to create the new entity
        /// </summary>
        /// <param name="licuser">The entity to be created and stored in database</param>
        /// <returns>The view result if anything is wrong</returns>
        /// <remarks>Redirects to <see cref="Index(int,int,int,int)"/> if creation was succesfull</remarks>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LicUser licuser)
        {
            if (log.IsDebugEnabled) log.Debug("Create");

            if (ModelState.IsValid)
            {
                db.LicUser.Add(licuser);
                db.SaveChanges();

                if (log.IsDebugEnabled) log.Debug("Create RedirectToAction Done");
                return RedirectToAction("Index");
            }

            if (log.IsDebugEnabled) log.Debug("Create Done");
            return View(licuser);
        }

        /// <summary>
        /// Shows the edit page for an entity
        /// </summary>
        /// <param name="id">The id of the entity to be edited</param>
        /// <returns>The view result if entity has been found</returns>
        /// <remarks>The method redirects to <see cref="HomeController.Error"/> if the entity with given id is not found</remarks>
        public ActionResult Edit(int id = 0)
        {
            if (log.IsDebugEnabled) log.Debug("Edit");

            LicUser licuser = db.LicUser.Find(id);
            if (licuser == null)
            {
                if (log.IsDebugEnabled) log.Debug("Edit HttpNotFound Done");
                return RedirectToAction("Error", "Home", new { id = "404", aspxerrorpath = Request.Url.AbsolutePath });
            }

            if (log.IsDebugEnabled) log.Debug("Edit Done");
            return View(licuser);
        }

        /// <summary>
        /// Post to edit the new entity
        /// </summary>
        /// <param name="licuser">The entity to be changed and stored in database</param>
        /// <returns>The view result if anything is wrong</returns>
        /// <remarks>Redirects to <see cref="Index(int,int,int,int)"/> if change was succesfull</remarks>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LicUser licuser)
        {
            if (log.IsDebugEnabled) log.Debug("Edit");

            if (ModelState.IsValid)
            {
                db.SetModified(licuser);
                db.SaveChanges();

                if (log.IsDebugEnabled) log.Debug("Edit RedirectToAction Done");
                return RedirectToAction("Index");
            }

            if (log.IsDebugEnabled) log.Debug("Edit Done");
            return View(licuser);
        }

        /// <summary>
        /// Shows the delete page for an entity
        /// </summary>
        /// <param name="id">The id of the entity to be deleted</param>
        /// <returns>The view result if entity has been found</returns>
        /// <remarks>The method redirects to <see cref="HomeController.Error"/> if the entity with given id is not found</remarks>
        public ActionResult Delete(int id = 0)
        {
            if (log.IsDebugEnabled) log.Debug("Delete");

            LicUser licuser = db.LicUser.Find(id);
            if (licuser == null)
            {
                if (log.IsDebugEnabled) log.Debug("Delete HttpNotFound Done");
                return RedirectToAction("Error", "Home", new { id = "404", aspxerrorpath = Request.Url.AbsolutePath });
            }

            if (log.IsDebugEnabled) log.Debug("Delete Done");
            return View(licuser);
        }

        /// <summary>
        /// Post to delete the specified entity
        /// </summary>
        /// <param name="id">The id of the entity to be deleted</param>
        /// <returns>The redirect action to <see cref="Index(int,int,int,int)"/></returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (log.IsDebugEnabled) log.Debug("DeleteConfirmed");

            LicUser licuser = db.LicUser.Find(id);
            db.LicUser.Remove(licuser);
            db.SaveChanges();

            if (log.IsDebugEnabled) log.Debug("DeleteConfirmed Done");
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (log.IsDebugEnabled) log.Debug("Dispose");

            db.Dispose();
            base.Dispose(disposing);

            if (log.IsDebugEnabled) log.Debug("Dispose Done");
        }
    }
}