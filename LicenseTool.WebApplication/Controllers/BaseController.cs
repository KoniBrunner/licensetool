﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LicenseTool.WebApplication.Controllers
{
    /// <summary>
    /// Base controller for other controllers
    /// </summary>
    public class BaseController : Controller
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// OnException hook for excpetion handling
        /// </summary>
        /// <param name="filterContext">The actual filter context</param>
        protected override void OnException(ExceptionContext filterContext)
        {
            if (log.IsDebugEnabled) log.Debug("OnException");

            this.Session["ErrorException"] = filterContext.Exception;
            log.Error(filterContext.Exception);
            filterContext.ExceptionHandled = true;
            filterContext.Result = this.RedirectToAction("Error", "Home", new { id = "500"});
            base.OnException(filterContext);

            if (log.IsDebugEnabled) log.Debug("OnException Done");
        }

    }
}
