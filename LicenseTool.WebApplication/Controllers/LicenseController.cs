﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LicenseTool.ClassLibrary;
using LicenseTool.WebApplication.Models;

namespace LicenseTool.WebApplication.Controllers
{

    /// <summary>
    /// Controller for the License entity
    /// </summary>
    public class LicenseController : BaseController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private ILicenseToolContext db = null;

        /// <summary>
        /// Constructor initializing the instance
        /// </summary>
        public LicenseController()
        {
            if (log.IsDebugEnabled) log.Debug("LicenseController");

            this.db = new LicenseToolDatabaseEntities();

            if (log.IsDebugEnabled) log.Debug("LicenseController Done");
        }

        /// <summary>
        /// Constructor initializing the instance
        /// </summary>
        /// <param name="db">The databse context to be used</param>
        public LicenseController(ILicenseToolContext db)
        {
            if (log.IsDebugEnabled) log.Debug("LicenseController");

            this.db = db;

            if (log.IsDebugEnabled) log.Debug("LicenseController Done");
        }

        /// <summary>
        /// Shows the list of all entities and passes it to <see cref="Index(LicenseSearchModel)"/>
        /// </summary>
        /// <param name="serverId">The id of the LicServer the index has to be filtered</param>
        /// <param name="licenseId">The id of the License the index has to be filtered</param>
        /// <returns>The view result</returns>
        public ActionResult Index(int serverId = 0, int licenseId = 0)
        {
            if (log.IsDebugEnabled) log.Debug("Index");

            LicenseSearchModel search = new LicenseSearchModel();
            search.SelectedLicServer = serverId;
            search.SelectedLicense = licenseId;
            search.SortColumn = "FeatureName";
            if (licenseId > 0)
            {
                License lic = db.License.FirstOrDefault(l => l.Id == licenseId);
                if (lic != null) search.SelectedLicServer = lic.LicServer;
            }

            if (log.IsDebugEnabled) log.Debug("Index Done");
            return Index(search);
        }

        /// <summary>
        /// Shows the list of all entities
        /// </summary>
        /// <param name="search">The search model to be applied</param>
        /// <returns>The view result</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(LicenseSearchModel search)
        {
            if (log.IsDebugEnabled) log.Debug("Index");

            var license = db.License.Include(l => l.NavLicServer);
            List<LicServer> servers = new List<LicServer>(db.LicServer.OrderBy(s => s.Name));
            ViewBag.AllTypes = Helpers.EnumAsSelectList(typeof(LicenseType));
            ViewBag.AllStatus = Helpers.EnumAsSelectList(typeof(LicenseStatus));
            IQueryable<License> foundItems = null;
            if (search.SelectedLicServer == 0)
            {
                if (search.SelectedLicense == 0)
                    foundItems = license;
                else
                    foundItems = license.Where(l => l.Id == search.SelectedLicense);
                SelectList lst = new SelectList(servers, "Id", "Name");
                ViewBag.AllLicServers = lst;
            }
            else
            {
                if (search.SelectedLicense == 0)
                    foundItems = license.Where(l => l.LicServer == search.SelectedLicServer);
                else
                    foundItems = license.Where(l => l.Id == search.SelectedLicense && l.LicServer == search.SelectedLicServer);
                SelectList lst = new SelectList(servers, "Id", "Name", search.SelectedLicServer);
                ViewBag.AllLicServers = lst;
            }
            if (search.NavLicServer!=null && search.NavLicServer.Name != null)
            {
                int id = Convert.ToInt32(search.NavLicServer.Name);
                foundItems = license.Where(l => l.LicServer == id);
            }
            if (!string.IsNullOrEmpty(search.FeatureName))
            {
                string[] tsts = search.FeatureName.ToLower().Trim(new char[] { '*' }).Split(new char[] { '*' });
                foreach(string tst in tsts)
                    foundItems = foundItems.Where(l => l.FeatureName.ToLower().Contains(tst));
            }
            if (!string.IsNullOrEmpty(search.OrderName))
            {
                string[] tsts = search.OrderName.ToLower().Trim(new char[] { '*' }).Split(new char[] { '*' });
                foreach (string tst in tsts)
                    foundItems = foundItems.Where(l => l.OrderName.ToLower().Contains(tst));
            }
            if (!string.IsNullOrEmpty(search.Version))
            {
                string[] tsts = search.Version.ToLower().Trim(new char[] { '*' }).Split(new char[] { '*' });
                foreach (string tst in tsts)
                    foundItems = foundItems.Where(l => l.Version.ToLower().Contains(tst));
            }
            if (search.NumLicenses != null)
            {
                foundItems = foundItems.Where(l => l.NumLicenses == search.NumLicenses);
            }
            if (search.Type != null)
            {
                foundItems = foundItems.Where(l => l.Type == search.Type);
            }
            if (search.Status != null)
            {
                foundItems = foundItems.Where(l => l.Status == search.Status);
            }
            if (search.OrderEndDate != null)
            {
                foundItems = foundItems.Where(l => l.OrderEndDate == search.OrderEndDate);
            }
            if (search.OrderStartDate != null)
            {
                foundItems = foundItems.Where(l => l.OrderStartDate == search.OrderStartDate);
            }

            search.MaxItems = foundItems.Count();
            search.PageSize = Helpers.GetPageSize();
            foundItems = foundItems.Distinct();
            if (search.SortColumn == "FeatureName")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(l => l.NavLicServer.Name).ThenBy(s => s.FeatureName);
                else
                    foundItems = foundItems.OrderBy(l => l.NavLicServer.Name).ThenByDescending(s => s.FeatureName);
            }
            if (search.SortColumn == "Status")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(l => l.NavLicServer.Name).ThenBy(s => s.Status);
                else
                    foundItems = foundItems.OrderBy(l => l.NavLicServer.Name).ThenByDescending(s => s.Status);
            }
            if (search.SortColumn == "OrderName")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(l => l.NavLicServer.Name).ThenBy(s => s.OrderName);
                else
                    foundItems = foundItems.OrderBy(l => l.NavLicServer.Name).ThenByDescending(s => s.OrderName);
            }
            if (search.SortColumn == "OrderStartDate")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(l => l.NavLicServer.Name).ThenBy(s => s.OrderStartDate);
                else
                    foundItems = foundItems.OrderBy(l => l.NavLicServer.Name).ThenByDescending(s => s.OrderStartDate);
            }
            if (search.SortColumn == "OrderEndDate")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(l => l.NavLicServer.Name).ThenBy(s => s.OrderEndDate);
                else
                    foundItems = foundItems.OrderBy(l => l.NavLicServer.Name).ThenByDescending(s => s.OrderEndDate);
            }
            if (search.SortColumn == "Type")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(l => l.NavLicServer.Name).ThenBy(s => s.Type);
                else
                    foundItems = foundItems.OrderBy(l => l.NavLicServer.Name).ThenByDescending(s => s.Type);
            }
            if (search.SortColumn == "Version")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(l => l.NavLicServer.Name).ThenBy(s => s.Version);
                else
                    foundItems = foundItems.OrderBy(l => l.NavLicServer.Name).ThenByDescending(s => s.Version);
            }
            if (search.SortColumn == "NumLicenses")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(l => l.NavLicServer.Name).ThenBy(s => s.NumLicenses);
                else
                    foundItems = foundItems.OrderBy(l => l.NavLicServer.Name).ThenByDescending(s => s.NumLicenses);
            }
            if (search.SortColumn == "NavLicServer.Name")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(l => l.NavLicServer.Name);
                else
                    foundItems = foundItems.OrderByDescending(l => l.NavLicServer.Name);
            }
            ViewBag.ShownItems = foundItems.Skip(search.PagingStartIndex * Helpers.GetPageSize())
                .Take(Helpers.GetPageSize()).ToList();

            if (log.IsDebugEnabled) log.Debug("Index Done");
            return View(search);
        }

        /// <summary>
        /// Shows the details of an entity
        /// </summary>
        /// <param name="id">The id of the entity to be shown</param>
        /// <returns>The view result if entity has been found</returns>
        /// <remarks>The method redirects to <see cref="HomeController.Error"/> if the entity with given id is not found</remarks>
        public ActionResult Details(int id = 0)
        {
            if (log.IsDebugEnabled) log.Debug("Details");

            License license = db.License.Find(id);
            if (license == null)
            {
                if (log.IsDebugEnabled) log.Debug("Dispose Done");
                return RedirectToAction("Error", "Home", new { id = "404", aspxerrorpath = Request.Url.AbsolutePath });
            }

            if (log.IsDebugEnabled) log.Debug("Details Done");
            return View(license);
        }

        /// <summary>
        /// Shows the create new entity page
        /// </summary>
        /// <returns>The view result</returns>
        public ActionResult Create()
        {
            if (log.IsDebugEnabled) log.Debug("Create");

            ViewBag.LicServer = new SelectList(db.LicServer, "Id", "Name");

            if (log.IsDebugEnabled) log.Debug("Create Done");
            return View();
        }

        //
        // POST: /License/Create

        /// <summary>
        /// Post to create the new entity
        /// </summary>
        /// <param name="license">The entity to be created and stored in database</param>
        /// <returns>The view result if anything is wrong</returns>
        /// <remarks>Redirects to <see cref="Index(int,int)"/> if creation was succesfull</remarks>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(License license)
        {
            if (log.IsDebugEnabled) log.Debug("Create");

            if (ModelState.IsValid)
            {
                db.License.Add(license);
                db.SaveChanges();

                if (log.IsDebugEnabled) log.Debug("Create RedirectToAction Done");
                return RedirectToAction("Index");
            }
            ViewBag.LicServer = new SelectList(db.LicServer, "Id", "Name", license.LicServer);

            if (log.IsDebugEnabled) log.Debug("Create Done");
            return View(license);
        }

        /// <summary>
        /// Shows the edit page for an entity
        /// </summary>
        /// <param name="id">The id of the entity to be edited</param>
        /// <returns>The view result if entity has been found</returns>
        /// <remarks>The method redirects to <see cref="HomeController.Error"/> if the entity with given id is not found</remarks>
        public ActionResult Edit(int id = 0)
        {
            if (log.IsDebugEnabled) log.Debug("Edit");

            License license = db.License.Find(id);
            if (license == null)
            {
                if (log.IsDebugEnabled) log.Debug("Edit HttpNotFound Done");
                return RedirectToAction("Error", "Home", new { id = "404", aspxerrorpath = Request.Url.AbsolutePath });
            }
            ViewBag.LicServer = new SelectList(db.LicServer, "Id", "Name", license.LicServer);

            if (log.IsDebugEnabled) log.Debug("Edit Done");
            return View(license);
        }

        /// <summary>
        /// Post to edit the new entity
        /// </summary>
        /// <param name="license">The entity to be changed and stored in database</param>
        /// <returns>The view result if anything is wrong</returns>
        /// <remarks>Redirects to <see cref="Index(int,int)"/> if change was succesfull</remarks>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(License license)
        {
            if (log.IsDebugEnabled) log.Debug("Edit");

            if (ModelState.IsValid)
            {
                db.SetModified(license);
                db.SaveChanges();
                if (log.IsDebugEnabled) log.Debug("Edit RedirectToAction Done");
                return RedirectToAction("Index");
            }
            ViewBag.LicServer = new SelectList(db.LicServer, "Id", "Name", license.LicServer);

            if (log.IsDebugEnabled) log.Debug("Edit Done");
            return View(license);
        }

        /// <summary>
        /// Shows the delete page for an entity
        /// </summary>
        /// <param name="id">The id of the entity to be deleted</param>
        /// <returns>The view result if entity has been found</returns>
        /// <remarks>The method redirects to <see cref="HomeController.Error"/> if the entity with given id is not found</remarks>
        public ActionResult Delete(int id = 0)
        {
            if (log.IsDebugEnabled) log.Debug("Delete");

            License license = db.License.Find(id);
            if (license == null)
            {

                if (log.IsDebugEnabled) log.Debug("Delete HttpNotFound Done");
                return RedirectToAction("Error", "Home", new { id = "404", aspxerrorpath = Request.Url.AbsolutePath });
            }

            if (log.IsDebugEnabled) log.Debug("Delete Done");
            return View(license);
        }

        /// <summary>
        /// Post to delete the specified entity
        /// </summary>
        /// <param name="id">The id of the entity to be deleted</param>
        /// <returns>The redirect action to <see cref="Index(int,int)"/></returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (log.IsDebugEnabled) log.Debug("DeleteConfirmed");

            License license = db.License.Find(id);
            db.License.Remove(license);
            db.SaveChanges();

            if (log.IsDebugEnabled) log.Debug("DeleteConfirmed Done");
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (log.IsDebugEnabled) log.Debug("Dispose");

            db.Dispose();
            base.Dispose(disposing);

            if (log.IsDebugEnabled) log.Debug("Dispose Done");
        }
    }
}