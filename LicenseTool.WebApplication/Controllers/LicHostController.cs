﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LicenseTool.ClassLibrary;
using LicenseTool.WebApplication.Models;

namespace LicenseTool.WebApplication.Controllers
{

    /// <summary>
    /// Controller for the LicHost entity
    /// </summary>
    public class LicHostController : BaseController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private ILicenseToolContext db = null;

        /// <summary>
        /// Constructor initializing the instance
        /// </summary>
        public LicHostController()
        {
            if (log.IsDebugEnabled) log.Debug("LicHostController");

            this.db = new LicenseToolDatabaseEntities();

            if (log.IsDebugEnabled) log.Debug("LicHostController Done");
        }

        /// <summary>
        /// Constructor initializing the instance
        /// </summary>
        /// <param name="db">The databse context to be used</param>
        public LicHostController(ILicenseToolContext db)
        {
            if (log.IsDebugEnabled) log.Debug("LicHostController");

            this.db = db;

            if (log.IsDebugEnabled) log.Debug("LicHostController Done");
        }

        /// <summary>
        /// Shows the list of all entities and passes it to <see cref="Index(LicHostSearchModel)"/>
        /// </summary>
        /// <param name="licenseId">The id of the License the index has to be filtered</param>
        /// <param name="serverId">The id of the LicServer the index has to be filtered</param>
        /// <param name="hostId">The id of the LicHost the index has to be filtered</param>
        /// <param name="userId">The id of the LicUser the index has to be filtered</param>
        /// <returns>The view result</returns>
        public ActionResult Index(int licenseId = 0, int serverId = 0, int hostId = 0, int userId = 0)
        {
            if (log.IsDebugEnabled) log.Debug("Index");

            LicHostSearchModel search = new LicHostSearchModel();
            search.SelectedLicense = licenseId;
            search.SelectedLicServer = serverId;
            search.SelectedLicHost = hostId;
            search.SelectedLicUser = userId;
            search.SortColumn = "Name";
            if (licenseId > 0)
            {
                License lic = db.License.FirstOrDefault(l => l.Id == licenseId);
                if (lic != null) search.SelectedLicServer = lic.LicServer;
            }

            if (log.IsDebugEnabled) log.Debug("Index Done");
            return Index(search);
        }

        /// <summary>
        /// Shows the list of all entities
        /// </summary>
        /// <param name="search">The search model to be applied</param>
        /// <returns>The view result</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(LicHostSearchModel search)
        {
            if (log.IsDebugEnabled) log.Debug("Index");

            List<LicServer> servers = new List<LicServer>(db.LicServer.OrderBy(s => s.Name));
            IQueryable<LicHost> foundItems = null;
            if (search.SelectedLicServer == 0)
            {
                SelectList lst = new SelectList(servers, "Id", "Name");
                ViewBag.AllLicServers = lst;
                List<License> licenses = new List<License>();
                ViewBag.AllLicenses = new SelectList(licenses, "Id", "FeatureName");
                foundItems = db.LicHost;
            }
            else
            {
                SelectList lst = new SelectList(servers, "Id", "Name", search.SelectedLicServer);
                ViewBag.AllLicServers = lst;
                List<License> licenses = new List<License>(db.License.Where(l => l.LicServer == search.SelectedLicServer).OrderBy(s => s.FeatureName));
                if (search.SelectedLicense == 0)
                {
                    ViewBag.AllLicenses = new SelectList(licenses, "Id", "FeatureName");
                    foundItems = db.LicHost.Where(h => h.NavLicenseUsage.Any(u => u.NavLicense.NavLicServer.Id == search.SelectedLicServer));
                }
                else
                {
                    ViewBag.AllLicenses = new SelectList(licenses, "Id", "FeatureName", search.SelectedLicense);
                    foundItems = db.LicHost.Where(h => h.NavLicenseUsage.Any(u => u.NavLicense.Id == search.SelectedLicense));
                }
            }
            if (search.SelectedLicUser > 0)
            {
                foundItems = foundItems.Where(l => l.NavLicenseUsage.Any(u => u.LicUser == search.SelectedLicUser && u.LicHost == l.Id));
            }
            if (!string.IsNullOrEmpty(search.Name))
            {
                string[] tsts = search.Name.ToLower().Trim(new char[] { '*' }).Split(new char[] { '*' });
                foreach (string tst in tsts)
                    foundItems = foundItems.Where(l => l.Name.ToLower().Contains(tst));
            }
            if (search.SelectedLicHost > 0)
            {
                foundItems = foundItems.Where(h => h.Id == search.SelectedLicHost);
            }
            search.MaxItems = foundItems.Count();
            search.PageSize = Helpers.GetPageSize();
            foundItems = foundItems.Distinct();
            if (search.SortColumn == "Name")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(s => s.Name);
                else
                    foundItems = foundItems.OrderByDescending(s => s.Name);
            }
            ViewBag.ShownItems = foundItems.Skip(search.PagingStartIndex * Helpers.GetPageSize())
                .Take(Helpers.GetPageSize()).ToList();

            if (log.IsDebugEnabled) log.Debug("Index Done");
            return View(search);
        }

        /// <summary>
        /// Shows the details of an entity
        /// </summary>
        /// <param name="id">The id of the entity to be shown</param>
        /// <returns>The view result if entity has been found</returns>
        /// <remarks>The method redirects to <see cref="HomeController.Error"/> if the entity with given id is not found</remarks>
        public ActionResult Details(int id = 0)
        {
            if (log.IsDebugEnabled) log.Debug("Details");

            LicHost lichost = db.LicHost.Find(id);
            if (lichost == null)
            {
                if (log.IsDebugEnabled) log.Debug("Details HttpNotFound Done");
                return RedirectToAction("Error", "Home", new { id = "404", aspxerrorpath = Request.Url.AbsolutePath });
            }

            if (log.IsDebugEnabled) log.Debug("Details Done");
            return View(lichost);
        }

        /// <summary>
        /// Shows the create new entity page
        /// </summary>
        /// <returns>The view result</returns>
        public ActionResult Create()
        {
            if (log.IsDebugEnabled) log.Debug("Create");

            if (log.IsDebugEnabled) log.Debug("Create Done");
            return View();
        }

        /// <summary>
        /// Post to create the new entity
        /// </summary>
        /// <param name="lichost">The entity to be created and stored in database</param>
        /// <returns>The view result if anything is wrong</returns>
        /// <remarks>Redirects to <see cref="Index(int,int,int,int)"/> if creation was succesfull</remarks>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LicHost lichost)
        {
            if (log.IsDebugEnabled) log.Debug("Create");

            if (ModelState.IsValid)
            {
                db.LicHost.Add(lichost);
                db.SaveChanges();

                if (log.IsDebugEnabled) log.Debug("Create RedirectToAction Done");
                return RedirectToAction("Index");
            }

            if (log.IsDebugEnabled) log.Debug("Create Done");
            return View(lichost);
        }

        /// <summary>
        /// Shows the edit page for an entity
        /// </summary>
        /// <param name="id">The id of the entity to be edited</param>
        /// <returns>The view result if entity has been found</returns>
        /// <remarks>The method redirects to <see cref="HomeController.Error"/> if the entity with given id is not found</remarks>
        public ActionResult Edit(int id = 0)
        {
            if (log.IsDebugEnabled) log.Debug("Edit");

            LicHost lichost = db.LicHost.Find(id);
            if (lichost == null)
            {
                if (log.IsDebugEnabled) log.Debug("Edit HttpNotFound Done");
                return RedirectToAction("Error", "Home", new { id = "404", aspxerrorpath = Request.Url.AbsolutePath });
            }

            if (log.IsDebugEnabled) log.Debug("Edit Done");
            return View(lichost);
        }

        /// <summary>
        /// Post to edit the new entity
        /// </summary>
        /// <param name="lichost">The entity to be changed and stored in database</param>
        /// <returns>The view result if anything is wrong</returns>
        /// <remarks>Redirects to <see cref="Index(int,int,int,int)"/> if change was succesfull</remarks>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LicHost lichost)
        {
            if (log.IsDebugEnabled) log.Debug("Edit");

            if (ModelState.IsValid)
            {
                db.SetModified(lichost);
                db.SaveChanges();

                if (log.IsDebugEnabled) log.Debug("Edit RedirectToAction Done");
                return RedirectToAction("Index");
            }

            if (log.IsDebugEnabled) log.Debug("Edit Done");
            return View(lichost);
        }

        /// <summary>
        /// Shows the delete page for an entity
        /// </summary>
        /// <param name="id">The id of the entity to be deleted</param>
        /// <returns>The view result if entity has been found</returns>
        /// <remarks>The method redirects to <see cref="HomeController.Error"/> if the entity with given id is not found</remarks>
        public ActionResult Delete(int id = 0)
        {
            if (log.IsDebugEnabled) log.Debug("Delete");

            LicHost lichost = db.LicHost.Find(id);
            if (lichost == null)
            {
                if (log.IsDebugEnabled) log.Debug("Delete HttpNotFound Done");
                return RedirectToAction("Error", "Home", new { id = "404", aspxerrorpath = Request.Url.AbsolutePath });
            }

            if (log.IsDebugEnabled) log.Debug("Delete Done");
            return View(lichost);
        }

        /// <summary>
        /// Post to delete the specified entity
        /// </summary>
        /// <param name="id">The id of the entity to be deleted</param>
        /// <returns>The redirect action to <see cref="Index(int,int,int,int)"/></returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (log.IsDebugEnabled) log.Debug("DeleteConfirmed");

            LicHost lichost = db.LicHost.Find(id);
            db.LicHost.Remove(lichost);
            db.SaveChanges();

            if (log.IsDebugEnabled) log.Debug("DeleteConfirmed Done");
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (log.IsDebugEnabled) log.Debug("Dispose");

            db.Dispose();
            base.Dispose(disposing);

            if (log.IsDebugEnabled) log.Debug("Dispose Done");
        }
    }
}