﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LicenseTool.ClassLibrary;
using LicenseTool.WebApplication.Models;

namespace LicenseTool.WebApplication.Controllers
{

    /// <summary>
    /// Controller for the LicServer entity
    /// </summary>
    public class LicServerController : BaseController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private ILicenseToolContext db = null;

        /// <summary>
        /// Constructor initializing the instance
        /// </summary>
        public LicServerController()
        {
            if (log.IsDebugEnabled) log.Debug("LicServerController");

            this.db = new LicenseToolDatabaseEntities();

            if (log.IsDebugEnabled) log.Debug("LicServerController Done");
        }

        /// <summary>
        /// Constructor initializing the instance
        /// </summary>
        /// <param name="db">The databse context to be used</param>
        public LicServerController(ILicenseToolContext db)
        {
            if (log.IsDebugEnabled) log.Debug("LicServerController");

            this.db = db;

            if (log.IsDebugEnabled) log.Debug("LicServerController Done");
        }

        /// <summary>
        /// Shows the list of all entities and passes it to <see cref="Index(LicServerSearchModel)"/>
        /// </summary>
        /// <param name="serverId">The id of the LicServer the index has to be filtered</param>
        /// <returns>The view result</returns>
        public ActionResult Index(int serverId = 0)
        {
            if (log.IsDebugEnabled) log.Debug("Index");

            LicServerSearchModel search = new LicServerSearchModel();
            search.SelectedLicServer = serverId;
            search.SortColumn = "Name";

            if (log.IsDebugEnabled) log.Debug("Index Done");
            return Index(search);
        }

        /// <summary>
        /// Shows the list of all entities
        /// </summary>
        /// <param name="search">The search model to be applied</param>
        /// <returns>The view result</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(LicServerSearchModel search)
        {
            if (log.IsDebugEnabled) log.Debug("Index");

            IQueryable<LicServer> foundItems = null;
            var licserver = db.LicServer.Include(l => l.NavLicHost1).Include(l => l.NavLicHost2).Include(l => l.NavLicHost3);
            if (search.SelectedLicServer == 0)
                foundItems = licserver;
            else
                foundItems = licserver.Where(s => s.Id == search.SelectedLicServer);
            ViewBag.AllServerAndVendorStatus = Helpers.EnumAsSelectList(typeof(ServerAndVendorStatus));
            if (!string.IsNullOrEmpty(search.Name))
            {
                string[] tsts = search.Name.ToLower().Trim(new char[] { '*' }).Split(new char[] { '*' });
                foreach (string tst in tsts)
                    foundItems = foundItems.Where(l => l.Name.ToLower().Contains(tst));
            }
            if (!string.IsNullOrEmpty(search.SelectedLicHost))
            {
                string[] tsts = search.SelectedLicHost.ToLower().Trim(new char[] { '*' }).Split(new char[] { '*' });
                foreach (string tst in tsts)
                    foundItems = foundItems.Where(l => 
                        l.NavLicHost1.Name.ToLower().Contains(tst) || 
                        l.NavLicHost2.Name.ToLower().Contains(tst) || 
                        l.NavLicHost3.Name.ToLower().Contains(tst) ||
                        l.LicPort1.ToString().Contains(tst) || 
                        l.LicPort2.ToString().Contains(tst) || 
                        l.LicPort3.ToString().Contains(tst));
            }
            if (!string.IsNullOrEmpty(search.VendorName))
            {
                string[] tsts = search.VendorName.ToLower().Trim(new char[] { '*' }).Split(new char[] { '*' });
                foreach (string tst in tsts)
                    foundItems = foundItems.Where(l => l.VendorName.ToLower().Contains(tst));
            }
            if (!string.IsNullOrEmpty(search.VendorVersion))
            {
                string[] tsts = search.VendorVersion.ToLower().Trim(new char[] { '*' }).Split(new char[] { '*' });
                foreach (string tst in tsts)
                    foundItems = foundItems.Where(l => l.VendorVersion.ToLower().Contains(tst));
            }
            if (!string.IsNullOrEmpty(search.ServerVersion))
            {
                string[] tsts = search.ServerVersion.ToLower().Trim(new char[] { '*' }).Split(new char[] { '*' });
                foreach (string tst in tsts)
                    foundItems = foundItems.Where(l => l.ServerVersion.ToLower().Contains(tst));
            }
            if (search.ServerStatus != null)
            {
                foundItems = foundItems.Where(l => l.ServerStatus == search.ServerStatus);
            }
            if (search.VendorStatus != null)
            {
                foundItems = foundItems.Where(l => l.VendorStatus == search.VendorStatus);
            }
            search.MaxItems = foundItems.Count();
            search.PageSize = Helpers.GetPageSize();
            foundItems = foundItems.Distinct();
            if (search.SortColumn == "Name")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(s => s.Name);
                else
                    foundItems = foundItems.OrderByDescending(s => s.Name);
            }
            if (search.SortColumn == "VendorName")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(s => s.VendorName);
                else
                    foundItems = foundItems.OrderByDescending(s => s.VendorName);
            }
            if (search.SortColumn == "ServerStatus")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(s => s.ServerStatus);
                else
                    foundItems = foundItems.OrderByDescending(s => s.ServerStatus);
            }
            if (search.SortColumn == "VendorStatus")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(s => s.VendorStatus);
                else
                    foundItems = foundItems.OrderByDescending(s => s.VendorStatus);
            }
            if (search.SortColumn == "ServerVersion")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(s => s.ServerVersion);
                else
                    foundItems = foundItems.OrderByDescending(s => s.ServerVersion);
            }
            if (search.SortColumn == "VendorVersion")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(s => s.VendorVersion);
                else
                    foundItems = foundItems.OrderByDescending(s => s.VendorVersion);
            }
            if (search.SortColumn == "LicHost")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(s => s.NavLicHost1.Name).ThenBy(s => s.NavLicHost2.Name).ThenBy(s => s.NavLicHost3.Name);
                else
                    foundItems = foundItems.OrderByDescending(s => s.NavLicHost1.Name).ThenBy(s => s.NavLicHost2.Name).ThenBy(s => s.NavLicHost3.Name);
            }
            ViewBag.ShownItems = foundItems.Skip(search.PagingStartIndex * Helpers.GetPageSize())
                .Take(Helpers.GetPageSize()).ToList();

            if (log.IsDebugEnabled) log.Debug("Index Done");
            return View(search);
        }

        /// <summary>
        /// Shows the details of an entity
        /// </summary>
        /// <param name="id">The id of the entity to be shown</param>
        /// <returns>The view result if entity has been found</returns>
        /// <remarks>The method redirects to <see cref="HomeController.Error"/> if the entity with given id is not found</remarks>
        public ActionResult Details(int id = 0)
        {
            if (log.IsDebugEnabled) log.Debug("Details");

            LicServer licserver = db.LicServer.Find(id);
            if (licserver == null)
            {

                if (log.IsDebugEnabled) log.Debug("Details HttpNotFound Done");
                HttpContext.Response.StatusCode = 404;
                HttpContext.Response.TrySkipIisCustomErrors = false;
                return RedirectToAction("Error", "Home", new { id = "404", aspxerrorpath = Request.Url.AbsolutePath });
            }

            if (log.IsDebugEnabled) log.Debug("Details Done");
            return View(licserver);
        }

        /// <summary>
        /// Shows the create new entity page
        /// </summary>
        /// <returns>The view result</returns>
        public ActionResult Create()
        {
            if (log.IsDebugEnabled) log.Debug("Create");

            ViewBag.LicHost1 = new SelectList(db.LicHost, "Id", "Name");
            ViewBag.LicHost2 = new SelectList(db.LicHost, "Id", "Name");
            ViewBag.LicHost3 = new SelectList(db.LicHost, "Id", "Name");

            ViewBag.AllServerAndVendorStatus = Helpers.EnumAsSelectList(typeof(ServerAndVendorStatus));

            if (log.IsDebugEnabled) log.Debug("Create Done");
            return View();
        }

        /// <summary>
        /// Post to create the new entity
        /// </summary>
        /// <param name="licserver">The entity to be created and stored in database</param>
        /// <returns>The view result if anything is wrong</returns>
        /// <remarks>Redirects to <see cref="Index(int)"/> if creation was succesfull</remarks>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LicServer licserver)
        {
            if (log.IsDebugEnabled) log.Debug("Create");

            if (ModelState.IsValid)
            {
                db.LicServer.Add(licserver);
                db.SaveChanges();

                if (log.IsDebugEnabled) log.Debug("Create RedirectToAction Done");
                return RedirectToAction("Index");
            }

            ViewBag.LicHost1 = new SelectList(db.LicHost, "Id", "Name", licserver.LicHost1);
            ViewBag.LicHost2 = new SelectList(db.LicHost, "Id", "Name", licserver.LicHost2);
            ViewBag.LicHost3 = new SelectList(db.LicHost, "Id", "Name", licserver.LicHost3);

            ViewBag.AllServerAndVendorStatus = Helpers.EnumAsSelectList(typeof(ServerAndVendorStatus));

            if (log.IsDebugEnabled) log.Debug("Create Done");
            return View(licserver);
        }

        /// <summary>
        /// Shows the edit page for an entity
        /// </summary>
        /// <param name="id">The id of the entity to be edited</param>
        /// <returns>The view result if entity has been found</returns>
        /// <remarks>The method redirects to <see cref="HomeController.Error"/> if the entity with given id is not found</remarks>
        public ActionResult Edit(int id = 0)
        {
            if (log.IsDebugEnabled) log.Debug("Edit");

            LicServer licserver = db.LicServer.Find(id);
            if (licserver == null)
            {

                if (log.IsDebugEnabled) log.Debug("Edit HttpNotFound Done");
                return RedirectToAction("Error", "Home", new { id = "404", aspxerrorpath = Request.Url.AbsolutePath });
            }
            ViewBag.LicHost1 = new SelectList(db.LicHost, "Id", "Name", licserver.LicHost1);
            ViewBag.LicHost2 = new SelectList(db.LicHost, "Id", "Name", licserver.LicHost2);
            ViewBag.LicHost3 = new SelectList(db.LicHost, "Id", "Name", licserver.LicHost3);

            ViewBag.AllServerAndVendorStatus = Helpers.EnumAsSelectList(typeof(ServerAndVendorStatus));

            if (log.IsDebugEnabled) log.Debug("Edit Done");
            return View(licserver);
        }

        /// <summary>
        /// Post to edit the new entity
        /// </summary>
        /// <param name="licserver">The entity to be changed and stored in database</param>
        /// <returns>The view result if anything is wrong</returns>
        /// <remarks>Redirects to <see cref="Index(int)"/> if change was succesfull</remarks>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LicServer licserver)
        {
            if (log.IsDebugEnabled) log.Debug("Edit");

            if (ModelState.IsValid)
            {
                db.SetModified(licserver);
                db.SaveChanges();

                if (log.IsDebugEnabled) log.Debug("Edit RedirectToAction Done");
                return RedirectToAction("Index");
            }
            ViewBag.LicHost1 = new SelectList(db.LicHost, "Id", "Name", licserver.LicHost1);
            ViewBag.LicHost2 = new SelectList(db.LicHost, "Id", "Name", licserver.LicHost2);
            ViewBag.LicHost3 = new SelectList(db.LicHost, "Id", "Name", licserver.LicHost3);

            ViewBag.AllServerAndVendorStatus = Helpers.EnumAsSelectList(typeof(ServerAndVendorStatus));

            if (log.IsDebugEnabled) log.Debug("Edit Done");
            return View(licserver);
        }

        /// <summary>
        /// Shows the delete page for an entity
        /// </summary>
        /// <param name="id">The id of the entity to be deleted</param>
        /// <returns>The view result if entity has been found</returns>
        /// <remarks>The method redirects to <see cref="HomeController.Error"/> if the entity with given id is not found</remarks>
        public ActionResult Delete(int id = 0)
        {
            if (log.IsDebugEnabled) log.Debug("Delete");

            LicServer licserver = db.LicServer.Find(id);
            if (licserver == null)
            {
                if (log.IsDebugEnabled) log.Debug("Delete HttpNotFound Done");
                return RedirectToAction("Error", "Home", new { id = "404", aspxerrorpath = Request.Url.AbsolutePath });
            }

            if (log.IsDebugEnabled) log.Debug("Delete Done");
            return View(licserver);
        }

        /// <summary>
        /// Post to delete the specified entity
        /// </summary>
        /// <param name="id">The id of the entity to be deleted</param>
        /// <returns>The redirect action to <see cref="Index(int)"/></returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (log.IsDebugEnabled) log.Debug("DeleteConfirmed");

            LicServer licserver = db.LicServer.Find(id);
            db.LicServer.Remove(licserver);
            db.SaveChanges();

            if (log.IsDebugEnabled) log.Debug("DeleteConfirmed Done");
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (log.IsDebugEnabled) log.Debug("Dispose");

            db.Dispose();
            base.Dispose(disposing);

            if (log.IsDebugEnabled) log.Debug("Dispose Done");
        }
    }
}