﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LicenseTool.ClassLibrary;
using LicenseTool.WebApplication.Models;

namespace LicenseTool.WebApplication.Controllers
{

    /// <summary>
    /// Controller for the LicenseUsage entity
    /// </summary>
    public class LicenseUsageController : BaseController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private ILicenseToolContext db = null;

        /// <summary>
        /// Constructor initializing the instance
        /// </summary>
        public LicenseUsageController()
        {
            if (log.IsDebugEnabled) log.Debug("LicenseUsageController");

            this.db = new LicenseToolDatabaseEntities();

            if (log.IsDebugEnabled) log.Debug("LicenseUsageController Done");
        }

        /// <summary>
        /// Constructor initializing the instance
        /// </summary>
        /// <param name="db">The databse context to be used</param>
        public LicenseUsageController(ILicenseToolContext db)
        {
            if (log.IsDebugEnabled) log.Debug("LicenseUsageController");

            this.db = db;

            if (log.IsDebugEnabled) log.Debug("LicenseUsageController Done");
        }

        /// <summary>
        /// Shows the list of all entities and passes it to <see cref="Index(LicenseUsageSearchModel)"/>
        /// </summary>
        /// <param name="licenseId">The id of the License the index has to be filtered</param>
        /// <param name="serverId">The id of the LicServer the index has to be filtered</param>
        /// <param name="hostId">The id of the LicHost the index has to be filtered</param>
        /// <param name="userId">The id of the LicUser the index has to be filtered</param>
        /// <param name="showOutOnly"></param>
        /// <returns>The view result</returns>
        public ActionResult Index(int licenseId = 0, int serverId = 0, int hostId = 0, int userId = 0, bool showOutOnly = false)
        {
            if (log.IsDebugEnabled) log.Debug("Index");

            LicenseUsageSearchModel search = new LicenseUsageSearchModel();
            search.License = licenseId;
            search.SelectedLicServer = serverId;
            search.ShowOutOnly = showOutOnly;
            search.LicHost = hostId;
            search.LicUser = userId;
            search.SortColumn = "StartDate";
            search.SortDirection = 1;
            if (licenseId > 0)
            {
                License lic = db.License.FirstOrDefault(l => l.Id == licenseId);
                if (lic != null) search.SelectedLicServer = lic.LicServer;
            }

            if (log.IsDebugEnabled) log.Debug("Index Done");
            return Index(search);
        }

        /// <summary>
        /// Shows the list of all entities
        /// </summary>
        /// <param name="search">The search model to be applied</param>
        /// <returns>The view result</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(LicenseUsageSearchModel search)
        {
            if (log.IsDebugEnabled) log.Debug("Index");

            var licenseusage = db.LicenseUsage.Include(l => l.NavLicense).Include(l => l.NavLicHost).Include(l => l.NavLicUser).Include(l => l.NavLicense.NavLicServer);
            List<LicServer> servers = new List<LicServer>(db.LicServer.OrderBy(s => s.Name));
            IQueryable<LicenseUsage> foundItems = null;
            SelectList lst = new SelectList(servers, "Id", "Name");
            ViewBag.AllLicServers = lst;
            ViewBag.AllStates = Helpers.EnumAsSelectList(typeof(LicenseState));
            if (search.SelectedLicServer == 0)
            {
                List<License> licenses = new List<License>();
                ViewBag.AllLicenses = new SelectList(licenses, "Id", "FeatureName");
                foundItems = licenseusage;
            }
            else
            {
                List<License> licenses = new List<License>(db.License.Where(l => l.LicServer == search.SelectedLicServer).OrderBy(s => s.FeatureName));
                if (search.License == 0)
                {
                    ViewBag.AllLicenses = new SelectList(licenses, "Id", "FeatureName");
                    foundItems = licenseusage.Where(u => u.NavLicense.NavLicServer.Id == search.SelectedLicServer);
                }
                else
                {
                    ViewBag.AllLicenses = new SelectList(licenses, "Id", "FeatureName", search.License);
                    foundItems = licenseusage.Where(u => u.NavLicense.Id == search.License);
                }
            }
            if (search.NavLicense == null && search.License > 0)
                search.NavLicense = db.License.Find(new object[]{ search.License });
            if (search.NavLicUser == null && search.LicUser > 0)
                search.NavLicUser = db.LicUser.Find(new object[] { search.LicUser });
            if (search.NavLicHost == null && search.LicHost > 0)
                search.NavLicHost = db.LicHost.Find(new object[] { search.LicHost });

            if (search.NavLicense != null && !string.IsNullOrEmpty(search.NavLicense.FeatureName))
            {
                string[] tsts = search.NavLicense.FeatureName.ToLower().Trim(new char[] { '*' }).Split(new char[] { '*' });
                foreach (string tst in tsts)
                    foundItems = foundItems.Where(u => u.NavLicense == null || u.NavLicense.FeatureName == null ? false : u.NavLicense.FeatureName.ToLower().Contains(tst));
            }
            if (search.NavLicUser != null && !string.IsNullOrEmpty(search.NavLicUser.Name))
            {
                string[] tsts = search.NavLicUser.Name.ToLower().Trim(new char[] { '*' }).Split(new char[] { '*' });
                foreach (string tst in tsts)
                    foundItems = foundItems.Where(u => u.NavLicUser == null || u.NavLicUser.Name == null ? false : u.NavLicUser.Name.ToLower().Contains(tst));
            }
            if (search.NavLicHost != null && !string.IsNullOrEmpty(search.NavLicHost.Name))
            {
                string[] tsts = search.NavLicHost.Name.ToLower().Trim(new char[] { '*' }).Split(new char[] { '*' });
                foreach (string tst in tsts)
                    foundItems = foundItems.Where(u => u.NavLicHost == null || u.NavLicHost.Name == null ? false : u.NavLicHost.Name.ToLower().Contains(tst));
            }
            if (!string.IsNullOrEmpty(search.Version))
            {
                string[] tsts = search.Version.ToLower().Trim(new char[] { '*' }).Split(new char[] { '*' });
                foreach (string tst in tsts)
                    foundItems = foundItems.Where(l => l.Version.ToLower().Contains(tst));
            }
            if (search.Quantity != null)
            {
                foundItems = foundItems.Where(u => u.Quantity == search.Quantity);
            }
            if (search.State != null)
            {
                foundItems = foundItems.Where(u => u.State == search.State);
            }
            if (search.StartDate != null)
            {
                foundItems = foundItems.Where(u => u.StartDate == search.StartDate);
            }
            if (search.EndDate != null)
            {
                foundItems = foundItems.Where(u => u.EndDate == search.EndDate);
            }
            if (search.ShowOutOnly)
            {
                foundItems = foundItems.Where(u => u.State == (int)LicenseState.OUT);
            }
            search.MaxItems = foundItems.Count();
            search.PageSize = Helpers.GetPageSize();
            foundItems = foundItems.Distinct();
            if (search.SortColumn == "EndDate")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(s => s.EndDate);
                else
                    foundItems = foundItems.OrderByDescending(s => s.EndDate);
            }
            if (search.SortColumn == "StartDate")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(s => s.StartDate);
                else
                    foundItems = foundItems.OrderByDescending(s => s.StartDate);
            }
            if (search.SortColumn == "State")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(s => s.State);
                else
                    foundItems = foundItems.OrderByDescending(s => s.State);
            }
            if (search.SortColumn == "Version")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(s => s.Version);
                else
                    foundItems = foundItems.OrderByDescending(s => s.Version);
            }
            if (search.SortColumn == "Quantity")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(s => s.Quantity);
                else
                    foundItems = foundItems.OrderByDescending(s => s.Quantity);
            }
            if (search.SortColumn == "NavLicUser.Name")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(s => s.NavLicUser.Name);
                else
                    foundItems = foundItems.OrderByDescending(s => s.NavLicUser.Name);
            }
            if (search.SortColumn == "NavLicHost.Name")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(s => s.NavLicHost.Name);
                else
                    foundItems = foundItems.OrderByDescending(s => s.NavLicHost.Name);
            }
            if (search.SortColumn == "NavLicense.FeatureName")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(s => s.NavLicense.FeatureName);
                else
                    foundItems = foundItems.OrderByDescending(s => s.NavLicense.FeatureName);
            }
            if (search.SortColumn == "NavLicense.NavLicServer.Name")
            {
                if (search.SortDirection == 0)
                    foundItems = foundItems.OrderBy(s => s.NavLicense.NavLicServer.Name);
                else
                    foundItems = foundItems.OrderByDescending(s => s.NavLicense.NavLicServer.Name);
            }

            ViewBag.ShownItems = foundItems.Skip(search.PagingStartIndex * Helpers.GetPageSize())
                .Take(Helpers.GetPageSize()).ToList();

            if (log.IsDebugEnabled) log.Debug("Index Done");
            return View(search);
        }

        /// <summary>
        /// Shows the details of an entity
        /// </summary>
        /// <param name="id">The id of the entity to be shown</param>
        /// <returns>The view result if entity has been found</returns>
        /// <remarks>The method redirects to <see cref="HomeController.Error"/> if the entity with given id is not found</remarks>
        public ActionResult Details(int id = 0)
        {
            if (log.IsDebugEnabled) log.Debug("Details");

            LicenseUsage licenseusage = db.LicenseUsage.Find(id);
            if (licenseusage == null)
            {
                if (log.IsDebugEnabled) log.Debug("Details HttpNotFound Done");
                return RedirectToAction("Error", "Home", new { id = "404", aspxerrorpath = Request.Url.AbsolutePath });
            }

            if (log.IsDebugEnabled) log.Debug("Details Done");
            return View(licenseusage);
        }

        /// <summary>
        /// Shows the create new entity page
        /// </summary>
        /// <returns>The view result</returns>
        public ActionResult Create()
        {
            if (log.IsDebugEnabled) log.Debug("Create");

            ViewBag.License = new SelectList(db.License, "Id", "FeatureName");
            ViewBag.LicHost = new SelectList(db.LicHost, "Id", "Name");
            ViewBag.LicUser = new SelectList(db.LicUser, "Id", "Name");
            ViewBag.AllStates = Helpers.EnumAsSelectList(typeof(LicenseState));

            if (log.IsDebugEnabled) log.Debug("Create Done");
            return View();
        }

        /// <summary>
        /// Post to create the new entity
        /// </summary>
        /// <param name="licenseusage">The entity to be created and stored in database</param>
        /// <returns>The view result if anything is wrong</returns>
        /// <remarks>Redirects to <see cref="Index(int,int,int,int,bool)"/> if creation was succesfull</remarks>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LicenseUsage licenseusage)
        {
            if (log.IsDebugEnabled) log.Debug("Create");

            if (ModelState.IsValid)
            {
                db.LicenseUsage.Add(licenseusage);
                db.SaveChanges();

                if (log.IsDebugEnabled) log.Debug("Create RedirectToAction Done");
                return RedirectToAction("Index");
            }

            ViewBag.License = new SelectList(db.License, "Id", "FeatureName", licenseusage.License);
            ViewBag.LicHost = new SelectList(db.LicHost, "Id", "Name", licenseusage.LicHost);
            ViewBag.LicUser = new SelectList(db.LicUser, "Id", "Name", licenseusage.LicUser);
            ViewBag.AllStates = Helpers.EnumAsSelectList(typeof(LicenseState));

            if (log.IsDebugEnabled) log.Debug("Create Done");
            return View(licenseusage);
        }

        /// <summary>
        /// Shows the edit page for an entity
        /// </summary>
        /// <param name="id">The id of the entity to be edited</param>
        /// <returns>The view result if entity has been found</returns>
        /// <remarks>The method redirects to <see cref="HomeController.Error"/> if the entity with given id is not found</remarks>
        public ActionResult Edit(int id = 0)
        {
            if (log.IsDebugEnabled) log.Debug("Edit");

            LicenseUsage licenseusage = db.LicenseUsage.Find(id);
            if (licenseusage == null)
            {
                if (log.IsDebugEnabled) log.Debug("Edit HttpNotFound Done");
                return RedirectToAction("Error", "Home", new { id = "404", aspxerrorpath = Request.Url.AbsolutePath });
            }
            ViewBag.License = new SelectList(db.License, "Id", "FeatureName", licenseusage.License);
            ViewBag.LicHost = new SelectList(db.LicHost, "Id", "Name", licenseusage.LicHost);
            ViewBag.LicUser = new SelectList(db.LicUser, "Id", "Name", licenseusage.LicUser);
            ViewBag.AllStates = Helpers.EnumAsSelectList(typeof(LicenseState));

            if (log.IsDebugEnabled) log.Debug("Edit Done");
            return View(licenseusage);
        }

        /// <summary>
        /// Post to edit the new entity
        /// </summary>
        /// <param name="licenseusage">The entity to be changed and stored in database</param>
        /// <returns>The view result if anything is wrong</returns>
        /// <remarks>Redirects to <see cref="Index(int,int,int,int,bool)"/> if change was succesfull</remarks>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LicenseUsage licenseusage)
        {
            if (log.IsDebugEnabled) log.Debug("Edit");

            if (ModelState.IsValid)
            {
                db.SetModified(licenseusage);
                db.SaveChanges();

                if (log.IsDebugEnabled) log.Debug("Edit RedirectToAction Done");
                return RedirectToAction("Index");
            }
            ViewBag.License = new SelectList(db.License, "Id", "FeatureName", licenseusage.License);
            ViewBag.LicHost = new SelectList(db.LicHost, "Id", "Name", licenseusage.LicHost);
            ViewBag.LicUser = new SelectList(db.LicUser, "Id", "Name", licenseusage.LicUser);
            ViewBag.AllStates = Helpers.EnumAsSelectList(typeof(LicenseState));

            if (log.IsDebugEnabled) log.Debug("Edit Done");
            return View(licenseusage);
        }

        /// <summary>
        /// Shows the delete page for an entity
        /// </summary>
        /// <param name="id">The id of the entity to be deleted</param>
        /// <returns>The view result if entity has been found</returns>
        /// <remarks>The method redirects to <see cref="HomeController.Error"/> if the entity with given id is not found</remarks>
        public ActionResult Delete(int id = 0)
        {
            if (log.IsDebugEnabled) log.Debug("Delete");

            LicenseUsage licenseusage = db.LicenseUsage.Find(id);
            if (licenseusage == null)
            {
                if (log.IsDebugEnabled) log.Debug("Delete HttpNotFound Done");
                return RedirectToAction("Error", "Home", new { id = "404", aspxerrorpath = Request.Url.AbsolutePath });
            }

            if (log.IsDebugEnabled) log.Debug("Delete Done");
            return View(licenseusage);
        }

        /// <summary>
        /// Post to delete the specified entity
        /// </summary>
        /// <param name="id">The id of the entity to be deleted</param>
        /// <returns>The redirect action to <see cref="Index(int,int,int,int,bool)"/></returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (log.IsDebugEnabled) log.Debug("DeleteConfirmed");

            LicenseUsage licenseusage = db.LicenseUsage.Find(id);
            db.LicenseUsage.Remove(licenseusage);
            db.SaveChanges();

            if (log.IsDebugEnabled) log.Debug("DeleteConfirmed Done");
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (log.IsDebugEnabled) log.Debug("Dispose");

            db.Dispose();
            base.Dispose(disposing);

            if (log.IsDebugEnabled) log.Debug("Dispose Done");
        }
    }
}