﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LicenseTool.WebApplication.Controllers;

namespace LicenseTool.WebApplication.Models
{

    /// <summary>
    /// The search model used in the reports page
    /// </summary>
    public class ReportSelectionModel
    {
        /// <summary>
        /// The name of the report
        /// </summary>
        public string ReportName { get; set; }

        /// <summary>
        /// The action we have to go back in case of error
        /// </summary>
        public string BackAction { get; set; }

        /// <summary>
        /// The paper format of the report
        /// </summary>
        public string PaperFormat { get; set; }

        /// <summary>
        /// True if user hits preview
        /// </summary>
        public bool Preview { get; set; }

        /// <summary>
        /// The start date for the report
        /// </summary>
        public DateTime ReportStartDate { get; set; }

        /// <summary>
        /// The month of the report
        /// </summary>
        public short Month { get; set; }

        /// <summary>
        /// The year of the report
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// The license server the report has to be generated for
        /// </summary>
        public int LicServer { get; set; }

        /// <summary>
        /// The license the report has to be generated for
        /// </summary>
        public int License { get; set; }

        /// <summary>
        /// The time frame for this report
        /// </summary>
        public ReportFrame ReportFrame { get; set; }

        /// <summary>
        /// The report file type
        /// </summary>
        public ReportFormat ReportFormat { get; set; }

        /// <summary>
        /// The group the report has to be done for
        /// </summary>
        public int? ReportGroup { get; set; }
    }

}