﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LicenseTool.WebApplication.Models
{

    /// <summary>
    /// Specifies required properties for paging
    /// </summary>
    public interface ISortModel
    {
        /// <summary>
        /// The actual sort column
        /// </summary>
        string SortColumn { get; set; }

        /// <summary>
        /// The actual sort direction
        /// </summary>
        int SortDirection { get; set; }
    }

}
