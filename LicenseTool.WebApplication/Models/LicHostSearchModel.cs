﻿using LicenseTool.ClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LicenseTool.WebApplication.Models
{

    /// <summary>
    /// The search model for LicHost used in the index page
    /// </summary>
    public class LicHostSearchModel : LicHost, IPageModel, ISortModel
    {

        /// <summary>
        /// List to be filtered with this LicServer
        /// </summary>
        public int SelectedLicServer { get; set; }

        /// <summary>
        /// List to be filtered with this License
        /// </summary>
        public int SelectedLicense { get; set; }

        /// <summary>
        /// List to be filtered with this LicUser
        /// </summary>
        public int SelectedLicUser { get; set; }

        /// <summary>
        /// List to be filtered with this LicHost
        /// </summary>
        public int SelectedLicHost { get; set; }

        /// <summary>
        /// The start index of the pages (0 based)
        /// </summary>
        public int PagingStartIndex { get; set; }

        /// <summary>
        /// The number of rows per page
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// The total number of rows
        /// </summary>
        public int MaxItems { get; set; }

        /// <summary>
        /// The actual sort column
        /// </summary>
        public string SortColumn { get; set; }

        /// <summary>
        /// The actual sort direction
        /// </summary>
        public int SortDirection { get; set; }
    }
}