﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LicenseTool.WebApplication.Models
{

    /// <summary>
    /// Specifies required properties for paging
    /// </summary>
    public interface IPageModel
    {
        /// <summary>
        /// The start index of the pages (0 based)
        /// </summary>
        int PagingStartIndex { get; set; }

        /// <summary>
        /// The number of rows per page
        /// </summary>
        int PageSize { get; set; }

        /// <summary>
        /// The total number of rows
        /// </summary>
        int MaxItems { get; set; }
    }
}
