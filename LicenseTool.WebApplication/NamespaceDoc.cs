﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LicenseTool.WebApplication
{
    /// <summary>
    /// The web application
    /// </summary>
    class NamespaceDoc
    {
    }
}

namespace LicenseTool.WebApplication.Controllers
{
    /// <summary>
    /// The web application controllers
    /// </summary>
    class NamespaceDoc
    {
    }
}

namespace LicenseTool.WebApplication.Models
{
    /// <summary>
    /// The web application models
    /// </summary>
    class NamespaceDoc
    {
    }
}

namespace LicenseTool.WebApplication.Views.Report
{
    /// <summary>
    /// Datasets for the reports
    /// </summary>
    class NamespaceDoc
    {
    }
}

namespace LicenseTool.WebApplication.Views.Report.UsageReportViewDataSetTableAdapters
{
    /// <summary>
    /// Datasets for the reports
    /// </summary>
    class NamespaceDoc
    {
    }
}
